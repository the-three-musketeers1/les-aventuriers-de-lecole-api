import {classToClass} from "class-transformer";
import * as express from "express";

export class PropertiesMiddleware {

    /**
     * check if all request body are Type properties
     * @param Type {Function}
     * @return function {Function}
     */
    static isBodyEntityProperties(Type: Function) {
        return (req: express.Request, res: express.Response, next: express.NextFunction) => {
            const instance = classToClass(Type);
            const notDefinedField = Object.keys(req.body).find(field => instance[field] === undefined);
            if (notDefinedField !== undefined) {
                res.status(403).send({error: `'${notDefinedField}' is not property of ${Type.name}`});
                return;
            }
            next();
        }
    }

    /**
     * check if all request body properties are in defined properties
     * @param definedProperties {string[]}
     * @return function {Function}
     */
    static isBodyDefinedProperties(definedProperties: string[]) {
        return (req: express.Request, res: express.Response, next: express.NextFunction) => {
            const notDefinedField = Object.keys(req.body).find(field => !definedProperties.includes(field));
            if (notDefinedField !== undefined) {
                res.status(403).send({error: `'${notDefinedField}' is not authorize to send`});
                return;
            }
            next();
        }
    }

    /**
     * check if request body not contain forbidden properties
     * @param forbiddenProperties {string[]}
     * @return function {Function}
     */
    static isBodyNotForbiddenProperties(forbiddenProperties: string[]) {
        return (req: express.Request, res: express.Response, next: express.NextFunction) => {
            const notDefinedField = Object.keys(req.body).find(field => forbiddenProperties.includes(field));
            if (notDefinedField !== undefined) {
                res.status(403).send({error: `'${notDefinedField}' property is forbidden to send`});
                return;
            }
            next();
        }
    }
}
