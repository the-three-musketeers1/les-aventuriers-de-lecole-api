import * as express from "express";
import {AuthService} from "../services/auth.service";
import {TYPE} from "../constants/types";
import {inject, injectable} from "inversify";
import {BaseMiddleware} from "inversify-express-utils";

@injectable()
export class AuthOnlyMiddleware extends BaseMiddleware {

    constructor(@inject(TYPE.AuthService) private authService: AuthService) {
        super();
    }

    async handler(req: express.Request, res: express.Response, next: express.NextFunction): Promise<void> {
        const authorization = req.headers.authorization;

        if (!authorization || authorization.length <= 0) {
            res.status(401).send({error: 'not authorization set'});
            return;
        }

        const token = authorization.slice('Bearer '.length);
        const user = await this.authService.getUserBySessionToken(token);
        if (!user) {
            res.status(403).send({error: 'not authorize'});
            return;
        }
        next();
    }

}
