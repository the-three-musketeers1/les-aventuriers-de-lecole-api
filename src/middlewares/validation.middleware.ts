import * as express from "express";
import {validateSync, ValidationError} from "class-validator";
import {plainToClass} from "class-transformer";

/**
 * Validate the request body data depend to entity
 * @param type {any}: Entity
 * @param skipMissingProperties {boolean} : skip validation for each empty property
 * @param trim {boolean} : trim all string properties
 * @return Function
 */
export function validationMiddleware(type: any, skipMissingProperties: boolean = false, trim: boolean = false): express.RequestHandler {

    return (req, res, next) => {
        if (trim) {
            trimStringProperties(req.body);
        }

        const errors = validateSync(plainToClass(type, req.body), {skipMissingProperties});
        if (errors.length > 0) {
            const errorProperties = errors.map((error: ValidationError) => {
                return {property: error.property, causes: error.constraints};
            });
            res.status(403).send({errors: errorProperties});
            return;
        }
        next();
    }
}

function trimStringProperties(body: Object) {
    const properties = Object.keys(body);

    properties.forEach(property => {
        if (typeof body[property] === "string") {
            body[property] = body[property].trim();
        }
    })
}
