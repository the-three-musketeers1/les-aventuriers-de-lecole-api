export class FormatQueryParamMiddleware {
    static formatstatsRequest() {
        return (req, res, next) => {
            const queryParam = req.query;
            if (queryParam.beginDate === undefined || queryParam.beginDate === null || queryParam.beginDate === 'null') {
                queryParam.beginDate = null;
            }
            if (queryParam.endDate === undefined || queryParam.endDate === null || queryParam.endDate === 'null') {
                queryParam.endDate = null;
            }
            if (queryParam.disciplineId !== undefined && queryParam.disciplineId !== null && queryParam.disciplineId !== 'null') {
                queryParam.disciplineId = Number.parseInt(queryParam.disciplineId)
            } else {
                queryParam.disciplineId = null;
            }
            if (queryParam.chapterId !== undefined && queryParam.chapterId !== null && queryParam.chapterId !== 'null') {
                queryParam.chapterId = Number.parseInt(queryParam.chapterId)
            } else {
                queryParam.chapterId = null;
            }
            next();
        }
    }
}
