import {BaseMiddleware} from "inversify-express-utils";
import * as express from "express";
import {TYPE} from "../../constants/types";
import {UserRepository} from "../../repositories/user.repository";
import {inject, injectable} from "inversify";
import {User} from "../../entities/User";

@injectable()
export class AuthStudentMiddleware extends BaseMiddleware {

    constructor(@inject(TYPE.UserRepository) private userRepository: UserRepository) {
        super();
    }

    async handler(req: express.Request, res: express.Response, next: express.NextFunction): Promise<void> {
        let user = req.body.user as User;

        if (!user && user.email) {
            user = await this.userRepository.findOne({email: user.email});
        } else if (user.id) {
            user = await this.userRepository.findOne({id: user.id});
        }

        if (!user) {
            res.status(404).send({error: 'request send data not correct'});
            return;
        }
        if (user.is_teacher !== false) {
            res.status(403).send({error: 'subscriber have to be student'});
            return;
        }
        next();
    }

}
