import {BaseMiddleware} from "inversify-express-utils";
import * as express from "express";
import {TYPE} from "../../constants/types";
import {UserRepository} from "../../repositories/user.repository";
import {inject, injectable} from "inversify";
import {User} from "../../entities/User";
import {Character} from "../../entities/Character";
import {CharacterRepository} from "../../repositories/CharacterRepository";

@injectable()
export class AuthCharacterMiddleware extends BaseMiddleware {

    constructor(@inject(TYPE.UserRepository) private userRepository: UserRepository,
                @inject(TYPE.CharacterRepository) private characterRepository: CharacterRepository) {
        super();
    }

    async handler(req: express.Request, res: express.Response, next: express.NextFunction): Promise<void> {
        let character = req.body.character as Character;
        if(!character) {
            let characterId = req.body.characterId ? req.body.characterId : req.params.characterId;
            character = await this.characterRepository.findOne(characterId);
        }

        if (!character) {
            res.status(404).send({error: 'request send data not correct'});
            return;
        }

        let user = req.body.user;
        if (!user) {
            res.status(404).send({error: 'request send data not correct'});
            return;
        }

        if(!await this.doesCharacterBelongToUser(character.id, user)) {
            res.status(403).send({error: 'Character does not belong to your user'});
            return;
        }

        req.body.character = character;
        next();
    }

    async doesCharacterBelongToUser(characterId: number, user: User): Promise<boolean> {
        if(!user.characters) {
            user.characters = await this.characterRepository.find({where: {user}});
        }
        for (let character of user.characters) {
            if(+character.id === +characterId) {
                return true;
            }
        }
        return false;
    }

}
