import {
    controller,
    httpPost,
    request,
    requestParam,
    response
} from "inversify-express-utils";
import {inject} from "inversify";
import {TYPE} from "../constants/types";
import {MessageService} from "../services/message.service";
import * as express from "express";
import {isBodyString} from "../validators/bodies.validator";

@controller('/message')
export class MessageController {
    constructor(
        @inject(TYPE.MessageService) private messageService: MessageService
    ) {}

    @httpPost('/:ticketId/character',
        TYPE.AuthMiddleware,
        TYPE.AuthCharacterMiddleware,
        isBodyString('content')
    )
    public async postMessage(
        @requestParam('ticketId') ticketId: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const content = req.body.content;
            let message = await this.messageService.save(ticketId, content, true, req.body.character.id);
            if (message === null) {
                res.status(403).send();
                return;
            }
            res.status(201).json(message);
        } catch (e) {
            res.status(500).send(e.message);
        }
    }

}
