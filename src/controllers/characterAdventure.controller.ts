import {controller, httpPost, requestBody, response} from "inversify-express-utils";
import {inject} from "inversify";
import {TYPE} from "../constants/types";
import {ExerciseService} from "../services/exercise.service";
import * as express from "express";
import {AdventureService} from "../services/adventure.service";
import {CharacterService} from "../services/character.service";
import {CharacterAdventureService} from "../services/characterAdventure.service";

@controller('/characterAdventure')
export class CharacterAdventureController {
    constructor(
        @inject(TYPE.CharacterAdventureService) private characterAdventureService: CharacterAdventureService,
        @inject(TYPE.AdventureService) private adventureService: AdventureService,
        @inject(TYPE.ExerciseService)  private exerciseService:  ExerciseService,
        @inject(TYPE.CharacterService) private characterService: CharacterService
    ) {
    }

    @httpPost('/ending')
    public async characterEndExpedition(
        @requestBody() data: any,
        @response() res: express.Response
    ) {
        try {
            let characterAdventure = await this.characterAdventureService.findByExpeditionIdAndCharacterId(data.expeditionId, data.characterId);
            if (characterAdventure !== null && characterAdventure.length > 0) {
                res.status(401).end();
                return
            }
            const exercises = await this.exerciseService.findExercisesOfExpeditionsWithCharacterContent(data.expeditionId, data.characterId);
            const exercisesExpeditionOfCharacter = this.exerciseService.transformListOfExercisesIntoExercisesExpeditionsOfCharacter(exercises);
            const expeditionIsFinished = this.adventureService.areAllExercisesDone(exercisesExpeditionOfCharacter);
            if (expeditionIsFinished) {
                this.characterAdventureService.createCharacterAdventure(data.expeditionId, data.characterId);
                const expedition = await this.adventureService.findOne(data.expeditionId);
                await this.characterService.addExperience(data.characterId, expedition.experienceBonus);
                return {experience: expedition.experienceBonus};
            } else {
                return null;
            }

            if (!exercises) {
                res.status(404).end();
                return
            }
            res.status(200).json(exercises);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }
}
