import {controller, httpGet, httpPost, request, requestParam, response} from "inversify-express-utils";
import {RoleService} from "../services/role.service";
import {TYPE} from "../constants/types";
import {inject} from "inversify";
import * as express from "express";
import {isBodyString} from "../validators/bodies.validator";
import {isParamNumber} from "../validators/params.validator";

@controller('/roles')
export class RoleController {
    constructor(
        @inject (TYPE.RoleService) private roleService: RoleService
    ) {
    }

    @httpPost('/',
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware,
        isBodyString('name')
    )
    public async postRole(
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const roleName = req.body.name;
            const newRole = await this.roleService.save(roleName);
            if (!newRole) {
                res.status(403).send({error : `Role with name '${roleName}' already exists`});
                return;
            }
            res.status(201).json(newRole);
        } catch (e) {
            res.status(500).send({error: e.message});
        }
    }

    @httpGet('/',
        TYPE.AuthMiddleware
    )
    public async getRoles(
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const roles = await this.roleService.find();
            res.status(200).json(roles);
        } catch (e) {
            res.status(500).send({error : e.message});
        }
    }

    @httpGet('/:id',
        TYPE.AuthMiddleware,
        isParamNumber('id')
    )
    public async getRole(
        @requestParam('id') roleId: number,
        @request() req: express.Request,
        @response() res: express.Response
    ){
        try {
            const role = await this.roleService.findOne(roleId);
            if (!role) {
                res.status(404).send({error: `Role with id '${roleId}' not found`});
                return;
            }
            res.status(200).json(role);
        } catch (e) {
            res.status(500).send({error : e.message});
        }
    }
}
