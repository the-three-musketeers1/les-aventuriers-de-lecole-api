import {controller, httpGet, queryParam, requestParam, response} from "inversify-express-utils";
import {inject} from "inversify";
import {TYPE} from "../constants/types";
import {isParamNumber} from "../validators/params.validator";
import * as express from "express";
import {CharacterExerciseService} from "../services/characterExercise.service";
import {ExerciseService} from "../services/exercise.service";
import {FormatQueryParamMiddleware} from "../middlewares/formatQueryParam.middleware";
import {CharacterService} from "../services/character.service";

@controller('/characterExercise')
export class CharacterExerciseController {
    constructor(
        @inject(TYPE.CharacterExerciseService) private characterExerciseService: CharacterExerciseService,
        @inject(TYPE.ExerciseService) private exerciseService: ExerciseService,
        @inject(TYPE.CharacterService) private guildService: CharacterService
        ) {
    }

    @httpGet('/character/:id', isParamNumber('id'),FormatQueryParamMiddleware.formatstatsRequest())
    public async getQuestionOfExercise(
        @requestParam('id') id: number,
        @queryParam('disciplineId') disciplineId: number,
        @queryParam('chapterId') chapterId: number,
        @queryParam('beginDate') beginDate: number,
        @queryParam('endDate') endDate: string,
        @response() res: express.Response
    ) {
        try {
            const characterAnswers = await this.characterExerciseService.getAllByCharacterIdWithOptions(id, disciplineId, chapterId, beginDate, endDate);
            if (!characterAnswers) {
                res.status(404).end();
                return;
            }
            res.status(200).json(characterAnswers);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }
    @httpGet('/guild/:id', isParamNumber('id'),FormatQueryParamMiddleware.formatstatsRequest())
    public async getQuestionOfExerciseByGuild(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const characterAnswers = await this.characterExerciseService.getAllByGuildId(id);
            if (!characterAnswers) {
                res.status(404).end();
                return;
            }
            res.status(200).json(characterAnswers);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/guild/:id/byCharacter', isParamNumber('id'),FormatQueryParamMiddleware.formatstatsRequest())
    public async getQuestionOfExerciseOfCharactersByGuild(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            let characters = await this.guildService.findCharactersByGuildId(id);
            let answersByCharacters = [];
            for (const character of characters) {
                const characterAnswers = await this.characterExerciseService.getAllByCharacterIdWithOptions(character.id, null, null, null, null);
                answersByCharacters.push({"id" : character.id,"pseudo" : character.pseudo, "firstName": character.first_name, "lastName": character.last_name,"exercisesAnswer" : characterAnswers})
            }
            if (!answersByCharacters) {
                res.status(404).end();
                return;
            }
            res.status(200).json(answersByCharacters);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }
}
