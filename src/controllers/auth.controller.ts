import {
    controller,
    httpDelete,
    httpGet,
    httpPost,
    request,
    requestBody,
    requestParam,
    response
} from "inversify-express-utils";
import {User} from "../entities/User";
import * as express from "express";
import {validationMiddleware} from "../middlewares/validation.middleware";
import {inject} from "inversify";
import {AuthService} from "../services/auth.service";
import {TYPE} from "../constants/types";
import {isBodyString} from "../validators/bodies.validator";
import {PropertiesMiddleware} from "../middlewares/properties.middleware";


@controller('/auth')
export class AuthController {

    constructor(@inject(TYPE.AuthService) private authService: AuthService) {
    }

    @httpGet('/salut')
    public async test(@request() req: express.Request,
                      @response() res: express.Response) {
        res.status(201).json('SALUT')
    }

    @httpPost('/subscribe',
        PropertiesMiddleware.isBodyNotForbiddenProperties(['id']),
        validationMiddleware(User, false, true)
    )
    public async subscribe(
        @requestBody() subscriber: User,
        @response() res: express.Response
    ) {
        try {
            const saveSubscriber = await this.authService.subscribe(subscriber);
            if (!saveSubscriber) {
                res.status(403).end();
                return;
            }
            res.status(201).json(saveSubscriber);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpPost('/login',
        isBodyString('password'),
        isBodyString('email'),
        validationMiddleware(User, true, true)
    )
    public async login(
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const session = await this.authService.login(req.body.password, req.body.email, false);
            if (!session) {
                res.status(404).send({error: 'not subscribe'});
                return;
            }
            res.status(201).json(session);
        } catch (e) {
            if (e.loginError) {
                res.status(403).send(e);
                return;
            }
            res.status(500);
            res.send(e.message);
        }
    }

    @httpPost('/login/teacher',
        isBodyString('password'),
        isBodyString('email'),
        validationMiddleware(User, true, true)
    )
    public async loginTeacher(
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const session = await this.authService.login(req.body.password, req.body.email, true);
            if (!session) {
                res.status(404).send(`User with ${req.body.email} not found`);
                return;
            }
            res.status(201).json(session);
        } catch (e) {
            if (e.loginError) {
                res.status(403).send(e);
                return;
            }
            res.status(500);
            res.send(e.message);
        }
    }

    @httpDelete('/logout',
        TYPE.AuthMiddleware
    )
    public async logout(
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const user = req.body.user as User;
            await this.authService.logout(user);
            res.status(204).end();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }
}
