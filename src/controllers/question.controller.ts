import * as express from "express";
import {
    controller,
    httpDelete,
    httpGet,
    httpPost,
    httpPut,
    requestBody,
    requestParam,
    response
} from "inversify-express-utils";
import {inject} from "inversify";
import {TYPE} from "../constants/types";
import {QuestionService} from "../services/question.service";
import {Question} from "../entities/Question";
import {validationMiddleware} from "../middlewares/validation.middleware";
import {isParamNumber} from "../validators/params.validator";

@controller('/questions')
export class QuestionController {
    constructor(@inject(TYPE.QuestionService) private questionService: QuestionService) {
    }

    @httpPost('/',
        validationMiddleware(Question),
        TYPE.AuthOnlyTeacherMiddleware)
    public async postQuestions(
        @requestBody() newQuestions: Question,
        @response() res: express.Response
    ) {
        try {
            const question = await this.questionService.save(newQuestions);
            res.status(201).json(question);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/')
    public async getQuestions(
        @response() res: express.Response
    ) {
        try {
            return await this.questionService.find();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/:id', isParamNumber('id'))
    public async getQuestion(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const question = await this.questionService.findOne(id);
            if (!question) {
                res.status(404).end();
                return
            }
            res.status(200).json(question);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/sequence-number/:sequenceNumber', isParamNumber('sequenceNumber'))
    public async getQuestionBySequenceNumber(
        @requestParam('sequenceNumber') sequenceNumber: number,
        @response() res: express.Response
    ) {
        try {
            const question = await this.questionService.findBySequenceNumber(sequenceNumber);
            if (!question) {
                res.status(404).end();
                return
            }
            res.status(200).json(question);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/:id/answers',
        isParamNumber('id'),
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware)
    public async getQuestionAnswers(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const answers = await this.questionService.findQuestionAnswers(id);
            if (!answers) {
                res.status(404).end();
                return;
            }
            res.status(200).json(answers);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpPut('/:id',
        isParamNumber('id'),
        validationMiddleware(Question, true),
        TYPE.AuthOnlyTeacherMiddleware)
    public async putQuestion(
        @requestParam('id') id: number,
        @requestBody() question: Question,
        @response() res: express.Response
    ) {
        try {
            const checkQuestion = await this.questionService.update(id, question);
            if (checkQuestion) {
                res.status(204).end();
                return;
            }
            res.status(404).end();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpDelete('/:id',
        isParamNumber('id'),
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware)
    public async deleteQuestion(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            await this.questionService.delete(id);
            const stillExists = await this.questionService.findOne(id);
            if(!stillExists) {
                res.status(204).end();
                return;
            }
            res.status(404).end();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }
}
