import {
    controller, httpDelete,
    httpGet,
    httpPost,
    httpPut, queryParam,
    request,
    requestParam,
    response
} from "inversify-express-utils";
import * as express from "express";
import {GuildService} from "../services/guild.service";
import {inject} from "inversify";
import {TYPE} from "../constants/types";
import {isBodyDateGreaterThan, isBodyString} from "../validators/bodies.validator";
import {isId, isParamNumber} from "../validators/params.validator";
import {User} from "../entities/User";
import {validationMiddleware} from "../middlewares/validation.middleware";
import {Guild} from "../entities/Guild";
import {isQueryString} from "../validators/queries.validator";
import {CharacterService} from "../services/character.service";

@controller('/guild')
export class GuildController {
    guildService: GuildService;
    characterService: CharacterService;

    constructor(
        @inject(TYPE.GuildService) guildService: GuildService,
        @inject(TYPE.CharacterService) characterService: CharacterService
    ) {
        this.guildService = guildService;
        this.characterService = characterService;
    }

    @httpPost('/',
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware,
        validationMiddleware(Guild, true),
        isBodyString('name'),
        isBodyDateGreaterThan(true, 'deadlineAddCharacter')
    )
    public async postGuild(
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const guildName = req.body.name;
            const user = req.body.leader;
            const deadlineAddCharacter = req.body.deadlineAddCharacter as Date;
            const newGuild = await this.guildService.save(guildName, user, deadlineAddCharacter);
            if (newGuild === null) {
                res.status(403).send({error: `guildName '${guildName}' already used`});
                return;
            }
            res.status(201).json(newGuild);
        } catch (e) {
            res.status(500).send(e.message);
        }
    }

    @httpGet('/student',
        TYPE.AuthMiddleware,
        TYPE.AuthStudentMiddleware,
        isQueryString('name'),
        isQueryString('token')
    )
    public async getGuildByNameAndToken(
        @queryParam('name') name: string,
        @queryParam('token') token: string,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const guild = await this.guildService.getGuildByNameAndToken(name, token);
            if (!guild) {
                res.status(404).send({
                    error: `Guild with name '${name}' and token '${token}' not found`
                });
                return;
            }
            res.status(200).json(guild);
        } catch (e) {
            res.status(500).send({error: e.message});
        }
    }

    @httpGet('/',
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware
    )
    public async getGuilds(
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const leader = req.body.user;
            const guilds = await this.guildService.find(leader);
            res.status(200).json(guilds);
            return;
        } catch (e) {
            res.status(500).send({error: e.message});
        }
    }

    @httpGet('/:id',
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware,
        isParamNumber('id')
    )
    public async getGuild(
        @requestParam('id') guildId: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const leader = req.body.user;
            const guild = await this.guildService.findOne(guildId, leader);
            if (!guild) {
                res.status(404).send({error: `guild id '${guildId}' with leader id '${leader.id}' not found`});
                return;
            }
            res.status(200).json(guild);
        } catch (e) {
            res.status(500).send({error: e.message});
        }

    }

    @httpPut('/:id',
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware,
        validationMiddleware(Guild, true),
        isParamNumber('id'),
        isBodyString('name'),
        isBodyDateGreaterThan(
            true,
            'deadlineAddCharacter'
        )
    )
    public async putGuild(
        @requestParam('id') guildId: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        const leader = req.body.leader;
        const guildName = req.body.name;
        const deadlineAddCharacter = req.body.deadlineAddCharacter as Date;
        try {
            const isUpdate = await this.guildService.update(guildId, guildName, leader, deadlineAddCharacter);
            if (!isUpdate) {
                res.status(404).send({error: `guild id '${guildId}' with leader id '${leader.id}' not found`});
                return;
            }
            res.status(204).end();
        } catch (e) {
            console.log(e);
            res.status(500).send({error: e.message});
        }
    }

    @httpDelete('/:id',
        TYPE.AuthOnlyTeacherMiddleware,
        isParamNumber('id')
    )
    public async deleteGuild(
        @requestParam('id') guildId: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const isDeleted = await this.guildService.delete(guildId);
            if (!isDeleted) {
                res.status(404).send({error: `guild id '${guildId}' not found`});
                return;
            }
            res.status(204).end();
        } catch (e) {
            res.status(500).send(e.message);
        }
    }

    @httpPost('/:guildId/characters',
        TYPE.AuthMiddleware,
        TYPE.AuthStudentMiddleware,
        isParamNumber('guildId')
    )
    public async postGuildCharacter(
        @requestParam('guildId') guildId: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const student = req.body.user as User;
            const characterName = req.body.name as string;
            const roleId = req.body.roleId as number;
            const newCharacter = await this.characterService.saveByGuildAndRole(student, characterName, guildId, roleId);

            res.status(201).json(newCharacter);
        } catch (e) {
            if (e.guildError || e.roleError) {
                res.status(404).send(e);
            }
            if (e.guildDeadlineError || e.characterAlreadyCreatedError) {
                res.status(403).send(e);
            }
            res.status(500).send(e);
        }
    }

    @httpGet('/leader/:id',
        isParamNumber('id'),
        TYPE.AuthOnlyTeacherMiddleware)
    public async getGuildsOfLeader(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const guilds = await this.guildService.findGuildsByLeaderId(id);
            if (!guilds) {
                res.status(404).end();
                return;
            }
            res.status(200).json(guilds);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/:id/members',
        isParamNumber('id'),
        TYPE.AuthOnlyTeacherMiddleware)
    public async getMembersOfGuild(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const members = await this.characterService.findCharactersByGuildId(id);
            if (!members) {
                res.status(404).end();
                return;
            }
            res.status(200).json(members);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/:id/token/:token',
        isParamNumber('id'),
        isId('id'),
        TYPE.AuthMiddleware,
        TYPE.AuthStudentMiddleware)
    public async getGuildByIdAndToken(
        @requestParam('id') guildId: number,
        @requestParam('token') token: string,
        @response() res: express.Response
    ) {
        try {
            const checkGuild = await this.guildService.findGuildByIdAndToken(+guildId, token);
            if (checkGuild === undefined) {
                res.status(404).send({guildNotFound: 'Guild not found'});
                return;
            }
            res.status(200).json(guildId);
        } catch (e) {
            res.status(500).send(e);
        }
    }
}
