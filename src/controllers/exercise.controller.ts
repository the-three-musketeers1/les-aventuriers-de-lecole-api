import {
    controller,
    httpDelete,
    httpGet,
    httpPost,
    httpPut, request,
    requestBody,
    requestParam,
    response
} from "inversify-express-utils";
import {inject} from "inversify";
import {TYPE} from "../constants/types";
import {ExerciseService} from "../services/exercise.service";
import {isParamNumber} from "../validators/params.validator";
import * as express from "express";
import {Exercise} from "../entities/Exercise";
import {validationMiddleware} from "../middlewares/validation.middleware";

@controller('/exercise')
export class ExerciseController {
    constructor(@inject(TYPE.ExerciseService) private exerciseService: ExerciseService) {
    }

    @httpGet('/:id/expedition', isParamNumber('id'))
    public async getExerciseExpeditions(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const expedition = await this.exerciseService.findExerciseExpeditions(id);
            if (!expedition) {
                res.status(404).end();
                return;
            }
            res.status(200).json(expedition);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/:id/questions/character/:characterId', isParamNumber('id'), isParamNumber('characterId'))
    public async getQuestionOfExercise(
        @requestParam('id') id: number,
        @requestParam('characterId') characterId: number,
        @response() res: express.Response
    ) {
        try {
            const authorizedExercise = await this.exerciseService.findIfExerciseIsLinkedToCharacter(id, characterId);
            if (authorizedExercise.length == 0) {
                res.status(404).end();
            }
            const exercise = await this.exerciseService.findExerciseWithQuestions(id);
            if (!exercise) {
                res.status(404).end();
                return;
            }
            res.status(200).json(exercise);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/expedition/:expeditionId/character/:characterId',
        TYPE.AuthMiddleware,
        TYPE.AuthStudentMiddleware,
        isParamNumber('expeditionId'), isParamNumber('characterId'))
    public async getExercisesOfExpeditionOfCharacter(
        @requestParam('expeditionId') expeditionId: number,
        @requestParam('characterId') characterId: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const adventure = await this.exerciseService.findIfExpeditionLinkedToUser(expeditionId, characterId);
            if(adventure.length == 0) {
                res.status(404).end();
                return;
            }
            const exercises = await this.exerciseService.findExercisesOfExpeditionsWithCharacterContent(expeditionId, characterId);

            const exercisesExpeditionOfCharacter = this.exerciseService.transformListOfExercisesIntoExercisesExpeditionsOfCharacter(exercises);
            if (!exercisesExpeditionOfCharacter) {
                res.status(404).end();
                return;
            }
            res.status(200).json(exercisesExpeditionOfCharacter);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/expedition/:id', isParamNumber('id'))
    public async getExercisesOfExpedition(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const exercises = await this.exerciseService.findExercisesOfExpeditions(id);
            const exerciseInformations = this.exerciseService.transformExercisesIntoExercisesExpeditions(exercises);
            if (!exerciseInformations) {
                res.status(404).end();
                return;
            }
            res.status(200).json(exercises);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/guild/:id',
        isParamNumber('id'),
        TYPE.AuthOnlyTeacherMiddleware)
    public async getExercisesOfGuild(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const exercises = await this.exerciseService.findExercisesOfGuild(id);
            if (!exercises) {
                res.status(404).end();
                return;
            }
            res.status(200).json(exercises);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpPost('/',
        validationMiddleware(Exercise, true),
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware
    )
    public async postExercises(
        @request() req: express.Request ,
        @requestBody() newExercises: Exercise,
        @response() res: express.Response
    ) {
        try {
            newExercises.leader = req.body.user;
            const exercise = await this.exerciseService.save(newExercises);
            res.status(201).json(exercise);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/')
    public async getExercises(
        @response() res: express.Response
    ) {
        try {
            return await this.exerciseService.find();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/:id', isParamNumber('id'))
    public async getExercise(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const exercise = await this.exerciseService.findOne(id);
            if (!exercise) {
                res.status(404).end();
                return
            }
            res.status(200).json(exercise);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/adventure/:id', isParamNumber('id'))
    public async getExercisesByAdventure(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const exercise = await this.exerciseService.findByAdventure(id);
            if (!exercise) {
                res.status(404).end();
                return
            }
            res.status(200).json(exercise);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/subject/:id', isParamNumber('id'))
    public async getExercisesBySubject(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const exercise = await this.exerciseService.findBySubject(id);
            if (!exercise) {
                res.status(404).end();
                return;
            }
            res.status(200).json(exercise);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/:id/only-questions',
        isParamNumber('id'),
        TYPE.AuthOnlyTeacherMiddleware)
    public async getExerciseQuestions(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const questions = await this.exerciseService.findExerciseQuestions(id);
            if (!questions) {
                res.status(404).end();
                return;
            }
            res.status(200).json(questions);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    /*@httpGet('/expedition/:id', isParamNumber('id')) TODO
    public async getExercisesByExpedition(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const exercise = await this.exerciseService.findByExpedition(id);
            if (!exercise) {
                res.status(404).end();
                return
            }
            res.status(200).json(exercise);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }*/

    @httpPut('/:id',
        isParamNumber('id'),
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware,
        validationMiddleware(Exercise),
    )
    public async putExercise(
        @requestParam('id') id: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            req.body.leader = req.body.user;
            delete req.body.user;
            const checkExercise = await this.exerciseService.update(id, req.body);
            if (checkExercise) {
                res.status(204).end();
                return;
            }
            res.status(404).end();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpDelete('/:id',
        isParamNumber('id'),
        TYPE.AuthOnlyTeacherMiddleware
    )
    public async deleteExercise(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const deleted = await this.exerciseService.delete(id);
            if (deleted) {
                res.status(204).end();
                return;
            }
            res.status(404).end();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

}
