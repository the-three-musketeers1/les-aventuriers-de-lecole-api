import {
    controller, httpDelete,
    httpGet,
    httpPut,
    request,
    requestBody,
    requestParam,
    response
} from "inversify-express-utils";
import {inject} from "inversify";
import {TYPE} from "../constants/types";
import {CharacterService} from "../services/character.service";
import {isParamNumber} from "../validators/params.validator";
import * as express from "express";
import {ExperienceToAdd} from "../entities/dto/ExperienceToAdd";
import {User} from "../entities/User";

@controller('/character')
export class CharacterController {
    constructor(
        @inject(TYPE.CharacterService) private characterService: CharacterService,
    ) {

    }

    @httpGet('/student',
        TYPE.AuthMiddleware,
        TYPE.AuthStudentMiddleware
    )
    public async getStudentCharacters(
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const student = req.body.user as User;
            const characters = await this.characterService.findStudentCharacters(student);
            res.status(200).json(characters);
        } catch (e) {
            res.status(500).send(e);
        }
    }

    @httpGet('/:characterId/student',
        TYPE.AuthMiddleware,
        TYPE.AuthStudentMiddleware,
        isParamNumber('characterId')
    )
    public async getStudentCharacter(
        @requestParam('characterId') characterId: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const student = req.body.user as User;
            const character = await this.characterService.findOneStudentCharacter(characterId, student);
            if (character === undefined) {
                res.status(404).send({error: `Character with id '${characterId}' not found`});
                return;
            }
            res.status(200).json(character);
        } catch (e) {
            res.status(500).send(e);
        }
    }

    @httpGet('/:characterId/leader',
        isParamNumber('characterId')
    )
    public async getCharacterLeaders(
        @requestParam('characterId') characterId: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const leader = await this.characterService.findCharacterLeader(characterId);
            if (leader == null) {
                res.status(404).send({error: `Character with id '${characterId}' not found`});
                return;
            }
            res.status(200).json(leader);
        } catch (e) {
            res.status(500).send(e);
        }
    }

    @httpGet('/:id', isParamNumber('id'))
    public async getCharacterInformation(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const informations = await this.characterService.getCharacterInformations(id);
            if (!informations) {
                res.status(404).end();
                return;
            }
            res.status(200).json(this.characterService.mapCharacterInfo(informations));
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpPut('/:id/experience', isParamNumber('id'))
    public async addExperienceToCharacter(
        @requestParam('id') id: number,
        @requestBody() data: ExperienceToAdd,
        @response() res: express.Response
    ) {
        try {
            const result = await this.characterService.addExperience(id, data.experience);
            if (result.raw.affectedRows == 0) {
                res.status(404).end();
                return;
            }
            res.status(200);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpDelete('/:characterId',
        isParamNumber('characterId'),
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware
    )
    public async deleteGuildCharacter(
        @requestParam('characterId') id: number,
        @response() res: express.Response
    ) {
        try {
            const deleted = await this.characterService.delete(id);
            if (deleted) {
                res.status(204).end();
                return;
            }
            res.status(404).end();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }
}
