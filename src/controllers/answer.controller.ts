import * as express from "express";
import {
    controller,
    httpDelete,
    httpGet,
    httpPost,
    httpPut,
    requestBody,
    requestParam,
    response
} from "inversify-express-utils";
import {inject} from "inversify";
import {TYPE} from "../constants/types";
import {AnswerService} from "../services/answer.service";
import {Answer} from "../entities/Answer";
import {validationMiddleware} from "../middlewares/validation.middleware";
import {isParamNumber} from "../validators/params.validator";

@controller('/answers')
export class AnswerController {
    constructor(@inject(TYPE.AnswerService) private answerService: AnswerService) {
    }

    @httpPost('/',
        validationMiddleware(Answer),
        TYPE.AuthOnlyTeacherMiddleware)
    public async postAnswers(
        @requestBody() newAnswers: Answer,
        @response() res: express.Response
    ) {
        try {
            const answer = await this.answerService.save(newAnswers);
            res.status(201).json(answer);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    /*@httpPost('/answers', validationMiddleware(Answer))
    public async postAnswers(
        @requestBody() newAnswer: Answer,
        @response() res: express.Response
    ) {
        try {
            const answer = await this.answerService.saveAnswer(newAnswer);
            res.status(201).json(answer);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }*/

    @httpGet('/question/:id', isParamNumber('id'))
    public async getAnswersByQuestion(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const answers = await this.answerService.findByQuestion(id);
            if (!answers) {
                res.status(404).end();
                return;
            }
            res.status(200).json(answers);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/:id', isParamNumber('id'))
    public async getAnswer(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const answer = await this.answerService.findOne(id);
            if (!answer) {
                res.status(404).end();
                return
            }
            res.status(200).json(answer);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpPut('/:id',
        isParamNumber('id'),
        validationMiddleware(Answer, true),
        TYPE.AuthOnlyTeacherMiddleware)
    public async putAnswer(
        @requestParam('id') id: number,
        @requestBody() answer: Answer,
        @response() res: express.Response
    ) {
        try {
            const checkAnswer = await this.answerService.update(id, answer);
            if (checkAnswer) {
                res.status(204).end();
                return;
            }
            res.status(404).end();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpDelete('/:id',
        isParamNumber('id'),
        TYPE.AuthOnlyTeacherMiddleware)
    public async deleteAnswer(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const deleted = await this.answerService.delete(id);
            if (deleted) {
                res.status(204).end();
                return;
            }
            res.status(404).end();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }
}
