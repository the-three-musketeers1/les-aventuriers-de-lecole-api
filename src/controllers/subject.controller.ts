import * as express from "express";
import {
    controller,
    httpDelete,
    httpGet,
    httpPost,
    httpPut, request,
    requestBody,
    requestParam,
    response
} from "inversify-express-utils";
import {inject} from "inversify";
import {TYPE} from "../constants/types";
import {Subject} from "../entities/Subject";
import {validationMiddleware} from "../middlewares/validation.middleware";
import {isParamNumber} from "../validators/params.validator";
import {SubjectService} from "../services/subject.service";
import {User} from "../entities/User";

@controller('/subjects')
export class SubjectController {
    constructor(@inject(TYPE.SubjectService) private subjectService: SubjectService) {
    }

    @httpPost('/',
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware,
        validationMiddleware(Subject)
    )
    public async postSubjects(
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const leader = req.body.user as User;
            const newSubject = {
                title: req.body.title,
                description: req.body.description,
            } as Subject;

            const subject = await this.subjectService.save(newSubject, leader);
            if (subject === null) {
                res.status(403).send({error: `subject with ${leader.id} is already created`});
            }
            res.status(201).json(subject);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/')
    public async getSubjects(
        @response() res: express.Response
    ) {
        try {
            return await this.subjectService.find();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/:id', isParamNumber('id'))
    public async getSubject(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const subject = await this.subjectService.findOne(id);
            if (!subject) {
                res.status(404).end();
                return
            }
            res.status(200).json(subject);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpPut('/:id',
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware,
        isParamNumber('id'),
        validationMiddleware(Subject, true),
    )
    public async putSubject(
        @requestParam('id') id: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const subject = {
                title: req.body.title,
                description: req.body.description
            } as Subject;
            const leader = req.body.user as User;
            const checkSubject = await this.subjectService.update(id, subject, leader);
            if (checkSubject) {
                res.status(204).end();
                return;
            }
            res.status(404).end();
        } catch (e) {
            if (e.leaderError) {
                res.status(403).send(e);
            }
            res.status(500);
            res.send(e.message);
        }
    }

    @httpDelete('/:id', isParamNumber('id'))
    public async deleteSubject(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const result = await this.subjectService.delete(id);
            res.status(204).end();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/leader/:id', isParamNumber('id'))
    public async getDisciplineByLeaderId(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const subject = await this.subjectService.findDisciplineByLeaderId(id);
            if (!subject) {
                res.status(404).end();
                return
            }
            res.status(200).json(subject);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/guild/:id', isParamNumber('id'))
    public async getDisciplineByGuildId(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const subject = await this.subjectService.findDisciplineByGuildId(id);
            if (!subject) {
                res.status(404).end();
                return
            }
            res.status(200).json(subject);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/character/:id', isParamNumber('id'))
    public async getDisciplineByCharacterId(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const subject = await this.subjectService.findDisciplineByCharacterId(id);
            if (!subject) {
                res.status(404).end();
                return
            }
            res.status(200).json(subject);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/disciplineChapter/:id', isParamNumber('id'))
    public async getChapterByDisciplineId(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const subject = await this.subjectService.findChapterByDisciplineId(id);
            if (!subject) {
                res.status(404).end();
                return
            }
            res.status(200).json(subject);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpPost('/disciplines/:id/chapters',
        validationMiddleware(Subject),
        isParamNumber('id'),
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware
    )
    public async postDisciplineChapter(
        @requestParam('id') id: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        const leader = req.body.user;
        const chapterToSave = {
            title: req.body.title,
            description: req.body.description
        } as Subject;
        try {
            const newChapter = await this.subjectService.saveChapter(chapterToSave, id, leader.id);
            res.status(201).json(newChapter);
        } catch (e) {
            if (e.notFoundError) {
                res.status(404).send(e);
                return;
            }
            if (e.forbiddenError) {
                res.status(403).send(e);
                return;
            }
            res.status(500).end();
        }
    }

    @httpGet('/disciplines/:id/chapters',
        TYPE.AuthMiddleware,
        isParamNumber('id')
    )
    public async getDisciplineChapters(
        @requestParam('id') id: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const chapters = await this.subjectService.findChaptersByDisciplineId(id);
            res.status(200).json(chapters);
        } catch (e) {
            if (e.notFoundError) {
                res.status(404);
                res.send(e);
                return;
            }
            if (e.forbiddenError) {
                res.status(403);
                res.send(e);
                return;
            }
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/disciplines/:disciplineId/chapters/:chapterId',
        TYPE.AuthMiddleware,
        isParamNumber('disciplineId'),
        isParamNumber('chapterId')
    )
    public async getDisciplineChapter(
        @requestParam('disciplineId') disciplineId: number,
        @requestParam('chapterId') chapterId: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const chapter = await this.subjectService.findOneChapter(+disciplineId, +chapterId);
            res.status(200).json(chapter);
        } catch (e) {
            if (e.notFoundError) {
                res.status(404).send(e);
                return;
            }
            res.status(500).end();
        }
    }

    @httpPut('/disciplines/:disciplineId/chapters/:chapterId',
        validationMiddleware(Subject, true, true),
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware,
        isParamNumber('disciplineId'),
        isParamNumber('chapterId')
    )
    public async putDisciplineChapter(
        @requestParam('disciplineId') disciplineId: number,
        @requestParam('chapterId') chapterId: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const leaderId = req.body.user.id;
            const chapter = {
                title: req.body.title,
                description: req.body.description
            } as Subject;
            const updateResult = await this.subjectService.updateChapter(+disciplineId, +chapterId, chapter, leaderId);
            if (updateResult.raw.affectedRows > 1) {
                res.status(500).send({serverError: 'update one chapter affected more than one row of subject table'});
                return;
            }
            res.status(204).end();
        } catch (e) {
            if (e.notFoundError) {
                res.status(404).send(e);
                return;
            }
            if (e.leaderError) {
                res.status(403).send(e);
                return;
            }
            res.status(500).end();
        }
    }

    @httpDelete('/disciplines/:disciplineId/chapters/:chapterId',
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware,
        isParamNumber('disciplineId'),
        isParamNumber('chapterId')
    )
    public async deleteDisciplineChapter(
        @requestParam('disciplineId') disciplineId: number,
        @requestParam('chapterId') chapterId: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        const leaderId = req.body.user.id as number;
        try {
            const resultDelete = await this.subjectService.deleteChapter(+disciplineId, +chapterId, leaderId);
            if (resultDelete.raw.affectedRows !== 1) {
                res.status(500).end();
            }
            res.status(204).end();
        } catch (e) {
            if (e.notFoundError) {
                res.status(404).send(e);
                return;
            }
            if (e.leaderError) {
                res.status(403).send(e);
                return;
            }
            res.status(500).end();
        }
    }
}
