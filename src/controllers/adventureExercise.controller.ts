import * as express from "express";
import {
    controller, httpDelete,
    httpGet, httpPost, request, requestBody,
    requestParam,
    response
} from "inversify-express-utils";
import {inject} from "inversify";
import {TYPE} from "../constants/types";
import {AdventureExerciseService} from "../services/adventureExercise.service";
import {validationMiddleware} from "../middlewares/validation.middleware";
import {AdventureExercise} from "../entities/AdventureExercise";
import {isParamNumber} from "../validators/params.validator";

@controller('/adventures-exercises')
export class AdventureExerciseController {
    constructor(@inject(TYPE.AdventureExerciseService) private adventureExerciseService: AdventureExerciseService) {
    }

    @httpGet('/:leaderId', TYPE.AuthOnlyTeacherMiddleware)
    public async getAdventureExercisesByLeader(
        @response() res: express.Response,
        @requestParam('leaderId') leaderId: number
    ) {
        try {
            const adventureExercises = await this.adventureExerciseService.findByLeader(leaderId);
            if(!adventureExercises) {
                res.status(404).send({
                    error: "No expedition exercises"
                });
            }
            res.status(200).json(adventureExercises);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpPost('/',
        validationMiddleware(AdventureExercise, true),
        TYPE.AuthOnlyTeacherMiddleware
    )
    public async postExpeditionExercise(
        @request() req: express.Request ,
        @requestBody() newExpeditionExercise: AdventureExercise,
        @response() res: express.Response
    ) {
        try {
            const expeditionExercise = await this.adventureExerciseService.save(newExpeditionExercise);
            res.status(201).json(expeditionExercise);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpDelete('/:id',
        isParamNumber('id'),
        TYPE.AuthOnlyTeacherMiddleware
    )
    public async deleteExpeditionExercise(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const deleted = await this.adventureExerciseService.delete(id);
            if (deleted) {
                res.status(204).end();
                return;
            }
            res.status(404).end();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }
}
