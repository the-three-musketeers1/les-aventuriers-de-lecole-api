import {
    controller,
    httpGet, httpPost, httpPut,
    request,
    requestParam,
    response
} from "inversify-express-utils";
import {inject} from "inversify";
import {TYPE} from "../constants/types";
import {TicketService} from "../services/ticket.service";
import * as express from "express";
import {isBodyDefined, isBodyString} from "../validators/bodies.validator";
import {CharacterRepository} from "../repositories/CharacterRepository";
import {MessageService} from "../services/message.service";
import {User} from "../entities/User";
import {isParamNumber} from "../validators/params.validator";
import {Status} from "../entities/Status";
import {validationMiddleware} from "../middlewares/validation.middleware";
import {Ticket} from "../entities/Ticket";

@controller('/ticket')
export class TicketController {
    constructor(
        @inject(TYPE.TicketService) private ticketService: TicketService,
        @inject(TYPE.MessageService) private messageService: MessageService,
        @inject(TYPE.CharacterRepository) private characterRepository: CharacterRepository
    ) {
    }

    @httpPost('/',
        TYPE.AuthMiddleware,
        TYPE.AuthCharacterMiddleware,
        isBodyString('title'),
        isBodyString('content')
    )
    public async postTicket(
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const title = req.body.title;
            const content = req.body.content;
            const character = req.body.character;
            let ticket = await this.ticketService.save(title, character);
            if (ticket === null) {
                res.status(403).send();
                return;
            }
            const message = this.messageService.save(ticket.id, content, true, character.id);
            if (message === null) {
                res.status(403).send();
                return;
            }
            res.status(201).json(ticket);
        } catch (e) {
            res.status(500).send(e.message);
        }
    }

    @httpGet('/character/:characterId',
        TYPE.AuthMiddleware,
        TYPE.AuthCharacterMiddleware
    )
    public async getCharacterTickets(
        @requestParam('characterId') characterId: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const character = req.body.character;
            const tickets = await this.ticketService.findCharacterTickets(character);
            res.status(200).json(tickets);
        } catch (e) {
            res.status(500).send(e);
        }
    }

    @httpGet('/:ticketId/messages/character/:characterId',
        TYPE.AuthMiddleware,
        TYPE.AuthCharacterMiddleware
    )
    public async getTicketMessagesByCharacter(
        @requestParam('ticketId') ticketId: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const messages = await this.ticketService.findTicketMessagesByCharacter(ticketId, req.body.character);
            if(messages === null) {
                res.status(403).send();
                return;
            }
            res.status(200).json(messages);
        } catch (e) {
            res.status(500).send(e);
        }
    }

    @httpGet('/:ticketId/messages/leader',
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware
    )
    public async getTicketMessagesByTeacher(
        @requestParam('ticketId') ticketId: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const messages = await this.ticketService.findTicketMessagesByLeader(ticketId, req.body.user);
            res.status(200).json(messages);
        } catch (e) {
            res.status(500).send(e);
        }
    }

    @httpGet('/leader',
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware
    )
    public async getTicketsLeader(
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        const leader = req.body.user as User;
        try {
            const tickets = await this.ticketService.findLeaderTickets(leader);
            res.status(200).json(tickets);
        } catch (e) {
            res.status(500).end();
        }
    }

    @httpPut('/:id/status',
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware,
        validationMiddleware(Ticket, true),
        isParamNumber('id'),
        isBodyDefined('status', true)
    )
    public async putTicketLeaderStatus(
        @requestParam('id') id: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        const leader = req.body.user as User;
        const newStatus = req.body.status as Status;
        try {
            const checkUpdate = await this.ticketService.updateTicketLeaderStatus(+id, leader, newStatus);
            if (checkUpdate === false) {
                res.status(404).send({notFoundError: 'Ticket not found'});
                return;
            }
            res.status(204).end();
        } catch (e) {
            res.status(500).end();
        }
    }

    @httpGet('/:id/leader',
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware,
        isParamNumber('id')
    )
    public async getTicketLeader(
        @requestParam('id') id: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        const leader = req.body.user as User;
        try {
            const ticket = await this.ticketService.findLeaderTicket(+id, leader);
            if (!ticket) {
                res.status(404).send({errorNotFound: 'Ticket not found'});
                return;
            }
            res.status(200).json(ticket);
        } catch (e) {
            res.status(500).end();
        }
    }

    @httpPost('/:id/leader/message',
        TYPE.AuthMiddleware,
        TYPE.AuthTeacherMiddleware,
        isParamNumber('id'),
        isBodyString('content')
    )
    public async postTicketMessage(
        @requestParam('id') id: number,
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        const content = req.body.content as string;
        try {
            const message = await this.messageService.saveByTeacher(id, content, false, req.body.user.id);
            if (message === null) {
                res.status(404).send({notFoundError: 'Ticket not found'});
                return;
            }
            res.status(201).json(message);
        } catch (e) {
            res.status(500).send(e.message);
        }
    }
}
