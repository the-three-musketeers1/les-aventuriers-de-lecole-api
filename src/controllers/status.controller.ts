import {controller, httpGet, request, response} from "inversify-express-utils";
import {StatusService} from "../services/status.service";
import {TYPE} from "../constants/types";
import {inject} from "inversify";
import * as express from "express";

@controller('/status')
export class StatusController {
    constructor(
        @inject (TYPE.StatusService) private statusService: StatusService
    ) {
    }

    @httpGet('/',
        TYPE.AuthMiddleware
    )
    public async getStatuses(
        @request() req: express.Request,
        @response() res: express.Response
    ) {
        try {
            const statuses = await this.statusService.find();
            res.status(200).json(statuses);
        } catch (e) {
            res.status(500).send({error : e.message});
        }
    }
}
