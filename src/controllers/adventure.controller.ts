import * as express from "express";
import {
    controller,
    httpDelete,
    httpGet,
    httpPost,
    httpPut,
    requestBody,
    requestParam,
    response
} from "inversify-express-utils";
import {inject} from "inversify";
import {TYPE} from "../constants/types";
import {Adventure} from "../entities/Adventure";
import {validationMiddleware} from "../middlewares/validation.middleware";
import {isParamNumber} from "../validators/params.validator";
import {AdventureService} from "../services/adventure.service";

@controller('/adventures')
export class AdventureController {
    constructor(@inject(TYPE.AdventureService) private adventureService: AdventureService) {
    }

    @httpPost('/',
        validationMiddleware(Adventure),
        TYPE.AuthOnlyTeacherMiddleware)
    public async postAdventures(
        @requestBody() newAdventures: Adventure,
        @response() res: express.Response
    ) {
        try {
            const adventure = await this.adventureService.save(newAdventures);
            res.status(201).json(adventure);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/')
    public async getAdventures(
        @response() res: express.Response
    ) {
        try {
            return await this.adventureService.find();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/expeditions')
    public async getExpeditions(
        @response() res: express.Response
    ) {
        try {
            return await this.adventureService.findExpeditions();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/expeditions/available/character/:id', isParamNumber('id'))
    public async getAvailableExpeditionsByCharacter(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const expeditions = await this.adventureService.findExpeditionsAvailableByCharacterId(id);
            return this.adventureService.mapToExpeditionsInformations(expeditions);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/expeditions/character/:id', isParamNumber('id'))
    public async getAllExpeditionsByCharacter(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const expeditions = await this.adventureService.findExpeditionsByCharacterId(id);
            return this.adventureService.mapToExpeditionsInformations(expeditions);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/:id', isParamNumber('id'))
    public async getAdventure(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const adventure = await this.adventureService.findOne(id);
            if (!adventure) {
                res.status(404).end();
                return
            }
            res.status(200).json(adventure);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/expeditions/:id', isParamNumber('id'))
    public async getExpedition(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const expedition = await this.adventureService.findOneExpedition(id);
            if (!expedition) {
                res.status(404).end();
                return
            }
            res.status(200).json(expedition);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpPut('/:id',
        isParamNumber('id'),
        validationMiddleware(Adventure, true),
        TYPE.AuthOnlyTeacherMiddleware)
    public async putAdventure(
        @requestParam('id') id: number,
        @requestBody() adventure: Adventure,
        @response() res: express.Response
    ) {
        try {
            const checkAdventure = await this.adventureService.update(id, adventure);
            if (checkAdventure) {
                res.status(204).end();
                return;
            }
            res.status(404).end();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpDelete('/:id',
        isParamNumber('id'),
        TYPE.AuthOnlyTeacherMiddleware)
    public async deleteAdventure(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const deleted = await this.adventureService.delete(id);
            if (deleted) {
                res.status(204).end();
                return;
            }
            res.status(404).end();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }
}
