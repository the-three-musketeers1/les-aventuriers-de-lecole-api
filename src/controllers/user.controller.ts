import {UserService} from "../services/user.service";
import {TYPE} from "../constants/types";
import {User} from "../entities/User";
import {
    controller,
    httpDelete,
    httpGet,
    httpPost,
    httpPut,
    requestBody,
    requestParam,
    response
} from "inversify-express-utils";
import * as express from "express";
import {inject} from "inversify";
import {validationMiddleware} from "../middlewares/validation.middleware";
import {isParamNumber} from "../validators/params.validator";

@controller('/users')
export class UserController {
    userService: UserService;

    constructor(@inject(TYPE.UserService) userService: UserService) {
        this.userService = userService;
    }

    @httpPost("/", validationMiddleware(User))
    public async postUser(
        @requestBody() newUser: User,
        @response() res: express.Response
    ) {
        try {
            const user = await this.userService.save(newUser);
            res.status(201).json(user);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/')
    public async getUsers(
        @response() res: express.Response
    ) {
        try {
            console.log("tu aimes les email james ?");
            return await this.userService.find();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/:id', isParamNumber('id'))
    public async getUser(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const result = await this.userService.findOne(id);
            if (!result) {
                res.status(404).end();
                return;
            }
            res.status(200).json(result);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/:id/:otherId', isParamNumber('id'))
    public async getUserTest(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const result = await this.userService.findOne(id);
            if (!result) {
                res.status(404).end();
                return;
            }
            res.status(200).json(result);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpPut('/:id', isParamNumber('id'), validationMiddleware(User, true))
    public async updateUser(
        @requestParam('id') id: number,
        @requestBody() user: User,
        @response() res: express.Response
    ) {
        try {
            const result: boolean = await this.userService.update(id, user);
            if (result) {
                res.status(204).end();
                return;
            }
            res.status(404).end();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpDelete('/:id', isParamNumber('id'))
    public async deleteUser(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        if (isNaN(id)) {
            res.status(400).end();
            return;
        }
        try {
            const result = await this.userService.delete(id);
            if (result) {
                res.status(204).end();
                return;
            }
            res.status(404).end();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }


    @httpGet('/leaders')
    public async getLeaders(
        @response() res: express.Response
    ) {
        try {
            return await this.userService.findLeader();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/leaders/one/:id',
        isParamNumber('id'),
        TYPE.AuthOnlyTeacherMiddleware)
    public async getLeader(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {

            const leader = await this.userService.findOneLeader(id);
            if (!leader) {
                res.status(404).end();
                return
            }
            res.status(200).json(leader);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/leaders/:id/exercises',
        isParamNumber('id'),
        TYPE.AuthOnlyTeacherMiddleware)
    public async getLeaderExercises(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const exercises = await this.userService.findLeaderExercises(id);
            if (!exercises) {
                res.status(404).end();
                return;
            }
            res.status(200).json(exercises);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/leaders/:id/adventures', isParamNumber('id'))
    public async getLeaderAdventures(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const adventures = await this.userService.findLeaderAdventures(id);
            if (!adventures) {
                res.status(404).end();
                return;
            }
            res.status(200).json(adventures);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/leaders/:id/subjects',
        isParamNumber('id'),
        TYPE.AuthOnlyTeacherMiddleware)
    public async getLeaderSubjects(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const subjects = await this.userService.findLeaderSubjects(id);
            if (!subjects) {
                res.status(404).end();
                return;
            }
            res.status(200).json(subjects);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/leaders/:id/expeditions',
        isParamNumber('id'),
        TYPE.AuthOnlyTeacherMiddleware)
    public async getLeaderExpeditions(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const expeditions = await this.userService.findLeaderExpeditions(id);
            if (!expeditions) {
                res.status(404).end();
                return;
            }
            res.status(200).json(expeditions);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/leaders/:leaderId/subject/:subjectId/exercises',
        isParamNumber('leaderId'),
        isParamNumber('subjectId'),
        TYPE.AuthOnlyTeacherMiddleware)
    public async getLeaderExercisesBySubject(
        @requestParam('leaderId') leaderId: number,
        @requestParam('subjectId') subjectId: number,
        @response() res: express.Response
    ) {
        try {
            const exercises = await this.userService.findLeaderExercisesBySubject(leaderId, subjectId);
            if (!exercises) {
                res.status(404).end();
                return;
            }
            res.status(200).json(exercises);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/leaders/:leaderId/adventure/:adventureId/exercises',
        isParamNumber('leaderId'),
        isParamNumber('adventureId'))
    public async getLeaderExercisesByAdventure(
        @requestParam('leaderId') leaderId: number,
        @requestParam('adventureId') adventureId: number,
        @response() res: express.Response
    ) {
        try {
            const exercises = await this.userService.findLeaderExercisesByAdventure(leaderId, adventureId);
            if (!exercises) {
                res.status(404).end();
                return;
            }
            res.status(200).json(exercises);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/leaders/:leaderId/expedition/:expeditionId/exercises',
        isParamNumber('leaderId'),
        isParamNumber('expeditionId'),
        TYPE.AuthOnlyTeacherMiddleware)
    public async getLeaderExercisesByExpedition(
        @requestParam('leaderId') leaderId: number,
        @requestParam('expeditionId') expeditionId: number,
        @response() res: express.Response
    ) {
        try {
            const exercises = await this.userService.findLeaderExercisesByExpedition(leaderId, expeditionId);
            if (!exercises) {
                res.status(404).end();
                return;
            }
            res.status(200).json(exercises);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }
}
