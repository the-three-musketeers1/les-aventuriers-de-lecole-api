import * as express from "express";
import {
    controller,
    httpDelete,
    httpGet,
    httpPost,
    httpPut,
    requestBody,
    requestParam,
    response
} from "inversify-express-utils";
import {inject} from "inversify";
import {TYPE} from "../constants/types";
import {validationMiddleware} from "../middlewares/validation.middleware";
import {isParamNumber} from "../validators/params.validator";
import {GuildAdventureService} from "../services/guildAdventure.service";
import {GuildAdventure} from "../entities/GuildAdventure";
import {isBodyDateGreaterThan} from "../validators/bodies.validator";

@controller('/guild-adventure')
export class GuildAdventureController {
    constructor(@inject(TYPE.GuildAdventureService) private guildAdventureService: GuildAdventureService) {
    }

    @httpGet('/expeditions/leader/:id',
        isParamNumber('id'),
        TYPE.AuthOnlyTeacherMiddleware)
    public async getLeaderGuildExpeditions(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const guildExpeditions = await this.guildAdventureService.findGuildExpeditionsByLeader(id);
            if (!guildExpeditions) {
                res.status(404).end();
                return
            }
            res.status(200).json(guildExpeditions);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpPost("/expeditions",
        validationMiddleware(GuildAdventure),
        TYPE.AuthOnlyTeacherMiddleware)
    public async createLeaderGuildExpeditions(
        @requestBody() newGuildExpedition: GuildAdventure,
        @response() res: express.Response
    ) {
        try {
            const guildExpedition = await this.guildAdventureService.save(newGuildExpedition);
            res.status(201).json(guildExpedition);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpDelete('/:id',
        isParamNumber('id'),
        TYPE.AuthOnlyTeacherMiddleware)
    public async deleteGuildAdventure(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const deleted = await this.guildAdventureService.delete(id);
            if (deleted) {
                res.status(204).end();
                return;
            }
            res.status(404).end();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpPut('/expedition/:id',
        isParamNumber('id'),
        validationMiddleware(GuildAdventure, true),
        isBodyDateGreaterThan(
            true,
            'deadline'
        ),
        TYPE.AuthOnlyTeacherMiddleware)
    public async updateGuildExpedition(
        @requestParam('id') id: number,
        @requestBody() guildExpedition: GuildAdventure,
        @response() res: express.Response
    ) {
        try {
            const newGuildExpedition = await this.guildAdventureService.update(id, guildExpedition);
            if (newGuildExpedition) {
                res.status(204).end();
                return;
            }
            res.status(404).end();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }
}
