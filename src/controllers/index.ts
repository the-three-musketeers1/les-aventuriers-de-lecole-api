import {UserController} from "./user.controller";
import {QuestionTypeController} from "./question.type.controller";
import {QuestionController} from "./question.controller";
import {AnswerController} from "./answer.controller";
import {AuthController} from "./auth.controller";
import {ExerciseController} from "./exercise.controller";
import {AdventureController} from "./adventure.controller";
import {GuildController} from "./guild.controller";
import {RoleController} from "./role.controller";
import {GuildAdventureController} from "./guildAdventure.controller";
import {TicketController} from "./ticket.controller";
import {StatusController} from "./status.controller";
import {MessageController} from "./message.controller";
import {AdventureExerciseController} from "./adventureExercise.controller";

export async function loadControllers() {
    await UserController;
    await QuestionTypeController;
    await QuestionController;
    await AnswerController;
    await AuthController;
    await ExerciseController;
    await AdventureController;
    await GuildController;
    await RoleController;
    await GuildAdventureController;
    await TicketController;
    await StatusController;
    await MessageController;
    await AdventureExerciseController;
}
