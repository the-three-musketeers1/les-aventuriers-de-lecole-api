import * as express from "express";
import {
    controller,
    httpDelete,
    httpGet,
    httpPost,
    httpPut,
    requestBody,
    requestParam,
    response
} from "inversify-express-utils";
import {inject} from "inversify";
import {TYPE} from "../constants/types";
import {QuestionTypeService} from "../services/question.type.service";
import {QuestionType} from "../entities/QuestionType";
import {validationMiddleware} from "../middlewares/validation.middleware";
import {isParamNumber} from "../validators/params.validator";

@controller('/question-types')
export class QuestionTypeController {
    constructor(@inject(TYPE.QuestionTypeService) private questionTypeService: QuestionTypeService) {
    }

    @httpPost('/', validationMiddleware(QuestionType))
    public async postQuestionTypes(
        @requestBody() newQuestionTypes: QuestionType,
        @response() res: express.Response
    ) {
        try {
            const questionType = await this.questionTypeService.save(newQuestionTypes);
            res.status(201).json(questionType);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/')
    public async getQuestionTypes(
        @response() res: express.Response
    ) {
        try {
            return await this.questionTypeService.find();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpGet('/:id', isParamNumber('id'))
    public async getQuestionType(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            const questionType = await this.questionTypeService.findOne(id);
            if (!questionType) {
                res.status(404).end();
                return
            }
            res.status(200).json(questionType);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpPut('/:id', isParamNumber('id'), validationMiddleware(QuestionType, true))
    public async putQuestionType(
        @requestParam('id') id: number,
        @requestBody() questionType: QuestionType,
        @response() res: express.Response
    ) {
        try {
            const checkQuestionType = await this.questionTypeService.update(id, questionType);
            if (checkQuestionType) {
                res.status(204).end();
                return;
            }
            res.status(404).end();
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }

    @httpDelete('/:id', isParamNumber('id'))
    public async deleteQuestionType(
        @requestParam('id') id: number,
        @response() res: express.Response
    ) {
        try {
            return await this.questionTypeService.delete(id);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }
}
