import {controller, httpPost, requestBody, requestParam, response} from "inversify-express-utils";
import {inject} from "inversify";
import {TYPE} from "../constants/types";
import {CharacterQuestionService} from "../services/characterQuestion.service";
import * as express from "express";
import {CharacterExerciseService} from "../services/characterExercise.service";
import {ExerciseAnswersDTO} from "../entities/dto/ExerciseAnswersDTO";
import {AdventureExerciseService} from "../services/adventureExercise.service";
import {CharacterService} from "../services/character.service";

@controller('/characterQuestion')
export class CharacterQuestionController {
    constructor(
        @inject(TYPE.CharacterQuestionService) private characterQuestionService: CharacterQuestionService,
        @inject(TYPE.CharacterExerciseService) private characterExerciseService: CharacterExerciseService,
        @inject(TYPE.AdventureExerciseService) private adventureExerciseService: AdventureExerciseService,
        @inject(TYPE.CharacterService)         private characterService: CharacterService
        ) {
    }

    @httpPost('')
    public async saveCharacterQuestion(
        @requestBody() exerciseAnswersDTO: ExerciseAnswersDTO,
        @response() res: express.Response
    ) {
        try {
            const adventureExercise = await this.adventureExerciseService.getByExerciseIdAndExpeditionId(exerciseAnswersDTO.exerciseId, exerciseAnswersDTO.adventureId);
            const characterExercise = await this.characterExerciseService.save(adventureExercise.id, exerciseAnswersDTO.characterId);
            const charactersQuestions = exerciseAnswersDTO.answers.map(answer =>  this.characterQuestionService.mapAnswerDtoToCharacterQuestion(answer, characterExercise.id));
            const numberOfBonusConsumed = charactersQuestions.filter(question => question.bonus_consumed == true).length;
            if (numberOfBonusConsumed > 0) {
                await this.characterService.useActionPoints(numberOfBonusConsumed, exerciseAnswersDTO.characterId);
            }
            const answers = await this.characterQuestionService.save(charactersQuestions);
            res.status(201).json(answers);
        } catch (e) {
            res.status(500);
            res.send(e.message);
        }
    }
}
