import {EntityRepository, Repository} from "typeorm";
import {QuestionType} from "../entities/QuestionType";

@EntityRepository(QuestionType)
export class QuestionTypeRepository extends Repository<QuestionType>{
    findAll() {
        return this.find();
    }

    saveQuestionType(questionType: QuestionType) {
        return this.save(questionType);
    }

    findById(id) {
        return this.findOne(id);
    }
    updateQuestionType(questionType: QuestionType) {
        return this.update(questionType.id, questionType);
    }

    deleteById(id: number) {
        return this.delete(id);
    }
}
