import {EntityRepository, Repository} from "typeorm";
import {AdventureExercise} from "../entities/AdventureExercise";

@EntityRepository(AdventureExercise)
export class AdventureExerciseRepository extends Repository<AdventureExercise>{

    findByLeader(leaderId: number) {
        return this.createQueryBuilder('expeditionExercise')
            .innerJoinAndSelect('expeditionExercise.adventure', 'expedition',
                'expedition.isExpedition = true')
            .innerJoinAndSelect('expeditionExercise.exercise', 'exercise')
            .where('exercise.leaderId = :leaderId', {leaderId})
            .andWhere('expedition.leaderId = :leaderId', {leaderId})
            .getMany();
    }

    findExistingAdventureExercise(adventureExercise: AdventureExercise) {
        return this.findOne({
            where: {
                exerciseId: adventureExercise.exerciseId,
                adventureId: adventureExercise.adventureId
            }
        });
    }
    saveAdventureExercise(adventureExercise: AdventureExercise) {
        return this.save(adventureExercise);
    }
    deleteById(id) {
        return this.delete(id);
    }
}
