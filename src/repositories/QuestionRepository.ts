import {EntityRepository, Repository} from "typeorm";
import {Question} from "../entities/Question";

@EntityRepository(Question)
export class QuestionRepository extends Repository<Question> {

    saveQuestion(question: Question) {
        return this.save(question);
    }

    findAllWithExerciseAndType() {
        return this.find({relations: ["questionType", "exercise"]});
    }

    findByIdWithExerciseAndType(id: number) {
        return this.findOne(id, {relations: ["questionType", "exercise"]});
    }

    findByIdWithAnswers(id: number) {
        return this.findOne(id, {relations: ['answers']});
    }

    updateQuestion(question: Question) {
        return this.update(question.id, question);
    }

    deleteById(id: number) {
        return this.delete(id);
    }

    findBySequenceNumber(sequence_number: number) {
        return this.findOne({
            where: {
                sequence_number
            }
        });
    }
}
