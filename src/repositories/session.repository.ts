import {EntityRepository, Repository} from "typeorm";
import {Session} from "../entities/Session";

@EntityRepository(Session)
export class SessionRepository extends Repository<Session> {
}
