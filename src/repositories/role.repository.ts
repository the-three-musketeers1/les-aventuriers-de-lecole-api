import {Role} from "../entities/Role";
import {EntityRepository, Repository} from "typeorm";

@EntityRepository(Role)
export class RoleRepository extends Repository<Role> {

    findByName(name: string) {
        return this.findOne({
            where: {
                name
            }
        });
    }

    saveRole(role: Role) {
        return this.save(role)
    }

    findAll() {
        return this.find();
    }

    findById(id: number) {
        return this.findOne(id);
    }



}
