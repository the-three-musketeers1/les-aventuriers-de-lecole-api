import {EntityRepository, Repository} from "typeorm";
import {Ticket} from "../entities/Ticket";
import {Character} from "../entities/Character";
import {User} from "../entities/User";
import {Status} from "../entities/Status";

@EntityRepository(Ticket)
export class TicketRepository extends Repository<Ticket> {

    findById(id: number) {
        return this.findOne(id);
    }

    findByIdWithLeader(id: number) {
        return this.findOne(id, {relations: ['leader']});
    }

    findByIdWithCharacter(id: number) {
        return this.findOne(id, {relations: ['character']});
    }

    saveTicket(ticket) {
        return this.save(ticket);
    }

    findCharacterTickets(character: Character) {
        return this.find({
            where: {character},
            relations: ['status', 'messages', 'leader']
        });
    }

    async findByCharacterWithMessages(ticketId: number, character: Character) {
        return await this.findOne(ticketId,
            {
                where: {character},
                relations: ['messages']
            });
    }

    async findByLeaderWithMessages(ticketId: number, leader: User) {
        return await this.findOne(ticketId,
            {
                where: {leader},
                relations: ['messages']
            });
    }

    findByIdWithMessages(id: number) {
        return this.findOne(id, {relations: ['messages']});
    }

    findAllByLeader(leader: User) {
        return this.find({
            relations: ['status', 'character', 'character.user', 'leader', 'messages'],
            where: {
                leader
            }
        });
    }

    findByIdAndLeader(id: number, leader: User) {
        return this.findOne({
            where: {
                id,
                leader
            }
        });
    }

    findByIdAndLeaderWIthInformations(id: number, leader: User) {
        return this.findOne({
            relations: ['status', 'character', 'character.user', 'leader', 'messages'],
            where: {
                id,
                leader
            }
        });
    }

    createTicketWithStatus(status: Status) {
        return this.create({
            status: status
        });
    }

    updateTicket(id: number, ticket: Ticket) {
        return this.update(id, ticket);
    }
}
