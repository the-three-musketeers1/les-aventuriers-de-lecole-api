import {EntityRepository, Repository} from "typeorm";
import {GuildAdventure} from "../entities/GuildAdventure";

@EntityRepository(GuildAdventure)
export class GuildAdventureRepository extends Repository<GuildAdventure>{

    saveGuildAdventure(guildAdventure: GuildAdventure) {
        return this.save(guildAdventure);
    }

    deleteById(id: number) {
        return this.delete(id);
    }

    findById(id: number) {
        return this.findOne(id);
    }

    findByLeaderId(id: number) {
        return this.createQueryBuilder('guildExpeditions')
            .innerJoinAndSelect('guildExpeditions.guild', 'guild')
            .innerJoinAndSelect('guildExpeditions.adventure', 'expedition')
            .innerJoin('guild.leader', 'guildLeader')
            .innerJoin('expedition.leader', 'expeditionLeader')
            .where('guildLeader.id = :id', {id})
            .andWhere('expeditionLeader.id = :id', {id})
            .andWhere('expedition.isExpedition = true')
            .getMany();
    }

    updateAccessibilityAndDeadline(guildExpedition: GuildAdventure) {
        return this.createQueryBuilder()
            .update(guildExpedition)
            .set({
                deadline: guildExpedition.deadline,
                accessible: guildExpedition.accessible
            })
            .where("id = :id", {id: guildExpedition.id})
            .execute();
    }
}
