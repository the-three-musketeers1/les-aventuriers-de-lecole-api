import {EntityRepository, Repository} from "typeorm";
import {CharacterExercise} from "../entities/CharacterExercise";

@EntityRepository(CharacterExercise)
export class CharacterExerciseRepository extends Repository<CharacterExercise>{

    saveCharacterExercise(characterExercise: CharacterExercise) {
        return this.save(characterExercise);
    }

    findAllByGuildId(guildId: number) {
        let query = this.createQueryBuilder('characterExercise')
            .leftJoinAndSelect('characterExercise.adventureExercise','adventureExercise')
            .leftJoinAndSelect('adventureExercise.exercise','exercise')
            .leftJoinAndSelect('exercise.subject', 'chapter')
            .leftJoinAndSelect('chapter.parentSubject','discipline')
            .leftJoinAndSelect('characterExercise.characterQuestions', 'characterQuestions')
            .leftJoinAndSelect('characterQuestions.answer', 'answer')
            .leftJoinAndSelect('characterQuestions.question', 'question')
            .leftJoin('characterExercise.character','character')
            .leftJoin('character.guilds', 'guilds')
            .where('guilds.id = :id', {id: guildId});

        return query.getMany();
    }

    findAllByCharacterIdWithOptions(characterId: number, disciplineId: number, chapterId: number, beginDate: any, endDate: any) {
        let query = this.createQueryBuilder('characterExercise')
            .leftJoinAndSelect('characterExercise.adventureExercise','adventureExercise')
            .leftJoinAndSelect('adventureExercise.exercise','exercise')
            .leftJoinAndSelect('exercise.subject', 'chapter')
            .leftJoinAndSelect('chapter.parentSubject','discipline')
            .leftJoinAndSelect('characterExercise.characterQuestions', 'characterQuestions')
            .leftJoinAndSelect('characterQuestions.answer', 'answer')
            .leftJoinAndSelect('characterQuestions.question', 'question')
            .where('characterExercise.characterId = :id', {id: characterId});

        if (disciplineId !== undefined && disciplineId !== null) {
            query = query.andWhere('discipline.id = :disciplineId',{disciplineId: disciplineId});
        }
        if (chapterId !== undefined && chapterId !== null) {
            query = query.andWhere('chapter.id = :chapterId', {chapterId: chapterId});
        }
        if (beginDate !== undefined && beginDate !== null && endDate !== undefined && endDate !== null) {
            query = query.andWhere('characterExercise.createdAt > :beginDate', {beginDate: beginDate})
                .andWhere('characterExercise.createdAt < :endDate', {endDate: endDate})
        }
        return query.getMany();
    }
}
