import {EntityRepository, Repository} from "typeorm";
import {User} from "../entities/User";

@EntityRepository(User)
export class UserRepository extends Repository<User> {

    saveUser(user: User) {
        return this.save(user);
    }

    findAll() {
        return this.find();
    }

    findById(id: number) {
        return this.findOne(id);
    }

    findByIdWithLessInformations(id: number) {
        return this.findOne({
            where: {
                id
            },
            select: ['firstname', 'lastname', 'password', 'email']
        });
    }

    updateUser(user: User) {
        return this.update(user.id, user);
    }

    deleteById(id: number) {
        return this.delete(id);
    }

    findLeaders() {
        return this.find({
            where: {
                is_teacher: true
            }
        });
    }

    findLeaderById(id: number) {
        return this.findOne(id, {
            where: {
                is_teacher: true
            }
        });
    }

    findLeaderSubjects(id: number) {
        return this.findOne(id, {
            relations: ["subjects", "subjects.childSubjects", "subjects.parentSubject"],
            where: {
                is_teacher: true
            }
        });
    }

}
