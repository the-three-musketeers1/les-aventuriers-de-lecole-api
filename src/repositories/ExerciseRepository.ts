import {EntityRepository, Repository} from "typeorm";

import {Exercise} from "../entities/Exercise";
import {Subject} from "../entities/Subject";

@EntityRepository(Exercise)
export class ExerciseRepository extends Repository<Exercise>{

    findExerciseWithQuestions(id: number) {
        return this.findOne({
            relations: ["questions","questions.answers","questions.questionType"],
            where: { id: id }
        });
    }

    findExercisesByExpeditionId(id: number) {
        return this.createQueryBuilder('exercise')
            .leftJoin('exercise.adventureExercises', 'adventureExercise')
            .leftJoinAndSelect('exercise.subject','subject')
            .leftJoinAndSelect('subject.parentSubject','discipline')
            .leftJoinAndSelect('exercise.questions','questions')
            .leftJoinAndSelect('exercise.leader','leader')
            .where('adventureExercise.adventureId = :id', {id : id})
            .getMany();
    }

    findExercisesByExpeditionIdWIthCharacterContent(expeditionId: number, characterId: number) {
      return this.createQueryBuilder('exercise')
          .leftJoinAndSelect('exercise.adventureExercises', 'adventureExercise')
          .leftJoinAndSelect('exercise.subject','subject')
          .leftJoinAndSelect('subject.parentSubject','discipline')
          .leftJoinAndSelect('exercise.questions','questions')
          .leftJoinAndSelect('adventureExercise.characterExercises', 'characterExercises','characterExercises.characterId = :characterId',{characterId: characterId})
          .leftJoinAndSelect('characterExercises.characterQuestions', 'characterQuestions')
          .leftJoinAndSelect('characterQuestions.answer','answer')
          .where('adventureExercise.adventureId = :id', {id : expeditionId})
          .getMany();
    }

    findExercisesByGuildId(id: number) {
        return this.createQueryBuilder('exercise')
            .leftJoin('exercise.adventureExercises', 'adventureExercise')
            .leftJoin('adventureExercise.adventure','adventure')
            .leftJoin('adventure.guildAdventures','guildAdventure')
            .leftJoinAndSelect('exercise.subject','subject')
            .leftJoinAndSelect('subject.parentSubject','discipline')
            .where('guildAdventure.guildId = :id', {id : id})
            .getMany();
    }

    saveExercise(exercise: Exercise) {
       return this.save(exercise);
    }

    findAll() {
        return this.find({ relations: ["subject", "leader", "subject.parentSubject"]});
    }

    findById(id: number) {
        return this.findOne(id, { relations: ["subject", "leader", "subject.parentSubject", "subject.parentSubject.childSubjects"]});
    }

    updateExercise(exercise: Exercise) {
        return this.update(exercise.id, exercise)
    }

    deleteExerciseById(id: number) {
        return this.delete(id);
    }

    findBySubject(subject: Subject) {
        return this.find(
            {
                relations: ["subject", "leader", "subject.parentSubject", "adventureExercises"],
                where: { subject : subject}
            });
    }

    findByIdWithAdventure(id: number) {
        return this.findOne(id, {relations: ['adventureExercises',
                'adventureExercises.adventure']});
    }

    findByLeaderId(id: number) {
        return this.find({
            where: {
                leader: id
            },
            relations: ["subject", "subject.parentSubject", "subject.childSubjects", "subject.parentSubject.childSubjects"]
        });
    }

    findByLeaderIdAndSubjectId(leaderId: number, subjectId: number) {
        return this.find({
            where: {
                leader: leaderId,
                subject: subjectId
            },
            relations: ["subject"]
        });
    }

    findByLeaderIdAndAdventureId(leaderId: number, adventureId: number) {
        return this.createQueryBuilder('exercise')
            .innerJoin('exercise.adventureExercises', 'adventure',
                'adventure.adventureId = :adventureId', { adventureId })
            .leftJoinAndSelect('exercise.subject', 'subject')
            .where('exercise.leaderId = :leaderId', { leaderId })
            .getMany();
    }

    findByLeaderIdAndExpeitionId(leaderId: number, expeditionId: number) {
        return this.createQueryBuilder('exercise')
            .innerJoin('exercise.adventureExercises', 'adventureExercise',
                'adventureExercise.adventureId = :expeditionId', { expeditionId })
            .innerJoin('adventureExercise.adventure', 'expedition',
                'expedition.id = :expeditionId', { expeditionId })
            .leftJoinAndSelect('exercise.subject', 'subject')
            .where('exercise.leaderId = :leaderId', { leaderId })
            .andWhere('expedition.isExpedition = true')
            .getMany();
    }

    findIfExerciseIsLinkedToCharacter(id: number, characterId: number) {
        return this.createQueryBuilder('exercise')
            .innerJoin('exercise.adventureExercises', 'adventureExercise')
            .innerJoin('adventureExercise.adventure', 'expedition')
            .innerJoin("expedition.guildAdventures",'guildAdventures')
            .innerJoin('guildAdventures.guild','guild')
            .innerJoin('guild.characters','character')
            .where('exercise.id = :id', { id })
            .andWhere('character.id = :characterId',{characterId})
            .getMany();
    }
}
