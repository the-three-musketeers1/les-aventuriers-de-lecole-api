import {EntityRepository, Repository} from "typeorm";
import {Subject} from "../entities/Subject";

@EntityRepository(Subject)
export class SubjectRepository extends Repository<Subject> {

    saveSubject(subject: Subject) {
        return this.save(subject);
    }

    findByTitleWithLeader(title: string) {
        return this.find({
            relations: ['leaders'],
            where: {
                title: title
            }
        });
    }

    findAll() {
        return this.find({relations: ["parentSubject"]});
    }

    findById(id: number) {
        return this.findOne(id, {relations: ["parentSubject"]});
    }

    findByIdWithLeader(id: number) {
        return this.findOne({
            relations: ['leaders'],
            where: {
                id
            }
        });
    }

    updateSubject(subject: Subject) {
        return this.update(subject.id, subject);
    }

    deleteById(id: number) {
        return this.delete(id);
    }

    findDisciplineByLeaderId(id: number) {
        return this.createQueryBuilder('subject')
            .innerJoin('subject.leaders', 'leader')
            .where('leader.id = :id', {id})
            .andWhere('subject.parentSubject IS NULL')
            .getMany();
    }

    findDisciplineByGuildId(id: number) {
        return this.createQueryBuilder('subject')
            .innerJoinAndSelect('subject.childSubjects', 'chapter')
            .innerJoin('chapter.exercises', 'exercise')
            .innerJoin('exercise.adventureExercises', 'adventureExercise')
            .innerJoin('adventureExercise.adventure', 'adventure')
            .innerJoin('adventure.guildAdventures', 'guildAdventure')
            .where('guildAdventure.guildId = :id', {id})
            .andWhere('subject.parentSubject IS NULL')
            .getMany();
    }

    findChapterByDisciplineId(id: number) {
        return this.createQueryBuilder('chapter')
            .innerJoin('chapter.parentSubject', 'discipline')
            .where('discipline.id = :id', {id})
            .getMany();
    }

    findDisciplineByCharacterId(id: number) {
        return this.createQueryBuilder('subject')
            .innerJoin('subject.leaders', 'leader')
            .innerJoin('leader.guilds', 'guild')
            .innerJoin('guild.characters', 'character')
            .where('character.id = :id', {id})
            .andWhere('subject.parentSubject IS NULL')
            .getMany();
    }

    findByIdWithLeaderParentAndChild(id: number) {
        return this.findOne({
            relations: ['leaders', 'parentSubject', 'childSubjects'],
            where: {
                id: id
            }
        });
    }

    findSubjectByIdWithParentAndChildren(id: number) {
        return this.findOne({
            relations: ['childSubjects', 'parentSubject'],
            where: {
                id: id
            }
        });
    }

    findChapterByDiscplineWithLeader(discipline: Subject) {
        return this.find({
            relations: ['leaders', 'parentSubject'],
            where: {
                parentSubject: discipline
            }
        });
    }
}
