import {EntityRepository, Repository} from "typeorm";
import {Answer} from "../entities/Answer";

@EntityRepository(Answer)
export class AnswerRepository extends Repository<Answer>{
    saveAnswer(answer: Answer) {
        return this.save(answer);
    }

    findAll() {
        return this.find({relations: ["question"]})
    }

    findByQuestion(questionId: number) {
        return this.find({
            relations: ["question"],
            where: { questionId: questionId }
        });
    }

    findById(id: number) {
        return this.findOne(id, { relations: ["question"]});
    }

    updateAnswer(answer: Answer) {
        return this.update(answer.id, answer);
    }

    deleteById(id: number) {
        return this.delete(id);
    }
}
