import {EntityRepository, Repository} from "typeorm";
import {Guild} from "../entities/Guild";

@EntityRepository(Guild)
export class GuildRepository extends Repository<Guild>{
    findByNameAndLeader(guildName, leader) {
        return this.findOne({
            where: {name: guildName, leader: leader}
        });
    }

    createWithName(guildName) {
        return this.create({name: guildName});
    }

    saveGuild(guild) {
        return this.save(guild);
    }

    findById(id) {
        return this.delete(id);
    }

    deleteById(id) {
        return this.findOne(id);
    }

    updateGuildNameAndDeadline(id, name, deadline) {
        return this.update(id, {
            name: name,
            deadlineAddCharacter: deadline
        });
    }

    findByLeader(leader) {
        return this.find({
            relations: ['leader'],
            where: {
                leader
            }
        });
    }

    findByIdAndLeader(id, leader) {
        return this.findOne({
            relations: ['leader'],
            where: {
                id,
                leader
            }
        })
    }

    findByNameAndToken(name,token) {
        return this.findOne({
            relations: ['leader'],
            where: {
                name,
                token
            }
        });
    }

    findGuildsByLeaderId(id) {
        return this.createQueryBuilder('guild')
            .leftJoinAndSelect('guild.characters', 'character')
            .leftJoinAndSelect('guild.leader','leader')
            .where('leader.id = :id', {id : id})
            .getMany();
    }
}
