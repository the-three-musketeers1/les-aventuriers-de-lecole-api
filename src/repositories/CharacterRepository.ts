import {EntityRepository, Repository} from "typeorm";
import {Character} from "../entities/Character";

@EntityRepository(Character)
export class CharacterRepository extends Repository<Character>{

    findById(id) {
        return this.findOne({
            relations: ['role', 'user', 'guilds'],
            where: {id: id}
        })
    }

    addExperienceToCharacter(id,amount) {
        return this.createQueryBuilder().update(Character)
            .set({experience: amount})
            .where("id = :id", {id: id}).execute();
    }

    addExperienceAndlevelToCharacter(id,amount,level) {
        return this.createQueryBuilder().update(Character)
            .set({experience: amount, level: level})
            .where("id = :id", {id: id}).execute();
    }

    findByStudentId(studentId) {
        return this.find({
            relations: ["user", "guilds","role"],
            where: {user: {id: studentId}}
        });
    }

    saveCharacter(character) {
        return this.save(character);
    }

    createCharacter(characterName) {
        return  this.create({
            name: characterName,
            guilds: []
        });
    }

    findOneStudentCharacterById(characterId: number, studentId: number) {
        return this.findOne({
            relations: ['role', 'user'],
            where: {
                id: characterId,
                user: studentId
            }
        });
    }

    findCharactersByGuildId(id) {
        return this.createQueryBuilder('character')
            .leftJoinAndSelect('character.role','role')
            .leftJoinAndSelect('character.guilds','guild')
            .leftJoinAndSelect('character.user','user')
            .where('guild.id = :id', {id : id})
            .getMany();
    }

    findCharacterLeader(characterId: number) {
        return this.createQueryBuilder('character')
            .innerJoinAndSelect('character.guilds', 'guilds')
            .innerJoinAndSelect('guilds.leader', 'leader')
            .where('character.id = :id', {id: characterId})
            .getOne();
    }

    updateActionPoint(remainingPoints, id) {
        return this.createQueryBuilder().update(Character)
            .set({action_points : remainingPoints})
            .where("id = :id", {id: id}).execute();
    }

    deleteById(id) {
        return this.delete(id);
    }
}
