import {EntityRepository, Repository} from "typeorm";
import {CharacterAdventure} from "../entities/CharacterAdventure";

@EntityRepository(CharacterAdventure)
export class CharacterAdventureRepository extends Repository<CharacterAdventure>{

    findByExpeditionIdAndCharacterId(expeditionId: number, characterId: number) {
        return this.find({
            where : {adventureId: expeditionId, characterId: characterId}
        });
    }

    saveCharacterAdventure(characterAdventure: CharacterAdventure) {
        return this.save(characterAdventure);
    }
}
