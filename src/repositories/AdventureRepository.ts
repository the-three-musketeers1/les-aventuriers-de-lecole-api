import {EntityRepository, Repository} from "typeorm";
import {Adventure} from "../entities/Adventure";

@EntityRepository(Adventure)
export class AdventureRepository extends Repository<Adventure>{
    findAll() {
        return this.find({ relations: ["leader", "guildAdventures"]});
    }
    findById(id: number) {
        return this.findOne(id, { relations: ["leader", "guildAdventures"]});
    }
    saveAdventure(adventure: Adventure) {
        return this.save(adventure);
    }

    updateAdventure(adventure: Adventure) {
        return this.update(adventure.id, adventure)
    }

    deleteAdventureById(id: number) {
        return this.delete(id);
    }

    findExpeditions() {
        return this.find({
            where: {
                isExpedition: true
            }
        });
    }

    findAvailableExpeditionsByCharacterId(id: number) {
        return this.createQueryBuilder("expedition")
            .innerJoinAndSelect('expedition.guildAdventures','guildExpedition')
            .innerJoin('guildExpedition.guild','guild')
            .innerJoin('guild.characters','characters')
            .innerJoinAndSelect('expedition.adventureExercises','adventureExercise')
            .innerJoinAndSelect('adventureExercise.exercise','exercise')
            .innerJoinAndSelect('exercise.subject','chapter')
            .leftJoinAndSelect('chapter.parentSubject','discipline')
            .leftJoinAndSelect("expedition.characterAdventures","characterAdventure",'characterAdventure.characterId = :id',{id: id})
            .where('characters.id = :id',{id:id})
            .andWhere('guildExpedition.deadline > :date', {date: new Date()})
            .andWhere('guildExpedition.accessible = :accessible', {accessible: true})
            .andWhere('expedition.isExpedition = :isExpedition',{isExpedition: true})
            .getMany();
    }

    findAllExpeditionsByCharacterId(id: number) {
        return this.createQueryBuilder("expedition")
            .innerJoinAndSelect('expedition.guildAdventures','guildExpedition')
            .innerJoin('guildExpedition.guild','guild')
            .innerJoin('guild.characters','characters')
            .innerJoinAndSelect('expedition.adventureExercises','adventureExercise')
            .innerJoinAndSelect('adventureExercise.exercise','exercise')
            .innerJoinAndSelect('exercise.subject','chapter')
            .leftJoinAndSelect('chapter.parentSubject','discipline')
            .leftJoinAndSelect("expedition.characterAdventures","characterAdventure",'characterAdventure.characterId = :id',{id: id})
            .where('characters.id = :id',{id:id})
            .andWhere('expedition.isExpedition = :isExpedition',{isExpedition: true})
            .getMany();
    }

    findOneExpeditionById(id: number) {
        return this.findOne({
            where: {
                isExpedition: true,
                id
            }
        });
    }
    findByLeaderId(id: number) {
        return this.find({
            where: {
                leader: id
            }
        });
    }

    findLeaderExpeditions(id: number) {
        return this.createQueryBuilder('expedition')
            .innerJoinAndSelect('expedition.leader', 'leader',
                'leader.id = :id', {id})
            .where('expedition.isExpedition = true')
            .getMany();
    }

    findIfExpeditonIsLinkedToCharacter(expeditionId: number, characterId: number) {
        return this.createQueryBuilder('adventure')
            .innerJoin('adventure.guildAdventures', 'guildAdventures')
            .innerJoin('guildAdventures.guild', 'guild')
            .innerJoin('guild.characters', 'character')
            .where('character.id = :id',{id: characterId})
            .andWhere("adventure.id = :expeditionId", {expeditionId: expeditionId})
            .getMany();
    }
}
