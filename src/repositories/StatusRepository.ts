import {Status} from "../entities/Status";
import {EntityRepository, Repository} from "typeorm";

@EntityRepository(Status)
export class StatusRepository extends Repository<Status> {

    findByState(state: string) {
        return this.findOne({
            where: {
                state
            }
        });
    }

    saveStatus(status: Status) {
        return this.save(
            status
        );
    }

    findAll() {
        return this.find();
    }

    findById(id: number) {
        return this.findOne(id);
    }

}
