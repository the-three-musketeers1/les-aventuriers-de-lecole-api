import {EntityRepository, Repository} from "typeorm";
import {CharacterQuestion} from "../entities/CharacterQuestion";

@EntityRepository(CharacterQuestion)
export class CharacterQuestionRepository extends Repository<CharacterQuestion>{
    saveCharacterQuestion(charactersQuestions: CharacterQuestion[]) {
        return this.save(charactersQuestions);
    }
}
