import {createHash, randomBytes} from "crypto";
import {injectable} from "inversify";

@injectable()
export class SecurityUtil {

    /**
     * get hash password
     * @param password {string}
     * @return {string}
     */
    hashPassword(password: string): string {
        const hash = createHash('sha256');
        hash.update(password);
        return hash.digest('hex').toString();
    }

    /**
     * generate random token
     * @return {Promise<string>}
     */
    randomToken(size = 32): Promise<string> {
        return new Promise((resolve, reject) => {
            randomBytes(size / 2, (err, buf) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve(buf.toString('hex'));
            });
        });
    }
}
