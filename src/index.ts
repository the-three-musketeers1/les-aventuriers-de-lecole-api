import "reflect-metadata";
import { resolve } from "path";
import { config } from "dotenv";
import {InversifyExpressServer} from "inversify-express-utils";
import bodyParser = require("body-parser");
import cors = require("cors") ;
import {getDbConnection} from "./db";
import {Connection} from "typeorm";
import {loadControllers} from "./controllers";
import {loadContainers} from "./containers";

config({ path: resolve(__dirname, "../.env")});

async function bootstrap() {
    try {
        const connection: Connection = await getDbConnection();
        await loadControllers();
        const container = loadContainers();
        const app = new InversifyExpressServer(container);
        app.setConfig((app) => {
            app.use(bodyParser.json());
            app.use(cors())
        });

        const server = app.build();
        const httpServer = server.listen(`${process.env.PORT}`, () =>
            console.log(`Serveur démarré au port ${process.env.PORT}`));

        return {httpServer, connection, container};
    } catch (e) {
        console.log(e);
    }
}

export default bootstrap();
