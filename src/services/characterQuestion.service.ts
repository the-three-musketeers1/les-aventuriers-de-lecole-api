import {inject, injectable} from "inversify";
import {TYPE} from "../constants/types";
import {CharacterQuestionRepository} from "../repositories/CharacterQuestionRepository";
import {AnswerDTO} from "../entities/dto/AnswerDTO";
import {CharacterQuestion} from "../entities/CharacterQuestion";

@injectable()
export class CharacterQuestionService {
    constructor(@inject(TYPE.CharacterQuestionRepository) private characterQuestionRepository: CharacterQuestionRepository) {
    }

    mapAnswerDtoToCharacterQuestion(answer: AnswerDTO, characterExerciseId) {
        const characterQuestion =  new CharacterQuestion();
        characterQuestion.answerId = answer.id;
        characterQuestion.bonus_consumed = answer.bonusConsumed;
        characterQuestion.questionId = answer.questionId;
        characterQuestion.characterExerciseId = characterExerciseId;
        return characterQuestion;
    }

    async save(charactersQuestions: CharacterQuestion[]) {
        return this.characterQuestionRepository.saveCharacterQuestion(charactersQuestions);

    }
}
