import {inject, injectable} from "inversify";
import {TYPE} from "../constants/types";
import {SubjectRepository} from "../repositories/SubjectRepository";
import {Subject} from "../entities/Subject";
import {User} from "../entities/User";

@injectable()
export class SubjectService {
    constructor(@inject(TYPE.SubjectRepository) private subjectRepository: SubjectRepository) {
    }

    async save(newSubject: Subject, leader: User) {
        const checkSubjects = await this.subjectRepository.findByTitleWithLeader(newSubject.title);
        const isSubjectAlreadyCreated = checkSubjects.some(subject => {
            return subject.leaders.some(subjectLeader => {
                return subjectLeader.id === leader.id
            });
        });
        if (isSubjectAlreadyCreated) {
            return null;
        }
        newSubject.leaders = [leader];
        return this.subjectRepository.saveSubject(newSubject);
    }

    async find() {
        return this.subjectRepository.findAll();
    }

    async findOne(id: number) {
        return this.subjectRepository.findById(id);
    }

    async update(id: number, subject: Subject, leader: User) {
        const checkSubject = await this.subjectRepository.findByIdWithLeader(id);
        if (!checkSubject) {
            return false;
        }
        if (!checkSubject.leaders.some(checkedLeader => checkedLeader.id === leader.id)) {
            throw {leaderError: `The subject '${checkSubject.id}' do not belong to the current user`};
        }
        subject.id = id;
        await this.subjectRepository.updateSubject(subject);
        return true;
    }

    async delete(id: number) {
        return this.subjectRepository.deleteById(id);
    }

    async findDisciplineByLeaderId(id: number) {
        return await this.subjectRepository.findDisciplineByLeaderId(id);
    }

    async findDisciplineByGuildId(id: number) {
        return await this.subjectRepository.findDisciplineByGuildId(id);
    }

    async findChapterByDisciplineId(id: number) {
        return await this.subjectRepository.findChapterByDisciplineId(id);
    }

    async findDisciplineByCharacterId(id: number) {
        return await this.subjectRepository.findDisciplineByCharacterId(id);
    }

    async saveChapter(chapterToSave: Subject, disciplineId: number, leaderId: number) {
        const checkDiscipline = await this.subjectRepository.findByIdWithLeaderParentAndChild(disciplineId);
        if (checkDiscipline === undefined) {
            throw {notFoundError: `Subject with id '${disciplineId}' not found`}
        }
        if (checkDiscipline.parentSubject) {
            throw {forbiddenError: `Subject with id '${disciplineId}' is not discipline`};
        }
        if (checkDiscipline.leaders[0].id !== leaderId) {
            throw {forbiddenError: `Discipline with id '${disciplineId}' not belong to the leader that send request`};
        }

        chapterToSave.parentSubject = checkDiscipline;
        return await this.subjectRepository.saveSubject(chapterToSave);
    }

    async findChaptersByDisciplineId(disciplineId: number) {
        const checkDiscipline = await this.subjectRepository.findSubjectByIdWithParentAndChildren(disciplineId);
        if (checkDiscipline === undefined) {
            throw {notFoundError: `Subject with '${disciplineId}' not found`}
        }
        if (checkDiscipline.parentSubject) {
            throw {forbiddenError: `Subject with '${disciplineId}' is not discipline`};
        }

        return await this.subjectRepository.findChapterByDiscplineWithLeader(checkDiscipline);
    }

    async findOneChapter(disciplineId: number, chapterId: number) {
        const checkDiscipline = await this.subjectRepository.findByIdWithLeaderParentAndChild(disciplineId);
        if (checkDiscipline === undefined) {
            throw {notFoundError: `Subject with id '${disciplineId}' not found`};
        }
        if (!checkDiscipline.childSubjects.some(subject => subject.id === chapterId)) {
            throw {notFoundError: `Subject with id '${chapterId}' not found`};
        }
        return await this.subjectRepository.findByIdWithLeaderParentAndChild(chapterId);
    }

    async updateChapter(disciplineId: number, chapterId: number, chapterToUpdate: Subject, leaderId: number) {
        const checkDiscipline = await this.subjectRepository.findByIdWithLeaderParentAndChild(disciplineId);
        if (checkDiscipline === undefined) {
            throw {notFoundError: `Subject with id '${disciplineId}' not found`};
        }
        if (!checkDiscipline.leaders.some(leader => leader.id === leaderId)) {
            throw {leaderError: `The subject '${disciplineId}' do not belong to the current user`}
        }
        const chapter = checkDiscipline.childSubjects.find(subject => subject.id === chapterId);
        if (chapter === undefined) {
            throw {notFoundError: `Subject with discipline id '${disciplineId}' and chapter id '${chapterId}' not found`};
        }
        chapterToUpdate.title = chapterToUpdate.title ? chapterToUpdate.title : chapter.title;
        chapterToUpdate.description = chapterToUpdate.description ? chapterToUpdate.description : chapter.description;
        chapterToUpdate.id = chapterId;
        return this.subjectRepository.updateSubject(chapterToUpdate);
    }

    async deleteChapter(disciplineId: number, chapterId: number, leaderId: number) {
        const checkDiscipline = await this.subjectRepository.findByIdWithLeaderParentAndChild(disciplineId);
        if (checkDiscipline === undefined) {
            throw {notFoundError: `Subject with id '${disciplineId}' not found`};
        }
        if (!checkDiscipline.leaders.some(leader => leader.id === leaderId)) {
            throw {leaderError: `The subject '${disciplineId}' do not belong to the current user`}
        }
        if (!checkDiscipline.childSubjects.some(subject => subject.id === chapterId)) {
            throw {notFoundError: `Subject with discipline id '${disciplineId}' and chapter id '${chapterId}' not found`};
        }

        return this.subjectRepository.deleteById(chapterId);
    }
}
