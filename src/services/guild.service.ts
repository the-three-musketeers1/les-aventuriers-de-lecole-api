import {inject, injectable} from "inversify";
import {User} from "../entities/User";
import {Guild} from "../entities/Guild";
import {TYPE} from "../constants/types";
import {GuildRepository} from "../repositories/GuildRepository";
import {SecurityUtil} from "../utils/security.util";

@injectable()
export class GuildService {

    constructor(
        @inject(TYPE.GuildRepository) private guildRepository: GuildRepository,
        @inject(TYPE.SecurityUtil) private securityUtil: SecurityUtil
    ) {
    }

    async save(guildName: string, user: User, deadlineAddCharacter: Date | null): Promise<Guild | null> {

        const checkGuild = await this.guildRepository.findByNameAndLeader(guildName, user)
        if (checkGuild) {
            return null;
        }
        const newGuild = this.guildRepository.createWithName(guildName);
        newGuild.leader = user;
        newGuild.token = await this.securityUtil.randomToken(16);
        newGuild.deadlineAddCharacter = deadlineAddCharacter;
        return await this.guildRepository.saveGuild(newGuild);
    }

    async find(leader: User): Promise<Guild[]> {
        return this.guildRepository.findByLeader(leader);
    }

    async findOne(id: number, leader: User): Promise<Guild | undefined> {
        return this.guildRepository.findByIdAndLeader(id, leader)
    }

    async update(id: number, guildName: string, leader: User, deadlineAddCharacter: Date | null): Promise<boolean> {

        const guildToUpdate = await this.findOne(id, leader);
        if (!guildToUpdate) {
            return false;
        }

        const checkGuild = await this.guildRepository.findByNameAndLeader(guildName, leader);

        if(checkGuild !== undefined && checkGuild !== null && +checkGuild.id !== +id) {
            return false;
        }

        guildToUpdate.name = (guildName) ? guildName : guildToUpdate.name;
        guildToUpdate.deadlineAddCharacter = (deadlineAddCharacter) ? deadlineAddCharacter : guildToUpdate.deadlineAddCharacter;

        await this.guildRepository.updateGuildNameAndDeadline(guildToUpdate.id, guildToUpdate.name,guildToUpdate.deadlineAddCharacter);

        return true;
    }

    async delete(id: number): Promise<boolean> {
        const guildToDelete = await this.guildRepository.findById(id);
        if (!guildToDelete) {
            return false;
        }
        await this.guildRepository.deleteById(id);
        return true;
    }

    async getGuildByNameAndToken(name: string, token: string) {
        return await this.guildRepository.findByNameAndToken(name, token);
    }

    async findGuildsByLeaderId(id: number): Promise<Guild[]> {
        return await this.guildRepository.findGuildsByLeaderId(id)
    }

    async findGuildByIdAndToken(guildId: number, token: string) {
        return this.guildRepository.findOne({
            where: {
                id: guildId,
                token
            }
        });
    }
}
