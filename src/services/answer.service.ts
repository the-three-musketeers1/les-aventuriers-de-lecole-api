import {inject, injectable} from "inversify";
import {TYPE} from "../constants/types";
import {AnswerRepository} from "../repositories/AnswerRepository";
import {Answer} from "../entities/Answer";

@injectable()
export class AnswerService {
    constructor(@inject(TYPE.AnswerRepository) private answerRepository: AnswerRepository) {
    }



    save(newAnswers: Answer) {
        return this.answerRepository.saveAnswer(newAnswers);
    }


    async find() {
        return this.answerRepository.findAll();
    }

    async findByQuestion(id: number) {
        return this.answerRepository.findByQuestion(id);
    }

    async findOne(id: number) {
        return this.answerRepository.findById(id);
    }

    async update(id: number, answer: Answer) {
        const checkAnswer = await this.findOne(id);
        if (!checkAnswer) {
            return false;
        }

        answer.content = (answer.content) ? answer.content : checkAnswer.content;
        answer.correct = (answer.correct != undefined || null) ? answer.correct : checkAnswer.correct;
        answer.question = (answer.question) ? answer.question : checkAnswer.question;
        answer.id = id;
        await this.answerRepository.updateAnswer(answer);
        return true;
    }

    async delete(id: number) {
        return await this.answerRepository.deleteById(id);
    }
}
