import {inject, injectable} from "inversify";
import {TYPE} from "../constants/types";
import {CharacterRepository} from "../repositories/CharacterRepository";
import {Character} from "../entities/Character";
import {CharacterInfoDTO} from "../entities/dto/characterInfoDTO";
import {User} from "../entities/User";
import {GuildRepository} from "../repositories/GuildRepository";
import {RoleRepository} from "../repositories/role.repository";

@injectable()
export class CharacterService {
    constructor(
        @inject(TYPE.CharacterRepository) private characterRepository: CharacterRepository,
        @inject(TYPE.GuildRepository) private guildRepository: GuildRepository,
        @inject(TYPE.RoleRepository) private roleRepository: RoleRepository
    ) {
    }

    async getCharacterInformations(id: number): Promise<Character> {
        return await this.characterRepository.findById(id);
    }

    async addExperience(id: number, experience: any) {
        const character = await this.characterRepository.findById(id);
        if (character.experience + experience > 2000) {
            await this.characterRepository.updateActionPoint(character.action_points + (character.experience + experience) / 2000, id);
            return await this.characterRepository.addExperienceAndlevelToCharacter(id,(character.experience + experience) % 2000, character.level + (character.experience + experience)/ 2000);
        } else {
            return await this.characterRepository.addExperienceToCharacter(id, character.experience + experience);
        }
    }

    mapCharacterInfo(character: Character) {
        const characterInfo = new CharacterInfoDTO();
        characterInfo.id = character.id;
        characterInfo.pseudo = character.name;
        characterInfo.action_points = character.action_points;
        characterInfo.level = character.level;
        characterInfo.experience = character.experience;
        characterInfo.role = character.role.name;
        characterInfo.board_bonus = character.role.board_bonus;
        characterInfo.board_bonus_cost = character.role.board_bonus_cost;
        characterInfo.guild_bonus_cost = character.role.guild_bonus_cost;
        characterInfo.guild_bonus = character.role.guild_bonus;
        characterInfo.first_name = character.user.firstname;
        characterInfo.last_name = character.user.lastname;
        characterInfo.email = character.user.email;
        characterInfo.guild_name = character.guilds.filter(guild => guild.parentGuild == null)[0].name;
        return characterInfo;
    }

    async saveByGuildAndRole(student: User, characterName: string, guildId: number, roleId: number) {
        const today = new Date();
        const checkGuild = await this.guildRepository.findOne(guildId);
        if (checkGuild === undefined) {
            throw {guildError: `Guild with id '${guildId}' not found`};
        }
        if (checkGuild.deadlineAddCharacter && checkGuild.deadlineAddCharacter.getTime() < today.getTime()) {
            throw {guildDeadlineError: `Guild deadline date to add character is exceed`};
        }
        const checkRole = await this.roleRepository.findOne(roleId);
        if (checkRole === undefined) {
            throw {roleError: `Role with id '${roleId}' not found`};
        }
        const checkCharacter = await this.characterRepository.findByStudentId(student.id)
        const checkCharacterGuild = checkCharacter.some(character => {
            if (character && character.guilds) {
                return character.guilds.some(guild => guild.id === checkGuild.id)
            }
            return false;
        });
        if (checkCharacter.length > 0 && checkCharacterGuild) {
            throw {characterAlreadyCreatedError: `Character with user id ${student.id} is already created`};
        }

        const newCharacter: Character = this.createCharacter(characterName, checkRole, student, checkGuild);

        return this.characterRepository.saveCharacter(newCharacter);
    }

    /**
     * Create character instance, not yet save in database
     * @param characterName
     * @param role
     * @param student
     * @param guild
     * @return Character
     */
    createCharacter(characterName, role, student, guild): Character {
        const newCharacter: Character = this.characterRepository.createCharacter(characterName);
        newCharacter.role = role;
        newCharacter.user = student;
        newCharacter.guilds.push(guild);
        return newCharacter;
    }

    async findStudentCharacters(student: User) {
        return this.characterRepository.findByStudentId(student.id);
    }

    async findOneStudentCharacter(characterId: number, student: User) {
        return await this.characterRepository.findOneStudentCharacterById(characterId, student.id)
    }

    async findCharactersByGuildId(id: number): Promise<CharacterInfoDTO[]> {
        const characters =  await this.characterRepository.findCharactersByGuildId(id)

        const charactersInformations = [];
        await characters.forEach(character => {
            charactersInformations.push(this.mapCharacterInfo(character));
        });
        return  charactersInformations;
    }


    async findLeadersByCharacter(characterId: number) {
        const leaders: User[] = [];
        const guilds = await this.guildRepository.find({
            relations: ['leader', 'characters']
        });

        for (let guild of guilds) {
            if (guild.characters.some(item => +item.id === +characterId)
                && leaders.every(item => +item.id !== +guild.leader.id)) {
                leaders.push(guild.leader);
            }
        }
        return leaders;
    }

    async findCharacterLeader(characterId: number) {
        const data = await this.characterRepository.findCharacterLeader(characterId);

        if(!data) {
            return null;
        }

        for (let guild of data.guilds) {
            while(!guild.leader) {
                if(!guild.parentGuild) {
                    break;
                }
                guild = guild.parentGuild;
            }
            if(guild.leader) {
                return guild.leader;
            }
        }

        return null;
    }
    async useActionPoints(numberOfBonusConsumed: number, characterId: number) {
        const character =  await this.characterRepository.findById(characterId);
        await this.characterRepository.updateActionPoint(character.action_points - character.role.guild_bonus_cost * numberOfBonusConsumed, characterId)

    }

    async delete(id: number) {
        return await this.characterRepository.deleteById(id);
    }
}
