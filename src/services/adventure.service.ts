import {inject, injectable} from "inversify";
import {TYPE} from "../constants/types";
import {AdventureRepository} from "../repositories/AdventureRepository";
import {Adventure} from "../entities/Adventure";
import {ExpeditionInformationsDTO} from "../entities/dto/ExpeditionInformationsDTO";
import {ExerciseInformationWithCharacterDTO} from "../entities/dto/ExerciseInformationWithCharacterDTO";

@injectable()
export class AdventureService {
    constructor(@inject(TYPE.AdventureRepository) private adventureRepository: AdventureRepository) {
    }

    save(newAdventures: Adventure) {
        return this.adventureRepository.saveAdventure(newAdventures);
    }

    async find() {
        return this.adventureRepository.findAll()
    }

    async findOne(id: number) {
        return this.adventureRepository.findById(id);
    }

    async update(id: number, adventure: Adventure) {
        const checkAdventure = await this.findOne(id);
        if (!checkAdventure) {
            return false;
        }

        adventure.title = (adventure.title) ? adventure.title : checkAdventure.title;
        adventure.description = (adventure.description) ? adventure.description : checkAdventure.description;
        adventure.life = (adventure.life) ? adventure.life : checkAdventure.life;
        adventure.mana = (adventure.mana) ? adventure.mana : checkAdventure.mana;
        adventure.isExpedition =  (adventure.isExpedition != undefined || null) ? adventure.isExpedition : checkAdventure.isExpedition;
        adventure.experienceBonus = (adventure.experienceBonus) ? adventure.experienceBonus : checkAdventure.experienceBonus;
        adventure.leader = (adventure.leader) ? adventure.leader : checkAdventure.leader;
        adventure.id = id;
        await this.adventureRepository.updateAdventure(adventure);
        return true;
    }

    async delete(id: number) {
        return this.adventureRepository.deleteAdventureById(id);
    }

    async findExpeditions() {
        return await this.adventureRepository.findExpeditions()
    }

    async findExpeditionsAvailableByCharacterId(id: number) {
        return this.adventureRepository.findAvailableExpeditionsByCharacterId(id)
    }

    async findExpeditionsByCharacterId(id: number) {
        return this.adventureRepository.findAllExpeditionsByCharacterId(id)
    }

    async findOneExpedition(id: number) {
        return await this.adventureRepository.findOneExpeditionById(id)
    }

    mapToExpeditionsInformations(expeditions: Adventure[]) {
        const expeditionsInformation: ExpeditionInformationsDTO[] = [];
        expeditions.forEach(expedition => {
            const expeditionInformation = new ExpeditionInformationsDTO();
            expeditionInformation.id = expedition.id;
            expeditionInformation.title = expedition.title;
            expeditionInformation.description = expedition.description;
            expeditionInformation.deadline = expedition.guildAdventures[0].deadline;
            expeditionInformation.accessible = expedition.guildAdventures[0].accessible;
            expeditionInformation.experienceBonus = expedition.experienceBonus;
            expeditionInformation.exercisesNumber = expedition.adventureExercises.length;
            const subjects = expedition.adventureExercises.map(adventure => adventure.exercise.subject);

            const disciplines:string[] = subjects.filter(subject =>subject.parentSubject == undefined).map(subject => subject.title);

            subjects.filter(subject =>subject.parentSubject != undefined).map(subject => subject.parentSubject.title).forEach(subject => disciplines.push(subject));
            const stringSet = new Set(disciplines);

            expeditionInformation.disciplines = [...stringSet];

            expeditionsInformation.push(expeditionInformation);
            expeditionInformation.done = expedition.characterAdventures.length == 1 ? expedition.characterAdventures[0].done : false;
        });
        return expeditionsInformation;
    }



    areAllExercisesDone(exercisesExpeditionOfCharacter: ExerciseInformationWithCharacterDTO[]) {
        let isFinished = true;
        exercisesExpeditionOfCharacter.forEach(exercise => {
            if (exercise.damageDone === null) {
                isFinished = false;
            }
        });
        return isFinished;
    }
}
