import {inject, injectable} from "inversify";
import {TYPE} from "../constants/types";
import {RoleRepository} from "../repositories/role.repository";
import {Role} from "../entities/Role";

@injectable()
export class RoleService {

    constructor(
        @inject(TYPE.RoleRepository) private roleRepository: RoleRepository
    ){
    }

    async save(name: string): Promise<Role> {
        const checkRole = await this.roleRepository.findByName(name);
        if (checkRole) {
            return null;
        }
        const newRole = this.roleRepository.create({name});
        return this.roleRepository.saveRole(newRole);
    }

    find(): Promise<Role[]> {
        return this.roleRepository.findAll();
    }

    async findOne(roleId: number): Promise<Role> {
        return this.roleRepository.findOne(roleId);
    }
}
