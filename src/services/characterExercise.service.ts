import {inject, injectable} from "inversify";
import {TYPE} from "../constants/types";
import {CharacterExerciseRepository} from "../repositories/CharacterExerciseRepository";
import {CharacterExercise} from "../entities/CharacterExercise";
import {StatisticsExerciseDTO} from "../entities/dto/StatisticsExerciseDTO";


@injectable()
export class CharacterExerciseService {
    constructor(@inject(TYPE.CharacterExerciseRepository) private characterExerciseRepository: CharacterExerciseRepository) {
    }

    save(adventureExerciseId: number,characterId: number) {
        const characterExercise = new CharacterExercise();
        characterExercise.adventureExerciseId = adventureExerciseId;
        characterExercise.characterId = characterId;
        return this.characterExerciseRepository.saveCharacterExercise(characterExercise);
    }

    async getAllByGuildId(guildId: number) {

        const guildExercises = await this.characterExerciseRepository.findAllByGuildId(guildId);
        return guildExercises.map(characterExercise => this.mapToStatisticsExercise(characterExercise))
    }

    async getAllByCharacterIdWithOptions(characterId: number, disciplineId: number, chapterId: number, beginDate: any, endDate: any) {
        const characterExercises = await this.characterExerciseRepository.findAllByCharacterIdWithOptions(characterId, disciplineId, chapterId, beginDate, endDate);
        return characterExercises.map(characterExercise => this.mapToStatisticsExercise(characterExercise))
    }

    mapToStatisticsExercise(characterExercise: CharacterExercise) {
        const statisticExercise = new StatisticsExerciseDTO();
        statisticExercise.characterExerciseId = characterExercise.id;
        statisticExercise.exerciseId  = characterExercise.adventureExercise.exerciseId;
        statisticExercise.createAt = characterExercise.createdAt;
        if (characterExercise.adventureExercise !== null && characterExercise.adventureExercise.exercise !== null) {
            if (characterExercise.adventureExercise.exercise.subject.parentSubject !== null ) {
                statisticExercise.discipline = characterExercise.adventureExercise.exercise.subject.parentSubject.title;
            }
            statisticExercise.chapter = characterExercise.adventureExercise.exercise.subject.title;
            statisticExercise.exerciseLife = characterExercise.adventureExercise.exercise.life;
        } else {
            throw Error('Missing data');
        }
        statisticExercise.goodAnswers = characterExercise.characterQuestions.filter(charQ => charQ.answer !== null && charQ.answer.correct === true).length;
        statisticExercise.badAnswers = characterExercise.characterQuestions.filter(charQ => charQ.answer === null || charQ.answer.correct === false).length;

        statisticExercise.damage = characterExercise.characterQuestions.filter(charQ => charQ.answer !== null && charQ.answer.correct === true).reduce((acc,item) => {
            acc += item.question.damage;
            return acc;
        },0);

        return statisticExercise;
    }
}
