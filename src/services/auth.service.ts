import {User} from "../entities/User";
import {TYPE} from "../constants/types";
import {UserRepository} from "../repositories/user.repository";
import {inject, injectable} from "inversify";
import {SecurityUtil} from "../utils/security.util";
import {SessionRepository} from "../repositories/session.repository";
import {Session} from "../entities/Session";

@injectable()
export class AuthService {

    constructor(
        @inject(TYPE.UserRepository) private userRepository: UserRepository,
        @inject(TYPE.SessionRepository) private sessionRepository: SessionRepository,
        @inject(TYPE.SecurityUtil) private securityUtil: SecurityUtil
    ) {
    }

    /**
     * Create user to be subscriber
     * @param subscriber {User}
     * @return {Promise<User|null>}
     */
    async subscribe(subscriber: User): Promise<User> {
        const checkUser = await this.userRepository.findOne({
            where: {
                email: subscriber.email
            }
        });
        if (checkUser) {
            return null;
        }

        subscriber.password = this.securityUtil.hashPassword(subscriber.password);
        return await this.userRepository.save(subscriber);
    }

    /**
     * Login user and create auth session
     * @param password {string}
     * @param email {string}
     * @param isTeacher {boolean}
     */
    async login(password: string, email: string, isTeacher: boolean): Promise<Session> {
        const user: User = await this.userRepository.findOne({
            where: {email, password: this.securityUtil.hashPassword(password)}
        });
        if (!user) {
            return null;
        }
        if (user.is_teacher !== isTeacher) {
            const typeUser = isTeacher ? "teacher" : "student";
            throw {loginError: `User is not ${typeUser}`};
        }
        const checkSession = await this.sessionRepository.findOne({
            where: {user: user}
        });
        if (checkSession) {
            await this.sessionRepository.delete(checkSession);
        }

        const hash: string = await this.securityUtil.randomToken();
        const session: Session = this.sessionRepository.create({
            token: JSON.stringify({hash, userId: user.id})
        });
        session.user = user;
        return this.sessionRepository.save(session);
    }

    /**
     * Get user by session's token
     * @param token {string}
     * @return {User}
     */
    async getUserBySessionToken(token: string): Promise<User> {
        const session = await this.sessionRepository.findOne({
            relations: ["user"],
            where: {
                token
            }
        });

        if (!session) {
            return undefined;
        }
        return session.user;
    }

    /**
     * Remove auth session of subscriber by user id
     * @param user
     */
    async logout(user: User): Promise<void> {
        const session = await this.sessionRepository.findOne({
            where: {
                user
            }
        });

        await this.sessionRepository.remove(session);
    }
}
