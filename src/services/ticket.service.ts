import {inject, injectable} from "inversify";
import {Ticket} from "../entities/Ticket";
import {TYPE} from "../constants/types";
import {TicketRepository} from "../repositories/TicketRepository";
import {SecurityUtil} from "../utils/security.util";
import {UserRepository} from "../repositories/user.repository";
import {CharacterRepository} from "../repositories/CharacterRepository";
import {Character} from "../entities/Character";
import {CharacterService} from "./character.service";
import {StatusService} from "./status.service";
import {User} from "../entities/User";
import {Status} from "../entities/Status";

@injectable()
export class TicketService {

    constructor(
        @inject(TYPE.TicketRepository) private ticketRepository: TicketRepository,
        @inject(TYPE.UserRepository) private userRepository: UserRepository,
        @inject(TYPE.CharacterRepository) private characterRepository: CharacterRepository,
        @inject(TYPE.CharacterService) private characterService: CharacterService,
        @inject(TYPE.StatusService) private statusService: StatusService,
        @inject(TYPE.SecurityUtil) private securityUtil: SecurityUtil
    ) {
    }

    async save(title: string, character: Character): Promise<Ticket | null> {
        const leader = await this.characterService.findCharacterLeader(character.id);
        if(!leader) {
            return null;
        }
        let newTicket = this.ticketRepository.create({
            title,
            leader,
            character,
            date: new Date().toJSON().slice(0, 10),
            status : await this.statusService.findOne(1)
        });

        return await this.ticketRepository.saveTicket(newTicket);
    }

    async findCharacterTickets(character: Character) {
        return await this.ticketRepository.findCharacterTickets(character);
    }

    async findTicketMessagesByCharacter(ticketId: number, character: Character) {
        const ticket = await this.ticketRepository.findByCharacterWithMessages(ticketId, character);
        if(!ticket) {
            return null;
        }
        return ticket.messages;
    }

    async findTicketMessagesByLeader(ticketId: number, leader: User) {
        const ticket = await this.ticketRepository.findByLeaderWithMessages(ticketId, leader);
        if(!ticket) {
            return null;
        }
        return ticket.messages;
    }

    async findLeaderTickets(leader: User) {
        return await this.ticketRepository.findAllByLeader(leader);
    }

    async updateTicketLeaderStatus(id: number, leader: User, newStatus: Status) {
        const updateTicket = this.ticketRepository.createTicketWithStatus(newStatus);
        const checkTicket = await this.ticketRepository.findByIdAndLeader(id, leader);
        if (checkTicket === undefined) {
            return false;
        }

        await this.ticketRepository.updateTicket(id, updateTicket);
        return true;
    }

    async findLeaderTicket(id: number, leader: User) {
        return this.ticketRepository.findByIdAndLeaderWIthInformations(id, leader);
    }
}
