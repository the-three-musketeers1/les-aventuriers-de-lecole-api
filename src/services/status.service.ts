import {inject, injectable} from "inversify";
import {TYPE} from "../constants/types";
import {StatusRepository} from "../repositories/StatusRepository";
import {Status} from "../entities/Status";

@injectable()
export class StatusService {

    constructor(
        @inject(TYPE.StatusRepository) private statusRepository: StatusRepository
    ){
    }

    async save(state: string): Promise<Status> {
        const checkStatus = await this.statusRepository.findByState(state);
        if (checkStatus) {
            return null;
        }
        const statusToSave = this.statusRepository.create({state});
        return await this.statusRepository.saveStatus(statusToSave);
    }

    async find(): Promise<Status[]> {
        return await this.statusRepository.findAll();
    }

    async findOne(statusId: number): Promise<Status> {
        return await this.statusRepository.findById(statusId);
    }
}
