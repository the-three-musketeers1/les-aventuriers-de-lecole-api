import {inject, injectable} from "inversify";
import {TYPE} from "../constants/types";
import {QuestionTypeRepository} from "../repositories/QuestionTypeRepository";
import {QuestionType} from "../entities/QuestionType";

@injectable()
export class QuestionTypeService {
    constructor(@inject(TYPE.QuestionTypeRepository) private questionTypeRepository: QuestionTypeRepository) {
    }

    /**
     * find all question types
     * @return Promise<QuestionType[]>
     */
    async find() {
        return this.questionTypeRepository.findAll();
    }

    /**
     * create new question type
     * @param newQuestionTypes {QuestionType}
     * @return Promise<QuestionType>
     */
    save(newQuestionTypes: QuestionType) {
        return this.questionTypeRepository.saveQuestionType(newQuestionTypes);
    }

    async findOne(id: number) {
        return this.questionTypeRepository.findById(id);
    }

    async update(id: number, questionType: QuestionType) {
        const checkQuestionType = await this.questionTypeRepository.findById(id);
        if (!checkQuestionType) {
            return false;
        }

        questionType.name = (questionType.name) ? questionType.name : checkQuestionType.name;
        questionType.id = id;
        await this.questionTypeRepository.updateQuestionType(questionType);
        return true;
    }

    async delete(id: number) {
        return this.questionTypeRepository.deleteById(id);
    }
}
