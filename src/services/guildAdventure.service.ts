import {inject, injectable} from "inversify";
import {TYPE} from "../constants/types";
import {GuildAdventureRepository} from "../repositories/GuildAdventureRepository";
import {GuildAdventure} from "../entities/GuildAdventure";

@injectable()
export class GuildAdventureService {
    constructor(@inject(TYPE.GuildAdventureRepository) private guildAdventureRepository: GuildAdventureRepository) {
    }

    async save(newGuildAdventures: GuildAdventure) {
        return await this.guildAdventureRepository.saveGuildAdventure(newGuildAdventures);
    }

    async delete(id: number) {
        return await this.guildAdventureRepository.deleteById(id);
    }

    async findGuildExpeditionsByLeader(id: number) {
        return await this.guildAdventureRepository.findByLeaderId(id);
    }

    async update(id: number, guildExpedition: GuildAdventure) {
        const checkGuildExpedition = await this.guildAdventureRepository.findOne(id);
        if (!checkGuildExpedition) {
            return false;
        }

        guildExpedition.deadline = (guildExpedition.deadline) ? guildExpedition.deadline : checkGuildExpedition.deadline;
        guildExpedition.accessible = (guildExpedition.accessible != undefined || null) ? guildExpedition.accessible : checkGuildExpedition.accessible;

        await this.guildAdventureRepository.updateAccessibilityAndDeadline(guildExpedition);
        return true;
    }
}
