import {inject, injectable} from "inversify";
import {UserRepository} from "../repositories/user.repository";
import {User} from "../entities/User";
import {TYPE} from "../constants/types";
import {ExerciseRepository} from "../repositories/ExerciseRepository";
import {AdventureRepository} from "../repositories/AdventureRepository";
import {SubjectRepository} from "../repositories/SubjectRepository";

@injectable()
export class UserService {
    constructor(@inject(TYPE.UserRepository) private userRepository: UserRepository,
                @inject(TYPE.ExerciseRepository) private exerciseRepository: ExerciseRepository,
                @inject(TYPE.AdventureRepository) private adventureRepository: AdventureRepository,
                @inject(TYPE.SubjectRepository) private subjectRepository: SubjectRepository) {

    }

    /**
     * save user by user model
     * @param newUser {User}
     * @return Promise<User>
     */
    async save(newUser: User) {
        return await this.userRepository.saveUser(newUser);
    }

    /**
     * find all users
     * @return Promise<User[]>
     */
    async find() {
        console.log("You are in V2")
        return await this.userRepository.findAll();
    }

    /**
     * find one user
     * @param id {number>
     * @return Promise<User>
     */
    async findOne(id: number) {
        return await this.userRepository.findById(id);
    }

    /**
     * update user
     * @param id {number}
     * @param user {User}
     * @return Promise<boolean>
     */
    async update(id: number, user: User) {
        const checkUser: User = await this.userRepository.findByIdWithLessInformations(id);
        if (!checkUser) {
            return false;
        }
        user.firstname = (user.firstname) ? user.firstname : checkUser.firstname;
        user.lastname = (user.lastname) ? user.lastname : checkUser.lastname;
        user.password = (user.password) ? user.password : checkUser.password;
        user.email = (user.email) ? user.email : checkUser.email;
        user.id = id;
        await this.userRepository.updateUser(user);
        return true;
    }

    /**
     * delete user by id
     * @param id {number}
     * @return Promise<boolean>
     */
    async delete(id: number) {
        const checkUser: User = await this.userRepository.findById(id);
        if (!checkUser) {
            return false;
        }
        await this.userRepository.deleteById(id);
        return true;
    }

    async findLeader() {
        return this.userRepository.findLeaders();
    }

    async findOneLeader(id: number) {
        return this.userRepository.findLeaderById(id);
    }

    async findLeaderAdventures(id: number) {
        return await this.adventureRepository.findByLeaderId(id);
    }

    async findLeaderSubjects(id: number) {
        const leader = await this.userRepository.findLeaderSubjects(id);
        if(!leader) {
            throw new Error("Can not find leader with id " + id);
        }

        return leader.subjects;
    }

    async findLeaderExercises(id: number) {
        return await this.exerciseRepository.findByLeaderId(id);
    }

    async findLeaderExercisesBySubject(leaderId: number, subjectId: number) {
        return await this.exerciseRepository.findByLeaderIdAndSubjectId(leaderId, subjectId);
    }

    async findLeaderExercisesByAdventure(leaderId: number, adventureId: number) {
        return await this.exerciseRepository.findByLeaderIdAndAdventureId(leaderId, adventureId);
    }

    async findLeaderExercisesByExpedition(leaderId: number, expeditionId: number) {
        return await this.exerciseRepository.findByLeaderIdAndExpeitionId(leaderId, expeditionId);
    }

    async findLeaderExpeditions(id: number) {
        return await this.adventureRepository.findLeaderExpeditions(id);
    }
}
