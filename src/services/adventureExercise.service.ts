import {inject, injectable} from "inversify";
import {TYPE} from "../constants/types";
import {AdventureExerciseRepository} from "../repositories/AdventureExerciseRepository";
import {AdventureExercise} from "../entities/AdventureExercise";
import {UserRepository} from "../repositories/user.repository";

@injectable()
export class AdventureExerciseService {
    constructor(@inject(TYPE.AdventureExerciseRepository) private adventureExerciseRepository: AdventureExerciseRepository,
                @inject(TYPE.UserRepository) private userRepository: UserRepository) {
    }

    async getByExerciseIdAndExpeditionId(exerciseId: number, adventureId: number) : Promise<AdventureExercise> {
        const adventureExercise = new AdventureExercise();
        adventureExercise.adventureId = adventureId;
        adventureExercise.exerciseId = exerciseId;
        return await this.adventureExerciseRepository.findExistingAdventureExercise(adventureExercise);
    }

    async findByLeader(leaderId: number) {
        return await this.adventureExerciseRepository.findByLeader(leaderId)
    }

    async save(expeditionExercise: AdventureExercise) {
        const alreadyExists = await this.adventureExerciseRepository.findExistingAdventureExercise(expeditionExercise)
        if(alreadyExists) {
            return null;
        }
        return await this.adventureExerciseRepository.saveAdventureExercise(expeditionExercise);
    }

    async delete(id: number) {
        return await this.adventureExerciseRepository.deleteById(id);
    }
}
