import {inject, injectable} from "inversify";
import {TYPE} from "../constants/types";
import {ExerciseRepository} from "../repositories/ExerciseRepository";
import {Exercise} from "../entities/Exercise";
import {ExerciseInformationDTO} from "../entities/dto/ExerciseInformationDTO";
import {SubjectService} from "./subject.service";
import {QuestionRepository} from "../repositories/QuestionRepository";
import {ExerciseInformationWithCharacterDTO} from "../entities/dto/ExerciseInformationWithCharacterDTO";
import {AdventureRepository} from "../repositories/AdventureRepository";

@injectable()
export class ExerciseService {
    constructor(@inject(TYPE.ExerciseRepository) private exerciseRepository: ExerciseRepository,
                @inject(TYPE.SubjectService) private subjectService: SubjectService,
                @inject(TYPE.QuestionRepository) private questionRepository: QuestionRepository,
                @inject(TYPE.AdventureRepository) private adventureRepository: AdventureRepository) {
    }

    async findExerciseWithQuestions(id: number) {
        return this.exerciseRepository.findExerciseWithQuestions(id);
    }

    async findExercisesOfExpeditions(id: number) {
        return await this.exerciseRepository.findExercisesByExpeditionId(id);
    }

    async findExercisesOfExpeditionsWithCharacterContent(expeditionId: number, characterId: number) {
        return  await this.exerciseRepository.findExercisesByExpeditionIdWIthCharacterContent(expeditionId, characterId);
    }

    async findExercisesOfGuild(id: number) {
        const exercises = await this.exerciseRepository.findExercisesByGuildId(id);

        return this.transformExercisesIntoExercisesExpeditions(exercises);

    }

    transformExercisesIntoExercisesExpeditions(exercises: Exercise[]) {
        const exercisesInformations = [];
        exercises.map(exercise => {
            exercisesInformations.push(this.transformExerciseIntoExerciseExpedition(exercise));

        });
        return exercisesInformations;
    }

    transformListOfExercisesIntoExercisesExpeditionsOfCharacter(exercises: Exercise[]) : ExerciseInformationWithCharacterDTO[] {
        const exercisesInformationsWithCharacter = [];
        exercises.map(exercise => {
            exercisesInformationsWithCharacter.push(this.transformExercisesIntoExercisesExpeditionsOfCharacter(exercise));
        });
        return exercisesInformationsWithCharacter;

    }

    transformExercisesIntoExercisesExpeditionsOfCharacter(exercise: Exercise) {
        const exerciseInfo = new ExerciseInformationWithCharacterDTO();
        exerciseInfo.id = exercise.id;
        exerciseInfo.life = exercise.life;
        exerciseInfo.description = exercise.description;
        exerciseInfo.experience = exercise.experience;
        if (exercise.subject !== null) {
            if (exercise.subject.parentSubject !== null) {
                exerciseInfo.discipline = exercise.subject.parentSubject.title;
                exerciseInfo.chapter = exercise.subject.title;
            } else {
                exerciseInfo.discipline = exercise.subject.title;
                exerciseInfo.chapter = "";
            }
        } else {
            exerciseInfo.discipline = "";
            exerciseInfo.chapter = "";
        }
        if (exercise.questions != null) {
            exerciseInfo.questionNumber = exercise.questions.length;
        }
        if (exercise.adventureExercises[0].characterExercises != null && exercise.adventureExercises[0].characterExercises.length > 0)  {
            const history = exercise.adventureExercises[0].characterExercises;
            history.sort((h1,h2) => h1.createdAt.getTime() - h2.createdAt.getTime());

            let maxSum = 0;
            let maxGoodAnswers = 0;
            history.forEach(characterExercise => {
                let sum = 0;
                let goodAnswers = 0;
                characterExercise.characterQuestions.forEach(characterAnswer => {
                    if (characterAnswer.answer != null &&  characterAnswer.answer.correct == true) {
                        goodAnswers++;
                        const question = exercise.questions.find(question => question.id == characterAnswer.questionId);
                        if (question != null) {
                            sum += question.damage
                        }
                    }
                });
                if (goodAnswers > maxGoodAnswers) {
                    maxSum = sum;
                    maxGoodAnswers = goodAnswers;
                }
            });
            if (exerciseInfo.questionNumber !== null && maxGoodAnswers !== null) {
                exerciseInfo.mark = (maxGoodAnswers * 20) / exerciseInfo.questionNumber;
                exerciseInfo.mark = Math.round(exerciseInfo.mark);
            }
            exerciseInfo.damageDone = maxSum;
            exerciseInfo.validated = !((exerciseInfo.life - exerciseInfo.damageDone) > 0);
        } else {
            exerciseInfo.damageDone = null;
            exerciseInfo.validated = false;
        }
        exerciseInfo.name = exercise.name;
        return exerciseInfo;
    }

    transformExerciseIntoExerciseExpedition(exercise: Exercise) {
        const exerciseInfo = new ExerciseInformationDTO();
        exerciseInfo.id = exercise.id;
        exerciseInfo.life = exercise.life;
        if (exercise.subject !== null) {
            if (exercise.subject.parentSubject !== null) {
                exerciseInfo.discipline = exercise.subject.parentSubject.title;
                exerciseInfo.chapter = exercise.subject.title;
            } else {
                exerciseInfo.discipline = exercise.subject.title;
                exerciseInfo.chapter = "";
            }
        } else {
            exerciseInfo.discipline = "";
            exerciseInfo.chapter = "";
        }
        exerciseInfo.name = exercise.name;
        return exerciseInfo;
    }



    save(newExercises: Exercise) {
        return this.exerciseRepository.saveExercise(newExercises);
    }


    async find() {
        return this.exerciseRepository.findAll();
    }

    async findOne(id: number) {
        return this.exerciseRepository.findById(id);
    }

    async update(id: number, exercise: Exercise) {
        const checkExercise = await this.findOne(id);
        if (!checkExercise) {
            return false;
        }

        exercise.name = (exercise.name) ? exercise.name : checkExercise.name;
        exercise.description = (exercise.description) ? exercise.description : checkExercise.description;
        exercise.life = (exercise.life) ? exercise.life : checkExercise.life;
        exercise.subject = (exercise.subject) ? exercise.subject : checkExercise.subject;
        exercise.leader = (exercise.leader) ? exercise.leader : checkExercise.leader;
        exercise.id = id;
        await this.exerciseRepository.updateExercise(exercise);
        return true;
    }

    async delete(id: number) {
        return await this.exerciseRepository.deleteExerciseById(id);
    }

    async findByAdventure(id: number) {
        return await this.exerciseRepository.findExercisesByExpeditionId(id);
    }

    async findBySubject(id: number) {
        const subject = await this.subjectService.findOne(id);
        if(!subject) {
            throw new Error("Can not find subject with id " + id);
        }

        return await this.exerciseRepository.findBySubject(subject);
    }

    async findExerciseQuestions(id: number) {
        return await this.questionRepository.createQueryBuilder('question')
            .innerJoinAndSelect('question.exercise', 'exercise')
            .innerJoinAndSelect('question.questionType', 'questionType')
            .where('exercise.id = :id', {id})
            .orderBy('question.sequence_number', 'ASC')
            .getMany();
    }

    async findExerciseExpeditions(id: number) {
        const exercise = await this.exerciseRepository.findByIdWithAdventure(id);
        if(!exercise) {
            return null;
        }
        const expeditions = [];
        for (let adventureExercise of exercise.adventureExercises) {
            if(adventureExercise.adventure.isExpedition) {
                expeditions.push(adventureExercise.adventure);
            }
        }
        return expeditions;
    }

    async findIfExerciseIsLinkedToCharacter(id: number, characterId: number) {
        return this.exerciseRepository.findIfExerciseIsLinkedToCharacter(id, characterId);
    }

    async findIfExpeditionLinkedToUser(expeditionId: number, characterId: number) {
        return this.adventureRepository.findIfExpeditonIsLinkedToCharacter(expeditionId, characterId);
    }
}
