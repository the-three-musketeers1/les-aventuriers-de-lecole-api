import {inject, injectable} from "inversify";
import {TYPE} from "../constants/types";
import {MessageRepository} from "../repositories/MessageRepository";
import {TicketRepository} from "../repositories/TicketRepository";
import {Message} from "../entities/Message";

@injectable()
export class MessageService {

    constructor(
        @inject(TYPE.MessageRepository) private messageRepository: MessageRepository,
        @inject(TYPE.TicketRepository) private ticketRepository: TicketRepository
    ) {
    }

    async save(ticketId: number, content: string, sentByCharacter: boolean, characterId: number) {
        const ticket = await this.ticketRepository.findByIdWithCharacter(ticketId);
        if (!ticket || ticket.character.id !== characterId) {
            return null;
        }

        const message = {
            ticket,
            content,
            sentByCharacter
        } as Message;
        return await this.messageRepository.saveMessage(message);
    }

    async saveByTeacher(ticketId: number, content: string, sentByCharacter: boolean, leaderId: number) {
        const ticket = await this.ticketRepository.findByIdWithLeader(ticketId);
        if (!ticket || ticket.leader.id !== leaderId) {
            return null;
        }

        const message = {
            ticket,
            content,
            sentByCharacter
        } as Message;
        return await this.messageRepository.saveMessage(message);
    }
}
