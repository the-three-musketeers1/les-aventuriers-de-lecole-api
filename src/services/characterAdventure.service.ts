import {inject, injectable} from "inversify";
import {TYPE} from "../constants/types";
import {CharacterAdventureRepository} from "../repositories/CharacterAdventureRepository";
import {CharacterAdventure} from "../entities/CharacterAdventure";

@injectable()
export class CharacterAdventureService {
    constructor(@inject(TYPE.CharacterAdventureRepository) private characterAdventureRepository: CharacterAdventureRepository) {

    }

    findByExpeditionIdAndCharacterId(expeditionId: number, characterId: number) {
        return this.characterAdventureRepository.findByExpeditionIdAndCharacterId(expeditionId, characterId);
    }

    createCharacterAdventure(expeditionId: number, characterId: number) {
        const characterAdventure = new CharacterAdventure();
        characterAdventure.characterId = characterId;
        characterAdventure.adventureId = expeditionId;
        characterAdventure.done = true;
        characterAdventure.questionId = null;
        this.characterAdventureRepository.saveCharacterAdventure(characterAdventure);
    }
}
