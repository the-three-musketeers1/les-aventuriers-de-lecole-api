import {inject, injectable} from "inversify";
import {TYPE} from "../constants/types";
import {QuestionRepository} from "../repositories/QuestionRepository";
import {Question} from "../entities/Question";

@injectable()
export class QuestionService {
    constructor(@inject(TYPE.QuestionRepository) private questionRepository: QuestionRepository) {
    }


    save(newQuestions: Question) {
        return this.questionRepository.saveQuestion(newQuestions);
    }


    async find() {
        return this.questionRepository.findAllWithExerciseAndType();
    }

    async findOne(id: number) {
        return this.questionRepository.findByIdWithExerciseAndType(id);
    }

    async update(id: number, question: Question) {
        const checkQuestion = await this.findOne(id);
        if (!checkQuestion) {
            return false;
        }

        question.wording = (question.wording) ? question.wording : checkQuestion.wording;
        question.sequence_number = (question.sequence_number) ? question.sequence_number : checkQuestion.sequence_number;
        question.damage = (question.damage) ? question.damage : checkQuestion.damage;
        question.time = (question.time) ? question.time : checkQuestion.time;
        question.exercise = (question.exercise) ? question.exercise : checkQuestion.exercise;
        question.questionType = (question.questionType) ? question.questionType : checkQuestion.questionType;
        question.id = id;
        await this.questionRepository.updateQuestion(question);
        return true;
    }

    async delete(id: number) {
        await this.questionRepository.deleteById(id);
    }

    async findQuestionAnswers(id: number) {
        const question = await this.questionRepository.findByIdWithAnswers(id);
        if (!question) {
            throw new Error('Can not find question with id ' + id);
        }
        return question.answers;
    }

    async findBySequenceNumber(sequence_number: number) {
        return await this.questionRepository.findBySequenceNumber(sequence_number);
    }
}
