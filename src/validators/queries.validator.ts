import * as express from "express";

export function isQueryString(queryName: string, isRequired = false) {
    return (req: express.Request, res: express.Response, next: express.NextFunction) => {
        const checkString = req.query[queryName];
        if (checkString && (checkString.length >= 1 || !isRequired)) {
            next();
            return;
        }
        res.status(403).send({error: `query ${queryName} is not correct`});
    }
}
