import * as express from "express";

/**
 * Check is the param of request is number
 * @param param {string} : request param name
 * @param isInteger {boolean} : check if is integer
 * @return Function
 */
export function isParamNumber(param: string, isInteger: boolean = true) {
    return (req: express.Request, res: express.Response, next: express.NextFunction) => {
        // const checkId = Number(req.params[param]);
        // if (isNaN(checkId) || isInteger && !Number.isInteger(checkId)) {
        //     res.status(403);
        //     res.send(param + ": is not number");
        //     return;
        // }
        next();
    }
}


export function isId(param: string) {
    return (req: express.Request, res: express.Response, next: express.NextFunction) => {
        // const checkId = Number(req.params[param]);
        // if (checkId <= 0) {
        //     res.status(403);
        //     res.send(param + ": is not number");
        //     return;
        // }
        next();
    }
}
