import * as express from "express";

export function isBodyString(bodyName: string) {
    return (req: express.Request, res: express.Response, next: express.NextFunction) => {
        const checkBody: string = req.body[bodyName];
        if (checkBody && checkBody.length > 0) {
            next();
            return;
        }
        res.status(403).end();
    }
}

export function isBodyDateGreaterThan(
    isOptional: boolean,
    propertyDate: string,
    isComparedProperty: boolean = false,
    comparedPropertyDate: string = new Date().toISOString()
) {
    return (req: express.Request, res: express.Response, next: express.NextFunction) => {
        if (req.body[propertyDate] == undefined && isOptional) {
            next();
            return;
        }
        const concernedDate = new Date(req.body[propertyDate]);
        const comparedDate = isComparedProperty ? new Date(req.body[comparedPropertyDate]) : new Date(comparedPropertyDate);

        if (concernedDate.getTime() > comparedDate.getTime()) {
            next();
            return;
        }
        res.status(403).send({error: `property '${propertyDate}' have to be greater than ${comparedDate.toDateString()}`});
    }
}

export function isBodyDefined(propertyName: string, isNotNull: boolean) {
    return (req: express.Request, res: express.Response, next: express.NextFunction) => {
        if (req.body[propertyName] !== undefined) {
            if (!isNotNull || req.body[propertyName] !== null) {
                next();
                return;
            }
            res.status(403).send({error: `property '${propertyName}' value is null `});
            return;
        }

        res.status(403).send({error: `property '${propertyName}' is not defined `});
    }
}
