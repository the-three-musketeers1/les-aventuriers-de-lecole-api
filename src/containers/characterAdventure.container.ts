import {Container} from "inversify";
import {CharacterAdventureRepository} from "../repositories/CharacterAdventureRepository";
import {TYPE} from "../constants/types";
import {getCustomRepository} from "typeorm";

import {CharacterAdventureController} from "../controllers/characterAdventure.controller";
import {CharacterAdventureService} from "../services/characterAdventure.service";

export function characterAdventureContainer(container: Container) {
    container.bind<CharacterAdventureRepository>(TYPE.CharacterAdventureRepository).toConstantValue(getCustomRepository(CharacterAdventureRepository));
    container.bind<CharacterAdventureService>(TYPE.CharacterAdventureService).to(CharacterAdventureService);
    container.bind<CharacterAdventureController>(TYPE.CharacterAdventureController).to(CharacterAdventureController);
}
