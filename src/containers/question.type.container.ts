import {Container} from "inversify";
import {QuestionTypeService} from "../services/question.type.service";
import {TYPE} from "../constants/types";
import {QuestionTypeRepository} from "../repositories/QuestionTypeRepository";
import {getCustomRepository} from "typeorm";
import {QuestionTypeController} from "../controllers/question.type.controller";

export function questionTypeContainer(container: Container) {
    container.bind<QuestionTypeRepository>(TYPE.QuestionTypeRepository).toConstantValue(getCustomRepository(QuestionTypeRepository));
    container.bind<QuestionTypeService>(TYPE.QuestionTypeService).to(QuestionTypeService);
    container.bind<QuestionTypeController>(TYPE.QuestionTypeController).to(QuestionTypeController);
}
