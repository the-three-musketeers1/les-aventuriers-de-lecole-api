import {Container} from "inversify";
import {StatusRepository} from "../repositories/StatusRepository";
import {TYPE} from "../constants/types";
import {getCustomRepository} from "typeorm";
import {StatusService} from "../services/status.service";

export function statusContainer(container: Container) {
    container.bind<StatusRepository>(TYPE.StatusRepository).toConstantValue(getCustomRepository(StatusRepository));
    container.bind<StatusService>(TYPE.StatusService).to(StatusService);
}
