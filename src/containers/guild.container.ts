import {Container} from "inversify";
import {TYPE} from "../constants/types";
import {getCustomRepository} from "typeorm";

import {GuildRepository} from "../repositories/GuildRepository";
import {GuildService} from "../services/guild.service";
import {GuildController} from "../controllers/guild.controller";

export function guildContainer(container: Container) {
    container.bind<GuildRepository>(TYPE.GuildRepository).toConstantValue(getCustomRepository(GuildRepository));
    container.bind<GuildService>(TYPE.GuildService).to(GuildService);
    container.bind<GuildController>(TYPE.GuildController).to(GuildController);
}
