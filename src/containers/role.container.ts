import {Container} from "inversify";
import {RoleRepository} from "../repositories/role.repository";
import {TYPE} from "../constants/types";
import {getCustomRepository} from "typeorm";
import {RoleService} from "../services/role.service";

export function roleContainer(container: Container) {
    container.bind<RoleRepository>(TYPE.RoleRepository).toConstantValue(getCustomRepository(RoleRepository));
    container.bind<RoleService>(TYPE.RoleService).to(RoleService);
}
