import {Container} from "inversify";
import {TYPE} from "../constants/types";
import {SubjectRepository} from "../repositories/SubjectRepository";
import {getCustomRepository} from "typeorm";
import {SubjectController} from "../controllers/subject.controller";
import {SubjectService} from "../services/subject.service";

export function subjectContainer(container: Container) {
    container.bind<SubjectRepository>(TYPE.SubjectRepository).toConstantValue(getCustomRepository(SubjectRepository));
    container.bind<SubjectService>(TYPE.SubjectService).to(SubjectService);
    container.bind<SubjectController>(TYPE.SubjectController).to(SubjectController);
}
