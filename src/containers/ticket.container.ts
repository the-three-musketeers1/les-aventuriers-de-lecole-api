import {Container} from "inversify";
import {TYPE} from "../constants/types";
import {TicketRepository} from "../repositories/TicketRepository";
import {getCustomRepository} from "typeorm";
import {TicketController} from "../controllers/ticket.controller";
import {TicketService} from "../services/ticket.service";

export function ticketContainer(container: Container) {
    container.bind<TicketRepository>(TYPE.TicketRepository).toConstantValue(getCustomRepository(TicketRepository));
    container.bind<TicketService>(TYPE.TicketService).to(TicketService);
    container.bind<TicketController>(TYPE.TicketController).to(TicketController);
}
