import {Container} from "inversify";
import {UserRepository} from "../repositories/user.repository";
import {TYPE} from "../constants/types";
import {getCustomRepository} from "typeorm";
import {UserService} from "../services/user.service";
import {UserController} from "../controllers/user.controller";

export function userContainer(container: Container) {
    container.bind<UserRepository>(TYPE.UserRepository).toConstantValue(getCustomRepository(UserRepository));
    container.bind<UserService>(TYPE.UserService).to(UserService);
    container.bind<UserController>(TYPE.UserController).to(UserController);
}
