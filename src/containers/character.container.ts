import {Container} from "inversify";
import {TYPE} from "../constants/types";
import {getCustomRepository} from "typeorm";
import {CharacterController} from "../controllers/character.controller";
import {CharacterRepository} from "../repositories/CharacterRepository";
import {CharacterService} from "../services/character.service";

export function characterContainer(container: Container) {
    container.bind<CharacterRepository>(TYPE.CharacterRepository).toConstantValue(getCustomRepository(CharacterRepository));
    container.bind<CharacterService>(TYPE.CharacterService).to(CharacterService);
    container.bind<CharacterController>(TYPE.CharacterController).to(CharacterController);
}
