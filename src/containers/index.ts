import {Container} from "inversify";
import {userContainer} from "./user.container";
import {questionTypeContainer} from "./question.type.container";
import {questionContainer} from "./question.container";
import {answerContainer} from "./answer.container";
import {TYPE} from "../constants/types";
import {SecurityUtil} from "../utils/security.util";
import {SessionRepository} from "../repositories/session.repository";
import {getCustomRepository} from "typeorm";
import {exerciseContainer} from "./exercise.container";
import {subjectContainer} from "./subject.container";
import {characterQuestionContainer} from "./characterQuestion.container";
import {adventureContainer} from "./adventure.container";
import {characterExerciseContainer} from "./characterExercise.container";
import {adventureExerciseContainer} from "./adventureExercise.container";
import {characterContainer} from "./character.container";
import {authContainer} from "./auth.container";
import {guildContainer} from "./guild.container";
import {roleContainer} from "./role.container";
import {guildAdventureContainer} from "./guildAdventure.container";
import {characterAdventureContainer} from "./characterAdventure.container";
import {ticketContainer} from "./ticket.container";
import {statusContainer} from "./status.container";
import {messageContainer} from "./message.container";

export function loadContainers(): Container {
    const container = new Container();
    userContainer(container);
    questionTypeContainer(container);
    questionContainer(container);
    answerContainer(container);
    exerciseContainer(container);
    subjectContainer(container);
    characterQuestionContainer(container);
    adventureContainer(container);
    characterExerciseContainer(container);
    adventureExerciseContainer(container);
    characterContainer(container);
    authContainer(container);
    guildContainer(container);
    roleContainer(container);
    guildAdventureContainer(container);
    characterAdventureContainer(container);
    ticketContainer(container);
    statusContainer(container);
    messageContainer(container);

    container.bind<SecurityUtil>(TYPE.SecurityUtil).to(SecurityUtil);
    container.bind<SessionRepository>(TYPE.SessionRepository).toConstantValue(getCustomRepository(SessionRepository));

    return container;
}
