import {Container} from "inversify";
import {TYPE} from "../constants/types";
import {AdventureRepository} from "../repositories/AdventureRepository";
import {getCustomRepository} from "typeorm";
import {AdventureController} from "../controllers/adventure.controller";
import {AdventureService} from "../services/adventure.service";
import {AdventureExerciseService} from "../services/adventureExercise.service";
import {AdventureExerciseRepository} from "../repositories/AdventureExerciseRepository";

export function adventureExerciseContainer(container: Container) {
    container.bind<AdventureExerciseRepository>(TYPE.AdventureExerciseRepository).toConstantValue(getCustomRepository(AdventureExerciseRepository));
    container.bind<AdventureExerciseService>(TYPE.AdventureExerciseService).to(AdventureExerciseService);
}
