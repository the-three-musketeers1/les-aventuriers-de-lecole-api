import {Container} from "inversify";
import {QuestionService} from "../services/question.service";
import {TYPE} from "../constants/types";
import {QuestionRepository} from "../repositories/QuestionRepository";
import {getCustomRepository} from "typeorm";
import {QuestionController} from "../controllers/question.controller";

export function questionContainer(container: Container) {
    container.bind<QuestionRepository>(TYPE.QuestionRepository).toConstantValue(getCustomRepository(QuestionRepository));
    container.bind<QuestionService>(TYPE.QuestionService).to(QuestionService);
    container.bind<QuestionController>(TYPE.QuestionController).to(QuestionController);
}
