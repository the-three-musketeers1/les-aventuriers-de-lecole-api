import {Container} from "inversify";
import {TYPE} from "../constants/types";
import {AdventureRepository} from "../repositories/AdventureRepository";
import {getCustomRepository} from "typeorm";
import {AdventureController} from "../controllers/adventure.controller";
import {AdventureService} from "../services/adventure.service";

export function adventureContainer(container: Container) {
    container.bind<AdventureRepository>(TYPE.AdventureRepository).toConstantValue(getCustomRepository(AdventureRepository));
    container.bind<AdventureService>(TYPE.AdventureService).to(AdventureService);
    container.bind<AdventureController>(TYPE.AdventureController).to(AdventureController);
}
