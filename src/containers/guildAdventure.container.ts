import {Container} from "inversify";
import {TYPE} from "../constants/types";
import {getCustomRepository} from "typeorm";
import {GuildAdventureController} from "../controllers/guildAdventure.controller";
import {GuildAdventureRepository} from "../repositories/GuildAdventureRepository";
import {GuildAdventureService} from "../services/guildAdventure.service";

export function guildAdventureContainer(container: Container) {
    container.bind<GuildAdventureRepository>(TYPE.GuildAdventureRepository).toConstantValue(getCustomRepository(GuildAdventureRepository));
    container.bind<GuildAdventureService>(TYPE.GuildAdventureService).to(GuildAdventureService);
    container.bind<GuildAdventureController>(TYPE.GuildAdventureController).to(GuildAdventureController);
}
