import {Container} from "inversify";
import {TYPE} from "../constants/types";
import {MessageRepository} from "../repositories/MessageRepository";
import {getCustomRepository} from "typeorm";
import {MessageService} from "../services/message.service";
import {MessageController} from "../controllers/message.controller";

export function messageContainer(container: Container) {
    container.bind<MessageRepository>(TYPE.MessageRepository).toConstantValue(getCustomRepository(MessageRepository));
    container.bind<MessageService>(TYPE.MessageService).to(MessageService);
    container.bind<MessageController>(TYPE.MessageController).to(MessageController);
}
