import {Container} from "inversify";
import {AuthService} from "../services/auth.service";
import {TYPE} from "../constants/types";
import {AuthMiddleware} from "../middlewares/auth.middleware";
import {AuthStudentMiddleware} from "../middlewares/auth/auth.student.middleware";
import {AuthTeacherMiddleware} from "../middlewares/auth/auth.teacher.middleware";
import {AuthCharacterMiddleware} from "../middlewares/auth/auth.character.middleware";
import {AuthOnlyMiddleware} from "../middlewares/auth.only.middleware";
import {AuthOnlyTeacherMiddleware} from "../middlewares/auth/auth.only.teacher.middleware";

export function authContainer(container: Container) {
    container.bind<AuthService>(TYPE.AuthService).to(AuthService);
    container.bind<AuthMiddleware>(TYPE.AuthMiddleware).to(AuthMiddleware);
    container.bind<AuthStudentMiddleware>(TYPE.AuthStudentMiddleware).to(AuthStudentMiddleware);
    container.bind<AuthTeacherMiddleware>(TYPE.AuthTeacherMiddleware).to(AuthTeacherMiddleware);
    container.bind<AuthCharacterMiddleware>(TYPE.AuthCharacterMiddleware).to(AuthCharacterMiddleware);
    container.bind<AuthOnlyMiddleware>(TYPE.AuthOnlyMiddleware).to(AuthOnlyMiddleware);
    container.bind<AuthOnlyTeacherMiddleware>(TYPE.AuthOnlyTeacherMiddleware).to(AuthOnlyTeacherMiddleware);
}
