import {Container} from "inversify";
import {AnswerService} from "../services/answer.service";
import {TYPE} from "../constants/types";
import {AnswerRepository} from "../repositories/AnswerRepository";
import {getCustomRepository} from "typeorm";
import {AnswerController} from "../controllers/answer.controller";

export function answerContainer(container: Container) {
    container.bind<AnswerRepository>(TYPE.AnswerRepository).toConstantValue(getCustomRepository(AnswerRepository));
    container.bind<AnswerService>(TYPE.AnswerService).to(AnswerService);
    container.bind<AnswerController>(TYPE.AnswerController).to(AnswerController);
}
