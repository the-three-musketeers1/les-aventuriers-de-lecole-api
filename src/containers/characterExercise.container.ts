import {Container} from "inversify";
import {TYPE} from "../constants/types";
import {getCustomRepository} from "typeorm";
import {CharacterQuestionController} from "../controllers/characterQuestion.controller";
import {CharacterExerciseService} from "../services/characterExercise.service";
import {CharacterExerciseRepository} from "../repositories/CharacterExerciseRepository";
import {CharacterExerciseController} from "../controllers/characterExercise.controller";

export function characterExerciseContainer(container: Container) {
    container.bind<CharacterExerciseRepository>(TYPE.CharacterExerciseRepository).toConstantValue(getCustomRepository(CharacterExerciseRepository));
    container.bind<CharacterExerciseService>(TYPE.CharacterExerciseService).to(CharacterExerciseService);
    container.bind<CharacterExerciseController>(TYPE.CharacterExerciseController).to(CharacterExerciseController);
}
