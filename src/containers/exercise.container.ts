import {Container} from "inversify";
import {TYPE} from "../constants/types";
import {getCustomRepository} from "typeorm";
import {ExerciseRepository} from "../repositories/ExerciseRepository";
import {ExerciseService} from "../services/exercise.service";
import {ExerciseController} from "../controllers/exercise.controller";

export function exerciseContainer(container: Container) {
    container.bind<ExerciseRepository>(TYPE.ExerciseRepository).toConstantValue(getCustomRepository(ExerciseRepository));
    container.bind<ExerciseService>(TYPE.ExerciseService).to(ExerciseService);
    container.bind<ExerciseController>(TYPE.ExerciseController).to(ExerciseController);
}
