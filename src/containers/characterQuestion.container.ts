import {Container} from "inversify";
import {TYPE} from "../constants/types";
import {getCustomRepository} from "typeorm";
import {CharacterQuestionRepository} from "../repositories/CharacterQuestionRepository";
import {CharacterQuestionService} from "../services/characterQuestion.service";
import {CharacterQuestionController} from "../controllers/characterQuestion.controller";

export function characterQuestionContainer(container: Container) {
    container.bind<CharacterQuestionRepository>(TYPE.CharacterQuestionRepository).toConstantValue(getCustomRepository(CharacterQuestionRepository));
    container.bind<CharacterQuestionService>(TYPE.CharacterQuestionService).to(CharacterQuestionService);
    container.bind<CharacterQuestionController>(TYPE.CharacterQuestionController).to(CharacterQuestionController);
}
