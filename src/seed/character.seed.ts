import {Container} from "inversify";
import {Character} from "../entities/Character";
import {TYPE} from "../constants/types";
import {CharacterRepository} from "../repositories/CharacterRepository";
import {SeedHelper} from "./seed.helper";

export class CharacterSeed {
    private static data(): Character[] {
        return [
            {
                name: "character1",
                guilds: []
            } as Character,
            {
                name: "character2",
                guilds: []
            } as Character,
            {
                name: "character3",
                guilds: []
            } as Character,
            {
                name: "character4",
                guilds: []
            } as Character
        ];
    }

    static async process(container: Container, isSeed: boolean) {
        const characterRepository = container.get<CharacterRepository>(TYPE.CharacterRepository);
        const characterData: Character[] = CharacterSeed.data() as Character[];
        if (isSeed) {
            await CharacterSeed.startSeed(characterRepository, characterData);
        } else {
            await CharacterSeed.startUnSeed(characterRepository, characterData);
        }
    }

    private static async startSeed(characterRepository: CharacterRepository, characterData: Character[]) {

        for (let i = 0; i < characterData.length; i++) {
            characterData[i].id = i + 1;
            characterData[i].role = SeedHelper.roles[i];
            characterData[i].user = SeedHelper.students[i];
            characterData[i].experience = 1600;
            characterData[i].action_points = 600;
            characterData[i].guilds.push(SeedHelper.guilds[1]);
            SeedHelper.addCharacter(await characterRepository.save(characterData[i]));
        }

    }

    private static async startUnSeed(characterRepository: CharacterRepository, characterData: Character[]) {
        for (let i = 0; i < characterData.length; i++) {
            delete characterData[i].guilds;
            await characterRepository.delete(characterData[i]);
        }
    }
}
