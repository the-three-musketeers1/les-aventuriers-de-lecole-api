import {Container} from "inversify";
import {GuildAdventure} from "../entities/GuildAdventure";
import {TYPE} from "../constants/types";
import {GuildAdventureRepository} from "../repositories/GuildAdventureRepository";
import {SeedHelper} from "./seed.helper";

export class GuildAdventureSeed {
    private static data(): GuildAdventure[] {
        const today = new Date();
        const deadline = new Date(new Date().setDate(today.getDate() + 7));
        return [
            {
                deadline: deadline,
                accessible: true,
            } as GuildAdventure,
            {
                deadline: deadline,
                accessible: true,
            } as GuildAdventure
        ];
    }

    static async process(container: Container, isSeed: boolean) {
        const guildAdventureRepository = container.get<GuildAdventureRepository>(TYPE.GuildAdventureRepository);

        const guildAdventureData: GuildAdventure[] = GuildAdventureSeed.data() as GuildAdventure[];

        if (isSeed) {
            await GuildAdventureSeed.startSeed(guildAdventureRepository, guildAdventureData);
        } else {
            await GuildAdventureSeed.startUnSeed(guildAdventureRepository, guildAdventureData);
        }
    }

    private static async startSeed(guildAdventureRepository: GuildAdventureRepository, guildAdventureData: GuildAdventure[]) {
        for (let i = 0; i < guildAdventureData.length; i++) {
            guildAdventureData[i].id = i + 1;
            guildAdventureData[i].guild = SeedHelper.guilds[i];
            guildAdventureData[i].adventure = SeedHelper.adventures[i];
            guildAdventureData[i].guildId = SeedHelper.guilds[i].id;
            guildAdventureData[i].adventureId = SeedHelper.adventures[i].id;
            SeedHelper.addGuildAdventure(await guildAdventureRepository.save(guildAdventureData[i]));
        }
    }

    private static async startUnSeed(guildAdventureRepository: GuildAdventureRepository, guildAdventureData: GuildAdventure[]) {
        for (const guildAdventure of guildAdventureData) {
            await guildAdventureRepository.delete(guildAdventure);
        }
    }
}
