import {Container} from "inversify";
import {TYPE} from "../constants/types";
import {Role} from "../entities/Role";
import {RoleRepository} from "../repositories/role.repository";
import {SeedHelper} from "./seed.helper";

export class RoleSeed {

    private static data(container: Container): Role[] {
        return [
            {
                name: "Guerrier",
                guild_bonus: "Supprimer la moitié des réponses d'un QCM",
                guild_bonus_cost: 2
            } as Role,
            {
                name: "Soigneur",
                guild_bonus: "Possibilité de refaire la question précédente",
                guild_bonus_cost: 4
            } as Role,
            {
                name: "Mage",
                guild_bonus: "Double la durée de la question actuelle",
                guild_bonus_cost: 2
            } as Role,
            {
                name: "Voleur",
                guild_bonus: "Obtiens un indice par rapport à sa réponse actuelle",
                guild_bonus_cost: 4
            } as Role
        ];
    }

    static async process(container: Container, isSeed: boolean) {
        const roleRepository = container.get<RoleRepository>(TYPE.RoleRepository);
        const roleData: Role[] = RoleSeed.data(container) as Role[];

        if (isSeed) {
            await RoleSeed.startSeed(roleRepository, roleData);
        } else {
            await RoleSeed.startUnSeed(roleRepository, roleData);
        }
    }

    private static async startSeed(roleRepository: RoleRepository, roleData: Role[]) {

        for (let i = 0; i < roleData.length; i++) {
            roleData[i].id = i + 1;
            SeedHelper.addRole(await roleRepository.save(roleData[i]));
        }
    }

    private static async startUnSeed(roleRepository: RoleRepository, roleData: Role[]) {
        for (let i = 0; i < roleData.length; i++) {
            await roleRepository.delete(roleData[i]);
        }
    }
}
