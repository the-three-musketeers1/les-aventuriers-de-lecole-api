import {Container} from "inversify";
import {Ticket} from "../entities/Ticket";
import {TYPE} from "../constants/types";
import {TicketRepository} from "../repositories/TicketRepository";
import {SeedHelper} from "./seed.helper";

export class TicketSeed {
    private static data(): Ticket[] {
        return [
            {
                id: 1,
                title: "L'expédition des racines carrés est trop dure !",
                date: new Date()
            } as Ticket,
            {
                id: 2,
                title: "Est-ce que ça m'intéresse ?",
                date: new Date()
            } as Ticket,
            {
                id: 3,
                title: "La tornade",
                date: new Date()
            } as Ticket
        ];
    }

    static async process(container: Container, isSeed: boolean) {
        const ticketRepository = container.get<TicketRepository>(TYPE.TicketRepository);
        const ticketData: Ticket[] = TicketSeed.data() as Ticket[];

        if (isSeed) {
            await TicketSeed.startSeed(ticketRepository, ticketData);
        } else {
            await TicketSeed.startUnSeed(ticketRepository, ticketData);
        }
    }

    private static async startSeed(ticketRepository: TicketRepository, ticketData: Ticket[]) {
        for(let i = 0; i < ticketData.length; i++) {
            const statusIndex = i === 2 ? 0 : 3;
            ticketData[i].character = SeedHelper.characters[1];
            ticketData[i].status = SeedHelper.status[statusIndex];
            ticketData[i].leader = SeedHelper.leader;
            SeedHelper.addTicket(await ticketRepository.save(ticketData[i]));
        }
    }

    private static async startUnSeed(ticketRepository: TicketRepository, ticketData: Ticket[]) {
        for (const ticket of ticketData) {
            delete ticket.messages;
            await ticketRepository.delete(ticket);
        }
    }
}
