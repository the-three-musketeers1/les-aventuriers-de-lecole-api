import {Container} from "inversify";
import {Question} from "../entities/Question";
import {TYPE} from "../constants/types";
import {QuestionRepository} from "../repositories/QuestionRepository";
import {SeedHelper} from "./seed.helper";

export class QuestionSeed {
    private static data(): Question[] {
        return [
            {
                wording: "En quelle année est mort César ?",
                sequence_number: 1,
                damage: 500,
                time: 40
            } as Question,
            {
                wording: "En quelle année est né César ?",
                sequence_number: 2,
                damage: 500,
                time: 40
            } as Question,
            {
                wording: "En quelle année est mort Napoléon ?",
                sequence_number: 2,
                damage: 400,
                time: 40
            } as Question,
            {
                wording: "En quelle année est né Napoléon ?",
                sequence_number: 1,
                damage: 300,
                time: 40
            } as Question,
            {
                wording: "Racine carré de 9",
                sequence_number: 1,
                damage: 250,
                time: 40
            } as Question,
            {
                wording: "Racine carré de 6 (gl)",
                sequence_number: 2,
                damage: 250,
                time: 40
            } as Question,
            {
                wording: "2 puissance 4",
                sequence_number: 1,
                damage: 200,
                time: 40
            } as Question,
            {
                wording: "3 puissance 5",
                sequence_number: 2,
                damage: 200,
                time: 40
            } as Question
        ];
    }

    static async process(container: Container, isSeed: boolean) {
        const questionRepository = container.get<QuestionRepository>(TYPE.QuestionRepository);
        const questionData: Question[] = QuestionSeed.data() as Question[];

        if (isSeed) {
            await QuestionSeed.startSeed(questionRepository, questionData);
        } else {
            await QuestionSeed.startUnSeed(questionRepository, questionData);
        }
    }

    private static async startSeed(questionRepository: QuestionRepository, questionData: Question[]) {
        for (let i = 0; i < questionData.length; i++) {
            questionData[i].id = i + 1;
            questionData[i].questionType = SeedHelper.questionTypes[(i % 2)];
            if(i < 2) {
                questionData[i].exercise = SeedHelper.exercises[0];
            }
            else if(i < 4) {
                questionData[i].exercise = SeedHelper.exercises[1];
            }
            else if(i < 6) {
                questionData[i].exercise = SeedHelper.exercises[2];
            }
            else if(i < 8) {
                questionData[i].exercise = SeedHelper.exercises[3];
            }
            SeedHelper.addQuestion(await questionRepository.save(questionData[i]));
        }
    }

    private static async startUnSeed(questionRepository: QuestionRepository, questionData: Question[]) {
        for (const question of questionData) {
            delete question.createdAt;
            delete question.updatedAt;
            delete question.answers;
            await questionRepository.delete(question);
        }
    }
}
