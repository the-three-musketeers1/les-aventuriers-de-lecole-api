import {Container} from "inversify";
import {Exercise} from "../entities/Exercise";
import {TYPE} from "../constants/types";
import {ExerciseRepository} from "../repositories/ExerciseRepository";
import {SeedHelper} from "./seed.helper";

export class ExerciseSeed {
    private static data(): Exercise[] {
        return [
            {
                name: "César",
                description: "Battez vous contre César",
                experience: 200,
                life: 1000
            } as Exercise,
            {
                name: "Napoléon",
                description: "Battez vous contre Napoléon",
                experience: 200,
                life: 700
            } as Exercise,
            {
                name: "Racines carrés",
                description: "Racine carrés",
                experience: 200,
                life: 500
            } as Exercise,
            {
                name: "Puissances",
                description: "Puissances",
                experience: 200,
                life: 400
            } as Exercise,

        ];
    }

    static async process(container: Container, isSeed: boolean) {
        const exerciseRepository = container.get<ExerciseRepository>(TYPE.ExerciseRepository);
        const exerciseData: Exercise[] = ExerciseSeed.data() as Exercise[];

        if (isSeed) {
            await ExerciseSeed.startSeed(exerciseRepository, exerciseData);
        } else {
            await ExerciseSeed.startUnSeed(exerciseRepository, exerciseData);
        }
    }

    private static async startSeed(exerciseRepository: ExerciseRepository, exerciseData: Exercise[]) {
        for (let i = 0; i < exerciseData.length; i++) {
            exerciseData[i].id = i + 1;
            exerciseData[i].leader = SeedHelper.leader;
            if(i < 2) {
                exerciseData[i].subject = SeedHelper.chapters[3];
            } else {
                exerciseData[i].subject = SeedHelper.chapters[i - 2];
            }
            SeedHelper.addExercise(await exerciseRepository.save(exerciseData[i]));
        }
    }

    private static async startUnSeed(exerciseRepository: ExerciseRepository, exerciseData: Exercise[]) {
        for (const exercise of exerciseData) {
            delete exercise.createdAt;
            delete exercise.updatedAt;
            delete exercise.questions;
            await exerciseRepository.delete(exercise);
        }
    }
}
