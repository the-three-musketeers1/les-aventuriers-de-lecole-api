import {config} from "dotenv";
import {resolve} from "path";
import app from "../index";
import "process";
import {UserSeed} from "./user.seed";
import {GuildSeed} from "./guild.seed";
import {RoleSeed} from "./role.seed";
import {SubjectUserSeed} from "./subject.user.seed";
import {StatusSeed} from "./status.seed";
import {QuestionTypeSeed} from "./question.type.seed";
import {AdventureSeed} from "./adventure.seed";
import {GuildAdventureSeed} from "./guild.adventure.seed";
import {CharacterSeed} from "./character.seed";
import {ExerciseSeed} from "./exercise.seed";
import {QuestionSeed} from "./question.seed";
import {AnswerSeed} from "./answers.seed";
import {SessionSeed} from "./session.seed";
import {AdventureExerciseSeed} from "./adventure.exercise.seed";
import {TicketSeed} from "./ticket.seed";
import {MessageSeed} from "./message.seed";
import {ChapterSeed} from "./chapter.seed";

config({ path: resolve(__dirname, "../.env")});

app.then(async result => {
    const connection = result.connection;
    const httpServer = result.httpServer;
    const container = result.container;

    async function seedProcess(isSeed: boolean) {
        let seeds = [
            UserSeed,
            SessionSeed,
            GuildSeed,
            RoleSeed,
            SubjectUserSeed,
            ChapterSeed,
            StatusSeed,
            QuestionTypeSeed,
            AdventureSeed,
            GuildAdventureSeed,
            CharacterSeed,
            ExerciseSeed,
            QuestionSeed,
            AnswerSeed,
            AdventureExerciseSeed,
            TicketSeed,
            MessageSeed
        ];

        seeds = (!isSeed) ? seeds.reverse() : seeds;

        for (const seed of seeds) {
            await seed.process(container, isSeed);
        }
    }

    try {
        if (process.argv[2] === "seed" || process.argv[2] === "un-seed") {
            const isSeed = process.argv[2] === "seed";

            await seedProcess(isSeed)
        }
    } catch (e) {
        console.error(e.message);
    } finally {
        await connection.close();
        await httpServer.close();
    }
});
