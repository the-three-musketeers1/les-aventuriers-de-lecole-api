import {Subject} from "../entities/Subject";
import {Container} from "inversify";
import {SubjectRepository} from "../repositories/SubjectRepository";
import {TYPE} from "../constants/types";
import {SeedHelper} from "./seed.helper";

export class ChapterSeed {
    private static data(): Subject[] {
        return [
            {
                title: "Arithmétique",
                description: "Pour apprendre des choses rationnelles"
            } as Subject,
            {
                title: "Algèbre",
                description: "Plus chaud que son confrère arthimétique"
            } as Subject,
            {
                title: "Matrice",
                description: "Ce chapitre cache un grand pouvoir dans certaines croyances."
            } as Subject,
            {
                title: "Les temps modernes",
                description: "Pas trop d'inspi là-dessus"
            } as Subject,
            {
                title: "L'antiquité",
                description: "C'était ya looongtemps"
            } as Subject,
        ];
    }

    static async process(container: Container, isSeed: boolean) {
        const subjectRepository = container.get<SubjectRepository>(TYPE.SubjectRepository);
        const chapterData: Subject[] = ChapterSeed.data();

        if (isSeed) {
            await ChapterSeed.startSeed(subjectRepository, chapterData);
        } else {
            await ChapterSeed.startUnSeed(subjectRepository, chapterData);
        }
    }

    private static async startSeed(subjectRepository: SubjectRepository, chapterData: Subject[]) {
        const subjects = SeedHelper.subjects;
        const normallyMath = subjects[0];
        for (let i = 0; i < chapterData.length; i++) {
            chapterData[i].id = subjects.length + i + 1;
            chapterData[i].leaders = [normallyMath.leaders[0]];
            chapterData[i].parentSubject = i < 3 ? normallyMath : SeedHelper.subjects[1];
            SeedHelper.addChapter(await subjectRepository.save(chapterData[i]));
        }
    }

    private static async startUnSeed(subjectRepository: SubjectRepository, chapterData: Subject[]) {
        for (let i = 0; i < chapterData.length; i++) {
            await subjectRepository.delete(chapterData[i]);
        }
    }
}
