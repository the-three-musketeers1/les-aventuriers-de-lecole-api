import {Subject} from "../entities/Subject";
import {Container} from "inversify";
import {SubjectRepository} from "../repositories/SubjectRepository";
import {TYPE} from "../constants/types";
import {SeedHelper} from "./seed.helper";

export class SubjectUserSeed {
    private static data():  Subject[] {
        return [
            {
                title: "Mathématique",
                description: "C'est une matière très importante, pour avoir des connaissances concrètes."
            } as Subject,
            {
                title: "Histoire",
                description: "C'est une matière très importante, pour avoir des connaissances."
            } as Subject,
            {
                title: "Philosophie",
                description: "Pour se questionner tout simplement"
            } as Subject,
            {
                title: "Japonais",
                description: "Une langue complexe mais symboliquement belle"
            } as Subject,
        ]
    }

    static async process(container: Container, isSeed: boolean) {
        const subjectRepository = container.get<SubjectRepository>(TYPE.SubjectRepository);
        const subjectData: Subject[] = SubjectUserSeed.data();
        if (isSeed) {
            await SubjectUserSeed.startSeed(subjectRepository, subjectData);
        } else {
            await SubjectUserSeed.startUnSeed(subjectRepository, subjectData);
        }
    }

    private static async startSeed(subjectRepository: SubjectRepository, subjectData: Subject[]) {
        for (let i = 0; i < subjectData.length; i++) {
            subjectData[i].id = i + 1;
            subjectData[i].leaders = [];
            subjectData[i].leaders.push(SeedHelper.leader);
            SeedHelper.addSubject(await subjectRepository.save(subjectData[i]));
        }
    }

    private static async startUnSeed(subjectRepository: SubjectRepository, subjectData: Subject[]) {
        for (let i = 0; i < subjectData.length; i++) {
            await subjectRepository.delete(subjectData[i]);
        }
    }
}
