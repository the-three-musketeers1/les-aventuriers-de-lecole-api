import {Container} from "inversify";
import {TYPE} from "../constants/types";
import {SeedHelper} from "./seed.helper";
import {AdventureExerciseRepository} from "../repositories/AdventureExerciseRepository";
import {AdventureExercise} from "../entities/AdventureExercise";

export class AdventureExerciseSeed {



    static async process(container: Container, isSeed: boolean) {
        const adventureExerciseRepository = container.get<AdventureExerciseRepository>(TYPE.AdventureExerciseRepository);

        if (isSeed) {
            await AdventureExerciseSeed.startSeed(adventureExerciseRepository);
        } else {
            await AdventureExerciseSeed.startUnSeed(adventureExerciseRepository);
        }
    }

    private static async startSeed(adventureExerciseRepository: AdventureExerciseRepository) {

        for (let i = 0; i < 4; i++) {
            let adventureExercise = new AdventureExercise();
            adventureExercise.id = i + 1;
            adventureExercise.exercise = SeedHelper.exercises[i];
            adventureExercise.exerciseId = SeedHelper.exercises[i].id;
            if (i < 2) {
                adventureExercise.adventure = SeedHelper.adventures[0];
                adventureExercise.adventureId = SeedHelper.adventures[0].id;
            } else {
                adventureExercise.adventure = SeedHelper.adventures[1];
                adventureExercise.adventureId = SeedHelper.adventures[1].id;
            }
            SeedHelper.addAdventureExercise(await adventureExerciseRepository.save(adventureExercise));
        }
    }

    private static async startUnSeed(adventureExerciseRepository: AdventureExerciseRepository) {
        for (let i = 0; i < 4; i++) {
            await adventureExerciseRepository.delete({id: i});
        }
    }
}
