import {Container} from "inversify";
import {TYPE} from "../constants/types";
import {QuestionType} from "../entities/QuestionType";
import {QuestionTypeRepository} from "../repositories/QuestionTypeRepository";
import {SeedHelper} from "./seed.helper";

export class QuestionTypeSeed {

    private static data(container: Container): QuestionType[] {
        return [
            {
                id: 1,
                name: "qcm"
            } as QuestionType,
            {
                id: 2,
                name: "input"
            } as QuestionType
        ];
    }

    static async process(container: Container, isSeed: boolean) {
        const questionTypeRepository = container.get<QuestionTypeRepository>(TYPE.QuestionTypeRepository);
        const questionTypeData: QuestionType[] = QuestionTypeSeed.data(container) as QuestionType[];

        if (isSeed) {
            await QuestionTypeSeed.startSeed(questionTypeRepository, questionTypeData);
        } else {
            await QuestionTypeSeed.startUnSeed(questionTypeRepository, questionTypeData);
        }
    }

    private static async startSeed(questionTypeRepository: QuestionTypeRepository, questionTypeData: QuestionType[]) {

        for (let i = 0; i < questionTypeData.length; i++) {
            SeedHelper.addQuestionType(await questionTypeRepository.save(questionTypeData[i]));
        }
    }

    private static async startUnSeed(questionTypeRepository: QuestionTypeRepository, questionTypeData: QuestionType[]) {
        for (let i = 0; i < questionTypeData.length; i++) {
            await questionTypeRepository.delete(questionTypeData[i]);
        }
    }
}
