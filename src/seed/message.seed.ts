import {Container} from "inversify";
import {Message} from "../entities/Message";
import {TYPE} from "../constants/types";
import {MessageRepository} from "../repositories/MessageRepository";
import {SeedHelper} from "./seed.helper";

export class MessageSeed {
    private static data(): Message[] {
        return [
            {
                id: 1,
                content: "Comment voulez-vous qu'on calcule la racine carré de 6 en moins de 3 secondes ? vous êtes sadique en fait ?",
                sentByCharacter: true
            } as Message,
            {
                id: 2,
                content: "Chut",
                sentByCharacter: false
            } as Message,
            {
                id: 3,
                content: "Ce chapitre n'est pas intéressant, mieux vaut regarder devant soit que dans le passé",
                sentByCharacter: true
            } as Message,
            {
                id: 4,
                content: "C'est vrai, je vais déposer ma démission dès demain",
                sentByCharacter: false
            } as Message,
        ];
    }

    static async process(container: Container, isSeed: boolean) {
        const messageRepository = container.get<MessageRepository>(TYPE.MessageRepository);
        const messageData: Message[] = MessageSeed.data() as Message[];

        if (isSeed) {
            await MessageSeed.startSeed(messageRepository, messageData);
        } else {
            await MessageSeed.startUnSeed(messageRepository, messageData);
        }
    }

    private static async startSeed(messageRepository: MessageRepository, messageData: Message[]) {
        for(let i = 0; i < messageData.length; i++) {
            const ticketIndex = i < 2 ? 0 : 1;
            messageData[i].ticket = SeedHelper.tickets[ticketIndex];
            SeedHelper.addMessage(await messageRepository.save(messageData[i]));
        }
    }

    private static async startUnSeed(messageRepository: MessageRepository, messageData: Message[]) {
        for (const message of messageData) {
            await messageRepository.delete(message);
        }
    }
}
