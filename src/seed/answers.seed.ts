import {Container} from "inversify";
import {Answer} from "../entities/Answer";
import {TYPE} from "../constants/types";
import {AnswerRepository} from "../repositories/AnswerRepository";
import {SeedHelper} from "./seed.helper";

export class AnswerSeed {
    private static data(): Answer[] {
        return [
            {
                content: "-200 avant JC",
                correct: false,
                question: SeedHelper.questions[0]
            } as Answer,
            {
                content: "1200",
                correct: false,
                question: SeedHelper.questions[0]
            } as Answer,
            {
                content: "2018",
                correct: false,
                question: SeedHelper.questions[0]
            } as Answer,
            {
                content: "-44 avant JC",
                correct: true,
                question: SeedHelper.questions[0]
            } as Answer,
            {
                content: "-100",
                correct: true,
                question: SeedHelper.questions[1]
            } as Answer,
            {
                content: "-102",
                correct: true,
                question: SeedHelper.questions[1]
            } as Answer,
            {
                content: "-200 avant JC",
                correct: false,
                question: SeedHelper.questions[2]
            } as Answer,
            {
                content: "1200",
                correct: false,
                question: SeedHelper.questions[2]
            } as Answer,
            {
                content: "1821",
                correct: true,
                question: SeedHelper.questions[2]
            } as Answer,
            {
                content: "2016",
                correct: false,
                question: SeedHelper.questions[2]
            } as Answer,
            {
                content: "1769",
                correct: true,
                question: SeedHelper.questions[3]
            } as Answer,
            {
                content: "3",
                correct: true,
                question: SeedHelper.questions[4]
            } as Answer,
            {
                content: "81",
                correct: false,
                question: SeedHelper.questions[4]
            } as Answer,
            {
                content: "2,4",
                correct: true,
                question: SeedHelper.questions[5]
            } as Answer,
            {
                content: "32/2",
                correct: true,
                question: SeedHelper.questions[6]
            } as Answer,
            {
                content: "16",
                correct: true,
                question: SeedHelper.questions[6]
            } as Answer,
            {
                content: "4",
                correct: false,
                question: SeedHelper.questions[6]
            } as Answer,
            {
                content: "8",
                correct: false,
                question: SeedHelper.questions[6]
            } as Answer,
            {
                content: "243",
                correct: true,
                question: SeedHelper.questions[7]
            } as Answer
        ];
    }

    static async process(container: Container, isSeed: boolean) {
        const answerRepository = container.get<AnswerRepository>(TYPE.AnswerRepository);
        const answerData: Answer[] = AnswerSeed.data() as Answer[];

        if (isSeed) {
            await AnswerSeed.startSeed(answerRepository, answerData);
        } else {
            await AnswerSeed.startUnSeed(answerRepository, answerData);
        }
    }

    private static async startSeed(answerRepository: AnswerRepository, answerData: Answer[]) {
        for(let i = 0; i < answerData.length; i++) {
            answerData[i].id = i + 1;
            SeedHelper.addAnswer(await answerRepository.save(answerData[i]));
        }
    }

    private static async startUnSeed(answerRepository: AnswerRepository, answerData: Answer[]) {
        for (const answer of answerData) {
            await answerRepository.delete(answer);
        }
    }
}
