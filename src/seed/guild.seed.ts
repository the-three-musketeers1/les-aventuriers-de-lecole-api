import {Container} from "inversify";
import {Guild} from "../entities/Guild";
import {TYPE} from "../constants/types";
import {GuildRepository} from "../repositories/GuildRepository";
import {SeedHelper} from "./seed.helper";

export class GuildSeed {
    private static data(): Guild[] {
        const today = new Date();
        const yesterday = new Date(new Date().setDate(today.getDate() - 1));
        const afterLongTime = new Date(new Date().setDate(today.getDay() + 360));
        return [
            {
                name: "guild1",
                token: "tokenGuild1",
            } as Guild,
            {
                name: "guild2",
                token: "tokenGuild2"
            } as Guild,
            {
                name: "guild3",
                token: "tokenGuild3",
                deadlineAddCharacter: today
            } as Guild,
            {
                name: "guildDeadlineExceed",
                token: "tokenGuildDeadlineExceed",
                deadlineAddCharacter: yesterday
            } as Guild,
            {
                name: "guildDeadlineAvailable",
                token: "tokenGuildDeadlineAvailable",
                deadlineAddCharacter: afterLongTime
            } as Guild
        ];
    }

    static async process(container: Container, isSeed: boolean) {
        const guildRepository = container.get<GuildRepository>(TYPE.GuildRepository);
        const guildData: Guild[] = GuildSeed.data() as Guild[];

        if (isSeed) {
            await GuildSeed.startSeed(guildRepository, guildData);
        } else {
            await GuildSeed.startUnSeed(guildRepository, guildData);
        }
    }

    private static async startSeed(guildRepository: GuildRepository, guildData: Guild[]) {

        for (let i = 0; i < guildData.length; i++) {
            guildData[i].leader = SeedHelper.leader;
            guildData[i].id = i + 1;
            SeedHelper.addGuild(await guildRepository.save(guildData[i]));
        }

    }

    private static async startUnSeed(guildRepository: GuildRepository, guildData: Guild[]) {
        for (let i = 0; i < guildData.length; i++) {
            delete guildData[i].deadlineAddCharacter;
            delete guildData[i].createdDate;
            delete guildData[i].updatedDate;
            await guildRepository.delete(guildData[i]);
        }
    }
}
