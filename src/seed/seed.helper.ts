import {User} from "../entities/User";
import {Guild} from "../entities/Guild";
import {Adventure} from "../entities/Adventure";
import {Role} from "../entities/Role";
import {Subject} from "../entities/Subject";
import {Status} from "../entities/Status";
import {QuestionType} from "../entities/QuestionType";
import {GuildAdventure} from "../entities/GuildAdventure";
import {Character} from "../entities/Character";
import {Exercise} from "../entities/Exercise";
import {Question} from "../entities/Question";
import {Answer} from "../entities/Answer";
import {AdventureExercise} from "../entities/AdventureExercise";
import {Session} from "../entities/Session";
import {Ticket} from "../entities/Ticket";
import {Message} from "../entities/Message";

export class SeedHelper {

    private static _guilds: Guild[] = [];
    private static _tickets: Ticket[] = [];
    private static _messages: Message[] = [];
    private static _characters: Character[] = [];
    private static _exercises: Exercise[] = [];
    private static _questions: Question[] = [];
    private static _answers: Answer[] = [];
    private static _users: User[] = [];
    private static _adventures: Adventure[] = [];
    private static _roles: Role[] = [];
    private static _subjects: Subject[] = [];
    private static _status: Status[] = [];
    private static _questionTypes: QuestionType[] = [];
    private static _guildAdventures: GuildAdventure[] = [];
    private static _adventureExercises: AdventureExercise[] = [];
    private static _sessions: Session[] = [];
    private static _chapters: Subject[] = [];

    static get leader(): User {
        for (let user of this._users) {
            if(user.is_teacher) {
                return user;
            }
        }
        return null;
    }

    static get students(): User[] {
        const students: User[] = [];
        for (let user of this._users) {
            if(!user.is_teacher) {
                students.push(user);
            }
        }
        return students;
    }

    static get status(): Status[] {
        return this._status;
    }

    static get characters(): Character[] {
        return this._characters;
    }

    static get exercises(): Exercise[] {
        return this._exercises;
    }

    static get questionTypes(): QuestionType[] {
        return this._questionTypes;
    }

    static get tickets(): Ticket[] {
        return this._tickets;
    }

    static get questions(): Question[] {
        return this._questions;
    }

    static get subjects(): Subject[] {
        return this._subjects;
    }

    static get chapters(): Subject[] {
        return this._chapters;
    }

    static get roles(): Role[] {
        return this._roles;
    }

    static get adventures(): Adventure[] {
        return this._adventures;
    }

    static get guilds(): Guild[] {
        return this._guilds;
    }

    static get sessions(): Session[] {
        return this._sessions;
    }

    static addTicket(ticket: Ticket) {
        this._tickets.push(ticket);
    }

    static addMessage(message: Message) {
        this._messages.push(message);
    }

    static addAnswer(answer: Answer) {
        this._answers.push(answer);
    }

    static addQuestion(question: Question) {
        this._questions.push(question);
    }

    static addExercise(exercise: Exercise) {
        this._exercises.push(exercise);
    }

    static addCharacter(character: Character) {
        this._characters.push(character);
    }

    static addGuildAdventure(guildAdventure: GuildAdventure) {
        this._guildAdventures.push(guildAdventure);
    }
    static addAdventureExercise(adventureExercise: AdventureExercise) {
        this._adventureExercises.push(adventureExercise);
    }

    static addQuestionType(questionType: QuestionType) {
        this._questionTypes.push(questionType);
    }

    static addStatus(status: Status) {
        this._status.push(status);
    }

    static addSubject(subject: Subject) {
        this._subjects.push(subject);
    }

    static addRole(role: Role) {
        this._roles.push(role);
    }

    static addAdventure(adventure: Adventure) {
        this._adventures.push(adventure);
    }

    static addGuild(guild: Guild) {
        this._guilds.push(guild);
    }

    static addUser(user: User) {
        this._users.push(user);
    }

    static addSession(session: Session) {
        this._sessions.push(session);
    }

    static addChapter(chapter: Subject) {
        this._chapters.push(chapter);
    }
}
