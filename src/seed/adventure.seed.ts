import {Container} from "inversify";
import {Adventure} from "../entities/Adventure";
import {TYPE} from "../constants/types";
import {AdventureRepository} from "../repositories/AdventureRepository";
import {SeedHelper} from "./seed.helper";

export class AdventureSeed {
    private static data(): Adventure[] {
        return [
            {
                id: 1,
                title: "L'expédition du moyen âge",
                description: "Découvrez l'époque du moyen âge et ses évèneemnts majeurs !",
                isExpedition: true,
                experienceBonus: 500
            } as Adventure,
            {
                id: 2,
                title: "Les puissances",
                description: "Apprenez les puissances, racines, etc..",
                isExpedition: true,
                experienceBonus: 400
            } as Adventure,

        ];
    }

    static async process(container: Container, isSeed: boolean) {
        const adventureRepository = container.get<AdventureRepository>(TYPE.AdventureRepository);
        const adventureData: Adventure[] = AdventureSeed.data() as Adventure[];

        if (isSeed) {
            await AdventureSeed.startSeed(adventureRepository, adventureData);
        } else {
            await AdventureSeed.startUnSeed(adventureRepository, adventureData);
        }
    }

    private static async startSeed(adventureRepository: AdventureRepository, adventureData: Adventure[]) {
        for (const adventure of adventureData) {
            adventure.leader = SeedHelper.leader;
            SeedHelper.addAdventure(await adventureRepository.save(adventure));
        }
    }

    private static async startUnSeed(adventureRepository: AdventureRepository, adventureData: Adventure[]) {
        for (const adventure of adventureData) {
            delete adventure.createdAt;
            delete adventure.updatedAt;
            await adventureRepository.delete(adventure);
        }
    }
}
