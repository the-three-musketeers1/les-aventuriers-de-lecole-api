import {Container} from "inversify";
import {UserRepository} from "../repositories/user.repository";
import {SecurityUtil} from "../utils/security.util";
import {TYPE} from "../constants/types";
import {User} from "../entities/User";
import {SeedHelper} from "./seed.helper";

export class UserSeed {

    private static data(container: Container): User[] {
        const securityUtil = container.get<SecurityUtil>(TYPE.SecurityUtil);

        return [
            {
                email: "ternisien99@gmail.com",
                firstname: "Jerem",
                lastname: "Ter",
                password: securityUtil.hashPassword("terter"),
                is_teacher: true
            } as User,
            {
                firstname: "James",
                lastname: "Bertho",
                email: "jamesbertho@gmail.com",
                password: securityUtil.hashPassword("skyzz"),
                is_teacher: true
            } as User,
            {
                firstname: "Masa",
                lastname: "Ishii",
                email: "masaradio78@gmail.com",
                password: securityUtil.hashPassword("masa"),
                is_teacher: true
            } as User,
            {
                firstname: "deshi",
                lastname: "deshi",
                email: "deshi@gmail.com",
                password: securityUtil.hashPassword("deshi"),
                is_teacher: false
            } as User,
            {
                firstname: "Marie",
                lastname: "Dupont",
                email: "mama@gmail.com",
                password: securityUtil.hashPassword("mama"),
                is_teacher: false
            } as User,
            {
                firstname: "François",
                lastname: "Hollande",
                email: "francois@gmail.com",
                password: securityUtil.hashPassword("francois"),
                is_teacher: false
            } as User,
            {
                firstname: "Nicolas",
                lastname: "Hulot",
                email: "nico@gmail.com",
                password: securityUtil.hashPassword("nico"),
                is_teacher: false
            } as User,
            {
                firstname: "onizuka",
                lastname: "teacher",
                email: "onizuka@aol.fr",
                password: securityUtil.hashPassword("onizukaPassword"),
                is_teacher: true
            } as User
        ];
    }

    static async process(container: Container, isSeed: boolean) {
        const userRepository = container.get<UserRepository>(TYPE.UserRepository);
        const userData: User[] = UserSeed.data(container) as User[];

        if (isSeed) {
            await UserSeed.startSeed(userRepository, userData);
        } else {
            await UserSeed.startUnSeed(userRepository, userData);
        }
    }

    private static async startSeed(userRepository: UserRepository, userData: User[]) {

        for (let i = 0; i < userData.length; i++) {
            userData[i].id = i + 1;
            SeedHelper.addUser(await userRepository.save(userData[i]));
        }

    }

    private static async startUnSeed(userRepository: UserRepository, userData: User[]) {
        for (let i = 0; i < userData.length; i++) {
            await userRepository.delete(userData[i]);
        }
    }
}
