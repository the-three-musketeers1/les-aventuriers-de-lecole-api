import {Container} from "inversify";
import {SessionRepository} from "../repositories/session.repository";
import {TYPE} from "../constants/types";
import {Session} from "../entities/Session";
import {SeedHelper} from "./seed.helper";

export class SessionSeed {

    private static data(): Session[] {
        return [
            {
                token: JSON.stringify({hash:"le_token_de_mama", userId: 5})
            } as Session,
        ]
    }

    static async process(container: Container, isSeed: boolean) {
        const sessionRepository = container.get<SessionRepository>(TYPE.SessionRepository);
        const sessionData: Session[] = SessionSeed.data();

        if (isSeed) {
            await SessionSeed.startSeed(sessionRepository, sessionData);
        } else {
            await SessionSeed.startUnSeed(sessionRepository, sessionData);
        }
    }

    private static async startSeed(sessionRepository: SessionRepository, sessionData: Session[]) {
        sessionData[0].id = 1;
        sessionData[0].user = SeedHelper.students.find(student => student.email === "mama@gmail.com");
        SeedHelper.addSession(await sessionRepository.save(sessionData[0]));
    }

    private static async startUnSeed(sessionRepository: SessionRepository, subjectData: Session[]) {
        for (let i = 0; i < subjectData.length; i++) {
            await sessionRepository.delete(subjectData[i]);
        }
    }
}
