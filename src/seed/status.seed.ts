import {Container} from "inversify";
import {TYPE} from "../constants/types";
import {Status} from "../entities/Status";
import {StatusRepository} from "../repositories/StatusRepository";
import {SeedHelper} from "./seed.helper";

export class StatusSeed {

    private static data(container: Container): Status[] {
        return [
            {
                id: 1,
                state: "Envoyé"
            } as Status,
            {
                id: 2,
                state: "Vu"
            } as Status,
            {
                id: 3,
                state: "En cours de traitement ..."
            } as Status,
            {
                id: 4,
                state: "Clôt"
            } as Status
        ];
    }

    static async process(container: Container, isSeed: boolean) {
        const statusRepository = container.get<StatusRepository>(TYPE.StatusRepository);
        const statusData: Status[] = StatusSeed.data(container) as Status[];

        if (isSeed) {
            await StatusSeed.startSeed(statusRepository, statusData);
        } else {
            await StatusSeed.startUnSeed(statusRepository, statusData);
        }
    }

    private static async startSeed(statusRepository: StatusRepository, statusData: Status[]) {

        for (let i = 0; i < statusData.length; i++) {
            SeedHelper.addStatus(await statusRepository.save(statusData[i]));
        }
    }

    private static async startUnSeed(statusRepository: StatusRepository, statusData: Status[]) {
        for (let i = 0; i < statusData.length; i++) {
            await statusRepository.delete(statusData[i]);
        }
    }
}
