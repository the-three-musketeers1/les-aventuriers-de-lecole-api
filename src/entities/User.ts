import {Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Character} from "./Character";
import {Ticket} from "./Ticket";
import {IsBoolean, IsEmail, IsNotEmpty, MaxLength} from "class-validator";
import {Session} from "./Session";
import {Guild} from "./Guild";
import {Exercise} from "./Exercise";
import {Adventure} from "./Adventure";
import {Subject} from "./Subject";

@Entity()
export class User {

    constructor(id?: number, email?: string, firstname?: string, lastname?: string, password?: string) {

        this.id = id;
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = password;
    }

    @PrimaryGeneratedColumn()
    id: number;

    @Column({unique:true})
    @IsEmail()
    @MaxLength(250)
    email: string;

    @Column()
    @IsNotEmpty()
    @MaxLength(250)
    firstname: string;

    @Column()
    @IsNotEmpty()
    @MaxLength(250)
    lastname: string;

    @Column({select: false})
    @IsNotEmpty()
    @MaxLength(250)
    password: string;

    @Column()
    @IsNotEmpty()
    @IsBoolean()
    is_teacher: boolean;

    @OneToMany(type => Character, character => character.user)
    characters: Character[];

    @OneToMany(type => Ticket, message => message.leader)
    tickets: Ticket[];

    @OneToMany(type => Session, session => session.user)
    sessions: Session[];

    @OneToMany(type => Guild, guild => guild.leader)
    guilds: Guild[];

    @OneToMany(type => Exercise, exercise => exercise.leader)
    exercises: Exercise[];

    @OneToMany(type => Adventure, adventure => adventure.leader)
    adventures: Adventure[];

    @ManyToMany(type => Subject)
    @JoinTable({name: "leader_subject"})
    subjects: Subject[];
}
