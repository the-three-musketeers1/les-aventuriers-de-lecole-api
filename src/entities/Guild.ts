import {
    Column,
    CreateDateColumn,
    Entity,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn, UpdateDateColumn
} from "typeorm";
import {Character} from "./Character";
import {GuildAdventure} from "./GuildAdventure";
import {Award} from "./Award";
import {User} from "./User";
import {Length} from "class-validator";

@Entity()
export class Guild {
    @PrimaryGeneratedColumn()
    id: number;

    @Length(1, 255)
    @Column()
    name: string;

    @Column({default: () => "''"})
    token: string;

    @Column({nullable: true})
    deadlineAddCharacter: Date;

    @ManyToMany(type => Character)
    @JoinTable({name: "guild_character"})
    characters: Character[];

    @ManyToOne(type => Guild, guild => guild.childGuilds)
    parentGuild: Guild;

    @OneToMany(type => Guild, guild => guild.parentGuild)
    childGuilds: Guild[];

    @ManyToOne(type => User, leader => leader.guilds)
    leader: User;

    @OneToMany(type => GuildAdventure, guildAdventure => guildAdventure.guild, {onDelete: 'CASCADE'})
    guildAdventures: GuildAdventure[];

    @OneToMany(type => Award, award => award.guild, {onDelete: 'CASCADE'})
    awards: Award[];

    @CreateDateColumn({type: "timestamp"})
    createdDate: Date;

    @UpdateDateColumn({type: "timestamp"})
    updatedDate: Date;
}
