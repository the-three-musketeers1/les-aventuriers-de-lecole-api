import {Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Character} from "./Character";
import {Exercise} from "./Exercise";
import {CharacterQuestion} from "./CharacterQuestion";
import {CharacterAdventure} from "./CharacterAdventure";
import {Adventure} from "./Adventure";
import {AdventureExercise} from "./AdventureExercise";

@Entity()
export class CharacterExercise {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    characterId: number;

    @Column()
    adventureExerciseId: number;

    @CreateDateColumn({type: "timestamp"})
    createdAt: Date;

    @ManyToOne(type => Character, character => character.characterExercise)
    character: Character;

    @ManyToOne(type => AdventureExercise, adventureExercise => adventureExercise.characterExercises, { onDelete: 'CASCADE' })
    adventureExercise: AdventureExercise;

    @OneToMany(type => CharacterQuestion, characterQuestions => characterQuestions.characterExercise)
    characterQuestions: CharacterQuestion[]
}
