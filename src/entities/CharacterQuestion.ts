import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Character} from "./Character";
import {Question} from "./Question";
import {Answer} from "./Answer";
import {CharacterExercise} from "./CharacterExercise";


@Entity()
export class CharacterQuestion {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    characterExerciseId: number;

    @Column()
    questionId: number;

    @Column({nullable : true})
    answerId: number;

    @Column()
    bonus_consumed: boolean;

    @ManyToOne(type => CharacterExercise, characterExercise => characterExercise.characterQuestions)
    characterExercise: CharacterExercise;

    @ManyToOne(type => Question, question => question.characterQuestions)
    question: Question;

    @ManyToOne(type => Answer, answer => answer.characterQuestions)
    answer: Answer;
}
