import {Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Exercise} from "./Exercise";
import {Adventure} from "./Adventure";
import {CharacterExercise} from "./CharacterExercise";
import {IsInt, Min} from "class-validator";

@Entity()
export class AdventureExercise {


    constructor(id?: number, ratio_to_damage?: number, exerciseId?: number, adventureId?: number, adventure?: Adventure, exercise?: Exercise, characterExercises?: CharacterExercise[]) {
        this.id = id;
        this.exerciseId = exerciseId;
        this.adventureId = adventureId;
        this.adventure = adventure;
        this.exercise = exercise;
        this.characterExercises = characterExercises;
    }

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsInt()
    @Min(1)
    exerciseId: number;

    @Column()
    @IsInt()
    @Min(1)
    adventureId: number;

    @ManyToOne(type => Adventure, adventure => adventure.adventureExercises, {onDelete: "CASCADE"})
    adventure: Adventure;

    @ManyToOne(type => Exercise, exercise => exercise.adventureExercises)
    exercise: Exercise;

    @OneToMany(type => CharacterExercise, characterExercises => characterExercises.adventureExercise, { onDelete: 'CASCADE' })
    characterExercises: CharacterExercise[]
}
