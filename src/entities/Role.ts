import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Character} from "./Character";

@Entity()
export class Role {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({default: ''})
    guild_bonus: string;

    @Column({default: 0})
    guild_bonus_cost: number;

    @Column({default: ''})
    board_bonus: string;
a
    @Column({default: 0})
    board_bonus_cost: number;

    @OneToMany(type => Character, character => character.role)
    characters: Character[];
}
