import {
    Column,
    Entity,
    OneToMany,
    PrimaryGeneratedColumn
} from "typeorm";
import {IsNotEmpty} from "class-validator";
import {Ticket} from "./Ticket";

@Entity()
export class Status {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsNotEmpty()
    state: string;

    @OneToMany(type => Ticket, ticket => ticket.status)
    tickets: Ticket[];
}
