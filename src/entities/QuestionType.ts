import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Question} from "./Question";
import {IsNotEmpty, IsString, MaxLength} from "class-validator";

@Entity()
export class QuestionType {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsNotEmpty()
    @IsString()
    @MaxLength(250)
    name: string;

    @OneToMany(type => Question, question => question.questionType, { onDelete: 'CASCADE' })
    questions: Question[];
}
