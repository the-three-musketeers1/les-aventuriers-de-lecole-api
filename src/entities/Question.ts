import {
    Column,
    CreateDateColumn,
    Entity,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import {Answer} from "./Answer";
import {Exercise} from "./Exercise";
import {QuestionType} from "./QuestionType";
import {CharacterQuestion} from "./CharacterQuestion";
import {CharacterAdventure} from "./CharacterAdventure";
import {IsInt, IsNotEmpty, IsPositive, IsString, MaxLength} from "class-validator";
import {Type} from "class-transformer";


@Entity()
export class Question {


    constructor(id?: number, wording?: string, sequence_number?: number, damage?: number,
                time?: number, exercise?: Exercise, questionType?: QuestionType) {
        this.id = id;
        this.wording = wording;
        this.sequence_number = sequence_number;
        this.damage = damage;
        this.time = time;
        this.exercise = exercise;
        this.questionType = questionType;
    }

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsNotEmpty()
    @IsString()
    @MaxLength(250)
    wording: string;

    @Column()
    @IsNotEmpty()
    @IsInt()
    @IsPositive()
    sequence_number: number;

    @Column()
    @IsInt()
    @IsPositive()
    damage: number;

    @Column()
    @IsNotEmpty()
    @IsInt()
    @IsPositive()
    time: number;

    @CreateDateColumn({type: "timestamp"})
    createdAt: Date;

    @UpdateDateColumn({type: "timestamp"})
    updatedAt: Date;

    @OneToMany(type => Answer, answer => answer.question, { onDelete: 'CASCADE' })
    answers: Answer[];

    @ManyToOne(type => Exercise, exercise => exercise.questions, {onDelete: 'CASCADE'})
    @IsNotEmpty()
    @Type(() => Exercise)
    exercise: Exercise;

    @ManyToOne(type => QuestionType, type => type.questions)
    @IsNotEmpty()
    @Type(() => QuestionType)
    questionType: QuestionType;

    @OneToMany(type => CharacterQuestion, characterQuestion => characterQuestion.question)
    characterQuestions: CharacterQuestion[];

    @OneToMany(type => CharacterAdventure, characterAdventure => characterAdventure.question)
    characterAdventures: CharacterAdventure[];
}
