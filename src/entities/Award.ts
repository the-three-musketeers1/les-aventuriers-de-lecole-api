import {Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {CharacterAward} from "./CharacterAward";
import {Guild} from "./Guild";


@Entity()
export class Award {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    unlock_level: number;

    @Column()
    cost: number;

    @OneToMany(type => CharacterAward, characterAward => characterAward.award)
    characterAwards: CharacterAward[];

    @ManyToOne(type => Guild, guild => guild.awards)
    guild: Guild;
}
