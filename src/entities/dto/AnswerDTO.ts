export class AnswerDTO {
    id: number;
    content: string;
    correct: boolean;
    questionId: number;
    bonusConsumed: boolean;

    constructor(id?: number, content?: string, correct?: boolean, questionId?: number, bonusConsumed?:boolean) {
        this.id = id;
        this.content = content;
        this.correct = correct;
        this.questionId = questionId;
        this.bonusConsumed = bonusConsumed;
    }
}
