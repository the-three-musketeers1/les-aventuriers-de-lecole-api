export class ExpeditionInformationsDTO {
    id: number;
    title: string;
    description: string;
    deadline: Date;
    experienceBonus: number;
    accessible: boolean;
    disciplines: string[];
    exercisesNumber: number;
    done: boolean;
}
