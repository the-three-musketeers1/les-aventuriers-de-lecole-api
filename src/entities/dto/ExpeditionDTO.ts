export class expeditionDTO {
    id: number;
    title: string;
    description: string;
    life: number;
    mana: number;
    winLevel: number;
    deadline: number;
    experienceBonus: number;
    accessible: boolean;
    current_life: number;
    current_mana: number;


    constructor(id: number, title: string, description: string, life: number, mana: number, winLevel: number, deadline: number, experienceBonus: number, accessible: boolean, current_life: number, current_mana: number) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.life = life;
        this.mana = mana;
        this.winLevel = winLevel;
        this.deadline = deadline;
        this.experienceBonus = experienceBonus;
        this.accessible = accessible;
        this.current_life = current_life;
        this.current_mana = current_mana;
    }
}
