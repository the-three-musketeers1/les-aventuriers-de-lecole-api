    export class ExerciseInformationDTO {
    id: number;
    life: number;
    name: string;
    chapter: string;
    discipline: string;

    constructor(id?: number, life?: number, name?: string, chapter?: string, discipline?: string) {
        this.id = id;
        this.life = life;
        this.name = name;
        this.chapter = chapter;
        this.discipline = discipline;
    }
}
