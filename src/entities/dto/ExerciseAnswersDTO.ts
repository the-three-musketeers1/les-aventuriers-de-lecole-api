import {AnswerDTO} from "./AnswerDTO";

export class ExerciseAnswersDTO {
    exerciseId: number;
    adventureId: number;
    characterId: number;
    answers: AnswerDTO[];

    constructor(exerciseId?: number, adventureId?: number, characterId?: number, answer?: AnswerDTO[]) {
        this.exerciseId = exerciseId;
        this.adventureId = adventureId;
        this.characterId = characterId;
        this.answers = answer;
    }
}
