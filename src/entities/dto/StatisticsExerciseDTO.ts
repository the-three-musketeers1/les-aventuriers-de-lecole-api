export class StatisticsExerciseDTO {
    characterExerciseId: number;
    exerciseLife: number;
    discipline: string;
    chapter: string;
    goodAnswers: number;
    badAnswers: number;
    damage: number;
    createAt: Date;
    exerciseId: number;


    constructor(characterExerciseId?: number, exerciseLife?: number, discipline?: string, chapter?: string, goodAnswers?: number, badAnswers?: number, damage?: number, createAt?: Date, exerciseId?: number) {
        this.characterExerciseId = characterExerciseId;
        this.exerciseLife = exerciseLife;
        this.discipline = discipline;
        this.chapter = chapter;
        this.goodAnswers = goodAnswers;
        this.badAnswers = badAnswers;
        this.damage = damage;
        this.createAt = createAt;
        this.exerciseId = exerciseId;
    }
}
