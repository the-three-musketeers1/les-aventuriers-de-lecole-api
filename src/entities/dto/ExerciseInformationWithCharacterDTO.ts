export class ExerciseInformationWithCharacterDTO {
    id: number;
    life: number;
    name: string;
    chapter: string;
    discipline: string;
    questionNumber: number;
    validated: boolean;
    damageDone: number;
    description: string;
    mark: number;
    experience: number;

    constructor(id?: number, life?: number, name?: string, chapter?: string, discipline?: string,questionNumber?: number, validated?: boolean, damageDone?: number, description?: string, mark?: number, experience?: number) {
        this.id = id;
        this.life = life;
        this.name = name;
        this.chapter = chapter;
        this.discipline = discipline;
        this.questionNumber = questionNumber;
        this.validated = validated;
        this.damageDone = damageDone;
        this.description = description;
        this.mark = mark;
        this.experience = experience;
    }
}
