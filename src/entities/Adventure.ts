import {
    Column,
    CreateDateColumn,
    Entity,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import {GuildAdventure} from "./GuildAdventure";
import {CharacterAdventure} from "./CharacterAdventure";
import {AdventureExercise} from "./AdventureExercise";
import {IsBoolean, IsNotEmpty, IsNumber, IsOptional, IsPositive, IsString, MaxLength} from "class-validator";
import {Type} from "class-transformer";
import {User} from "./User";


@Entity()
export class Adventure {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsNotEmpty()
    @IsString()
    @MaxLength(255)
    title: string;

    @Column("text")
    @IsNotEmpty()
    @IsString()
    description: string;

    @Column({nullable: true})
    @IsOptional()
    @IsNumber()
    @IsPositive()
    life: number;

    @Column({nullable: true})
    @IsOptional()
    @IsNumber()
    @IsPositive()
    mana: number;

    @Column({ name: "is_expedition"})
    @IsNotEmpty()
    @IsBoolean()
    isExpedition: boolean;

    @Column({name: "experience_bonus"})
    @IsNotEmpty()
    @IsNumber()
    @IsPositive()
    experienceBonus: number;

    @CreateDateColumn({type: "timestamp"})
    createdAt: Date;

    @UpdateDateColumn({type: "timestamp"})
    updatedAt: Date;

    @OneToMany(type => GuildAdventure, guildAdventure => guildAdventure.adventure, {onDelete: 'CASCADE'})
    guildAdventures: GuildAdventure[];

    @OneToMany(type => CharacterAdventure, characterAdventure => characterAdventure.adventure, {onDelete: 'CASCADE'})
    characterAdventures: CharacterAdventure[];

    @ManyToOne(type => User, leader => leader.adventures)
    @IsNotEmpty()
    @Type(() => User)
    leader: User;

    @OneToMany(type => AdventureExercise, adventureExercise => adventureExercise.adventure, {onDelete: "CASCADE"})
    adventureExercises: AdventureExercise[];

}
