import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Adventure} from "./Adventure";
import {Question} from "./Question";
import {Character} from "./Character";
import {IsOptional} from "class-validator";


@Entity()
export class CharacterAdventure {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    done: boolean;

    @Column()
    adventureId: number;

    @Column({nullable: true})
    @IsOptional()
    questionId: number;

    @Column()
    characterId: number;

    @ManyToOne(type => Adventure, adventure => adventure.characterAdventures)
    adventure: Adventure;

    @ManyToOne(type => Question, question => question.characterAdventures)
    question: Question;

    @ManyToOne(type => Character, character => character.characterAdventures)
    character: Character;
}
