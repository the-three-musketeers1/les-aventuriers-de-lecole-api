import {User} from "./User";
import {Character} from "./Character";
import {Role} from "./Role";
import {CharacterAward} from "./CharacterAward";
import {Award} from "./Award";
import {CharacterQuestion} from "./CharacterQuestion";
import {Question} from "./Question";
import {Answer} from "./Answer";
import {Exercise} from "./Exercise";
import {Subject} from "./Subject";
import {Adventure} from "./Adventure";
import {GuildAdventure} from "./GuildAdventure";
import {Guild} from "./Guild";
import {CharacterAdventure} from "./CharacterAdventure";
import {QuestionType} from "./QuestionType";
import {Ticket} from "./Ticket";
import {AdventureExercise} from "./AdventureExercise";
import {Session} from "./Session";
import {CharacterExercise} from "./CharacterExercise";
import {Message} from "./Message";
import {Status} from "./Status";

export const entities = [
    User,
    Session,
    Character,
    Role,
    CharacterAward,
    Award,
    CharacterQuestion,
    Question,
    Answer,
    Exercise,
    Subject,
    Adventure,
    GuildAdventure,
    Guild,
    CharacterAdventure,
    QuestionType,
    Ticket,
    AdventureExercise,
    CharacterExercise,
    Ticket,
    Message,
    Status
];
