import {Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Question} from "./Question";
import {CharacterQuestion} from "./CharacterQuestion";
import {IsBoolean, IsNotEmpty, IsString, MaxLength} from "class-validator";
import {Type} from "class-transformer";


@Entity()
export class Answer {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsString()
    @IsNotEmpty()
    @MaxLength(250)
    content: string;

    @Column()
    @IsBoolean()
    @IsNotEmpty()
    correct: boolean;

    @ManyToOne(type => Question, question => question.answers, {onDelete: 'CASCADE'})
    @IsNotEmpty()
    @Type(() => Question)
    question: Question;

    @OneToMany(type => CharacterQuestion, characterQuestion => characterQuestion.answer)
    characterQuestions: CharacterQuestion[];
}
