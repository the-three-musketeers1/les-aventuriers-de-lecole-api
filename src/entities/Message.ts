import {Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";
import {IsNotEmpty, IsString} from "class-validator";
import {Ticket} from "./Ticket";

@Entity()
export class Message {

    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    @IsNotEmpty()
    @IsString()
    content: string;

    @Column()
    @IsNotEmpty()
    sentByCharacter: boolean;

    @ManyToOne(type => Ticket, ticket => ticket.messages)
    ticket: Ticket;

    @CreateDateColumn({type: "timestamp"})
    sentDate: Date;

    @UpdateDateColumn({type: "timestamp"})
    updatedAt: Date;
}
