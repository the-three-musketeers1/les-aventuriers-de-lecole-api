import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Guild} from "./Guild";
import {Adventure} from "./Adventure";
import {IsBoolean, IsInt, IsNotEmpty, IsNumber, IsOptional} from "class-validator";
import {Type} from "class-transformer";


@Entity()
export class GuildAdventure {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: true})
    @IsOptional()
    @IsNumber()
    current_life: number;

    @Column({nullable: true})
    @IsOptional()
    @IsNumber()
    current_mana: number;

    @Column()
    @IsNotEmpty()
    @IsInt()
    guildId: number;

    @Column()
    @IsNotEmpty()
    @IsInt()
    adventureId: number;

    @Column()
    @IsNotEmpty()
    @IsBoolean()
    accessible: boolean;

    @Column()
    deadline: Date;

    @ManyToOne(type => Guild, guild => guild.guildAdventures, {onDelete: 'CASCADE'})
    @Type(() => Guild)
    guild: Guild;

    @ManyToOne(type => Adventure, adventure => adventure.guildAdventures, {onDelete: 'CASCADE'})
    @Type(() => Adventure)
    adventure: Adventure;
}
