import {Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Role} from "./Role";
import {CharacterAward} from "./CharacterAward";
import {User} from "./User";
import {CharacterAdventure} from "./CharacterAdventure";
import {CharacterExercise} from "./CharacterExercise";
import {Guild} from "./Guild";
import {Ticket} from "./Ticket";

@Entity()
export class Character {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({default: 1})
    level: number;

    @Column({default: 0})
    action_points: number;

    @Column({default: 0})
    experience: number;

    @ManyToOne(type => Role, role => role.characters)
    role: Role;

    @OneToMany(type => CharacterAward, characterAward => characterAward.character)
    characterAwards: CharacterAward[];

    @ManyToOne(type => User, user => user.characters)
    user: User;

    @OneToMany(type => CharacterAdventure, characterAdventure => characterAdventure.character, {onDelete: 'CASCADE'})
    characterAdventures: CharacterAdventure[];

    @OneToMany(type => CharacterExercise, characterExercise => characterExercise.character, {onDelete: 'CASCADE'})
    characterExercise: CharacterExercise[];

    @ManyToMany(type => Guild)
    @JoinTable({name: "guild_character"})
    guilds: Guild[];

    @OneToMany(type => Ticket, ticket => ticket.character)
    tickets: Ticket[];

}
