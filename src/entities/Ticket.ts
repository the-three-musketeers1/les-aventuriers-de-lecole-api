import {
    Column,
    Entity,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import {User} from "./User";
import {IsNotEmpty, IsString, MaxLength} from "class-validator";
import {Character} from "./Character";
import {Message} from "./Message";
import {Status} from "./Status";

@Entity()
export class Ticket {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsNotEmpty()
    @IsString()
    @MaxLength(250)
    title: string;

    @ManyToOne(type => Status, status => status.tickets)
    status: Status;

    @ManyToOne(type => Character, character => character.tickets)
    character: Character;

    @ManyToOne(type => User, user => user.tickets)
    leader: User;

    @OneToMany(type => Message, message => message.ticket)
    messages: Message[];

    @Column({type: "date"})
    date: Date;

    @UpdateDateColumn({type: "timestamp"})
    updatedAt: Date;
}
