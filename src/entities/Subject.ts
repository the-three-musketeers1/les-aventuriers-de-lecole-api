import {Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Exercise} from "./Exercise";
import {IsNotEmpty, IsString, MaxLength} from "class-validator";
import {Type} from "class-transformer";
import {User} from "./User";


@Entity()
export class Subject {


    constructor(id?: number, title?: string, description?: string, exercises?: Exercise[], parentSubject?: Subject, childSubjects?: Subject[]) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.exercises = exercises;
        this.parentSubject = parentSubject;
        this.childSubjects = childSubjects;
    }

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsNotEmpty()
    @IsString()
    @MaxLength(250)
    title: string;

    @Column("text")
    @IsNotEmpty()
    @IsString()
    @MaxLength(65000)
    description: string;

    @OneToMany(type => Exercise, exercise => exercise.subject)
    exercises: Exercise[];

    @ManyToOne(type => Subject, subject => subject.childSubjects)
    @Type(() => Subject)
    parentSubject: Subject;

    @OneToMany(type => Subject, subject => subject.parentSubject)
    childSubjects: Subject[];

    @ManyToMany(type => User)
    @JoinTable({name: "leader_subject"})
    leaders: User[]
}
