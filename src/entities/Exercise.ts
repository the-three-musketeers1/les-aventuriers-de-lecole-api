import {
    Column,
    CreateDateColumn,
    Entity,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import {Question} from "./Question";
import {Subject} from "./Subject";
import {AdventureExercise} from "./AdventureExercise";
import {IsInt, IsNotEmpty, IsPositive, IsString} from "class-validator";
import {Type} from "class-transformer";
import {User} from "./User";


@Entity()
export class Exercise {


    constructor(id?: number, name?: string, description?: string, life?: number, questions?: Question[], subject?: Subject, leader?: User, adventureExercises?: AdventureExercise[]) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.life = life;
        this.questions = questions;
        this.subject = subject;
        this.leader = leader;
        this.adventureExercises = adventureExercises;
    }

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsNotEmpty()
    @IsString()
    name: string;

    @Column("text")
    @IsNotEmpty()
    @IsString()
    description: string;

    @Column()
    @IsNotEmpty()
    @IsInt()
    @IsPositive()
    life: number;

    @Column()
    @IsNotEmpty()
    @IsInt()
    @IsPositive()
    experience: number;

    @CreateDateColumn({type: "timestamp"})
    createdAt: Date;

    @UpdateDateColumn({type: "timestamp"})
    updatedAt: Date;

    @OneToMany(type => Question, question => question.exercise, { onDelete: 'CASCADE' })
    questions: Question[];

    @ManyToOne(type => Subject, subject => subject.exercises)
    @IsNotEmpty()
    @Type(() => Subject)
    subject: Subject;

    @ManyToOne(type => User, leader => leader.exercises)
    @IsNotEmpty()
    @Type(() => User)
    leader: User;

    @OneToMany(type => AdventureExercise, adventureExercise => adventureExercise.exercise)
    adventureExercises: AdventureExercise[];
}
