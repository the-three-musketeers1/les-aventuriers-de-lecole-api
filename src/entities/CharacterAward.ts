import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Character} from "./Character";
import {Award} from "./Award";



@Entity()
export class CharacterAward {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    characterId: number;

    @Column()
    awardId: number;

    @Column()
    usage_count: number;

    @ManyToOne(type => Character, character => character.characterAwards)
    character: Character;

    @ManyToOne(type => Award, award => award.characterAwards)
    award: Award;
}
