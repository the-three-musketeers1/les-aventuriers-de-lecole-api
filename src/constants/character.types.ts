export const characterTypes = {
    CharacterRepository: Symbol('CharacterRepository'),
    CharacterService: Symbol('CharacterService'),
    CharacterController: Symbol('CharacterController')
};
