export const characterAdventureTypes = {
    CharacterAdventureRepository: Symbol('CharacterAdventureRepository'),
    CharacterAdventureService: Symbol('CharacterAdventureService'),
    CharacterAdventureController: Symbol('CharacterAdventureController')
};
