export const characterExerciseTypes = {
    CharacterExerciseRepository: Symbol('CharacterExerciseRepository'),
    CharacterExerciseService: Symbol('CharacterExerciseService'),
    CharacterExerciseController: Symbol('CharacterExerciseController')
};
