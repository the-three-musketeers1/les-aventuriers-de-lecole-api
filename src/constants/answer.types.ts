export const answerTypes = {
    AnswerRepository: Symbol('AnswerRepository'),
    AnswerService: Symbol('AnswerService'),
    AnswerController: Symbol('AnswerController')
};
