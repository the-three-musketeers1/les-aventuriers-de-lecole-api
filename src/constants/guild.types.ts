export const guildTypes = {
    GuildRepository: Symbol('GuildRepository'),
    GuildService: Symbol('GuildService'),
    GuildController: Symbol('GuildController')
};
