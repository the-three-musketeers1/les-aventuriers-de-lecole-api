import {userTypes} from "./user.types";
import {questionTypeTypes} from "./question.type.types";
import {questionTypes} from "./question.types";
import {answerTypes} from "./answer.types";
import {exerciseTypes} from "./exercise.types";
import {subjectTypes} from "./subject.types";
import {characterQuestionTypes} from "./characterQuestion.types";
import {adventureTypes} from "./adventure.types";
import {characterExerciseTypes} from "./characterExercise.types";
import {adventureExerciseTypes} from "./adventureExercise.types";
import {characterTypes} from "./character.types";
import {authTypes} from "./auth.types";
import {guildTypes} from "./guild.types";
import {roleTypes} from "./role.types";
import {guildAdventureTypes} from "./guildAdventure.types";
import {characterAdventureTypes} from "./characterAdventure.types";
import {ticketTypes} from "./ticket.types";
import {statusTypes} from "./status.types";
import {messageTypes} from "./message.types";

export const TYPE = {
    ...userTypes,
    ...questionTypeTypes,
    ...questionTypes,
    ...answerTypes,
    ...exerciseTypes,
    ...subjectTypes,
    ...characterQuestionTypes,
    ...characterExerciseTypes,
    ...adventureExerciseTypes,
    ...adventureTypes,
    ...guildAdventureTypes,
    ...guildTypes,
    ...characterTypes,
    ...authTypes,
    ...guildTypes,
    ...roleTypes,
    ...characterAdventureTypes,
    ...ticketTypes,
    ...statusTypes,
    ...messageTypes,
    SecurityUtil: Symbol('SecurityUtil'),
    SessionRepository: Symbol('SessionRepository')
};
