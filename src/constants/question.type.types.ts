export const questionTypeTypes = {
    QuestionTypeRepository: Symbol('QuestionTypeRepository'),
    QuestionTypeService: Symbol('QuestionTypeService'),
    QuestionTypeController: Symbol('QuestionTypeController')
};
