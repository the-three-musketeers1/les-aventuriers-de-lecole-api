export const authTypes = {
    AuthService: Symbol('AuthService'),
    AuthMiddleware: Symbol('AuthMiddleware'),
    AuthStudentMiddleware: Symbol('AuthStudentMiddleware'),
    AuthTeacherMiddleware: Symbol('AuthTeacherMiddleware'),
    AuthCharacterMiddleware: Symbol('AuthCharacterMiddleware'),
    AuthOnlyMiddleware: Symbol('AuthOnlyMiddleware'),
    AuthOnlyTeacherMiddleware: Symbol('AuthOnlyTeacherMiddleware')
};
