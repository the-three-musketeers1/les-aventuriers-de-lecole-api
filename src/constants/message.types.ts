export const messageTypes = {
    MessageRepository: Symbol('MessageRepository'),
    MessageService: Symbol('MessageService'),
    MessageController: Symbol('MessageController')
};
