export const guildAdventureTypes = {
    GuildAdventureRepository: Symbol('GuildAdventureRepository'),
    GuildAdventureService: Symbol('GuildAdventureService'),
    GuildAdventureController: Symbol('GuildAdventureController')
};
