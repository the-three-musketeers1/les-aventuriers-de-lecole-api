export const exerciseTypes = {
    ExerciseRepository: Symbol('ExerciseRepository'),
    ExerciseService: Symbol('ExerciseService'),
    ExerciseController: Symbol('ExerciseController')
};
