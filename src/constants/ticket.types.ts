export const ticketTypes = {
    TicketRepository: Symbol('TicketRepository'),
    TicketService: Symbol('TicketService'),
    TicketController: Symbol('TicketController')
};
