export const questionTypes = {
    QuestionRepository: Symbol('QuestionRepository'),
    QuestionService: Symbol('QuestionService'),
    QuestionController: Symbol('QuestionController')
};
