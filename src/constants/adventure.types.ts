export const adventureTypes = {
    AdventureRepository: Symbol('AdventureRepository'),
    AdventureService: Symbol('AdventureService'),
    AdventureController: Symbol('AdventureController')
};
