export const userTypes =  {
    UserRepository: Symbol("UserRepository"),
    UserService: Symbol("UserService"),
    UserController: Symbol('UserController'),
};
