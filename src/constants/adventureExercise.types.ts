export const adventureExerciseTypes = {
    AdventureExerciseRepository: Symbol('AdventureExerciseRepository'),
    AdventureExerciseService: Symbol('AdventureExerciseService')
};
