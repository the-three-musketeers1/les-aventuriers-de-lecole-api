export const subjectTypes = {
    SubjectRepository: Symbol('SubjectRepository'),
    SubjectService: Symbol('SubjectService'),
    SubjectController: Symbol('SubjectController')
};
