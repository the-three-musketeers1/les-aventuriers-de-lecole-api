export const characterQuestionTypes = {
    CharacterQuestionRepository: Symbol('CharacterQuestionRepository'),
    CharacterQuestionService: Symbol('CharacterQuestionService'),
    CharacterQuestionController: Symbol('CharacterQuestionController')
};
