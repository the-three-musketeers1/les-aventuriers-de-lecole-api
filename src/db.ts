import {createConnection} from "typeorm";
import {entities} from "./entities";

export async function getDbConnection() {
    const DATABASE_HOST = process.env.DATABASE_HOST;
    const DATABASE_USER = process.env.DATABASE_USER;
    const DATABASE_PORT: number = parseInt(process.env.DATABASE_PORT);
    const DATABASE_PASSWORD = process.env.DATABASE_PASSWORD;
    const DATABASE_DB = process.env.DATABASE_DB;

    return await createConnection({
        type: "mysql",
        host: DATABASE_HOST,
        port: DATABASE_PORT,
        username: DATABASE_USER,
        password: DATABASE_PASSWORD,
        database: DATABASE_DB,
        entities: entities,
        logging: false,
        synchronize: true,
        timezone: 'utc+2'
    });
}
