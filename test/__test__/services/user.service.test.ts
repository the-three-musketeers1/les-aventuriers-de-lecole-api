import {UserService} from "../../../src/services/user.service";
import {User} from "../../../src/entities/User";
import {UserRepository} from "../../../src/repositories/user.repository";

jest.mock('../../../src/repositories/user.repository');
jest.mock('../../../src/repositories/AdventureRepository');
jest.mock('../../../src/repositories/SubjectRepository');
jest.mock('../../../src/repositories/ExerciseRepository');

describe('UserService tests', () => {
    let userService: UserService;
    let users: User[];
    let mockUserRepository;
    let mockSubjectRepository;
    let mockAdventureRepository;
    let mockExerciseRepository;

    beforeAll(() => {
        users = [
            new User(2, "jeremy@orange.fr", "Jérémy", "Ternisien", "secret"),
            new User(3, "masa@gmail.fr", "Masataka", "Ishii", "secret")
        ];
        mockUserRepository = new UserRepository();
        userService = new UserService(
            mockUserRepository,
            mockExerciseRepository,
            mockAdventureRepository,
            mockSubjectRepository);
    });

    beforeEach(() => {
        jest.clearAllMocks();
    });

    describe('method save', () => {
       it('should call save user repository', async() => {
          await userService.save(users[0]);
          expect(mockUserRepository.saveUser).toBeCalledTimes(1);
          expect(mockUserRepository.saveUser).toBeCalledWith(users[0]);
       });
    });
    describe('method find', () => {
        it('should call find user repository', async () => {
            await userService.find();
            expect(mockUserRepository.findAll).toBeCalledTimes(1);
        });

        it('should return Users', async () => {
            mockUserRepository.findAll.mockReturnValue(users);

            const result = await userService.find();

            expect(mockUserRepository.findAll).toBeCalledTimes(1);
            expect(result.length).toEqual(2);

            expect(result).toEqual(users);
        });
    });
    describe('method findOne', () => {
       it('should find one user by user repository',async () => {
           mockUserRepository.findById.mockReturnValue(users[1]);

           const result = await userService.findOne(users[1].id);

           expect(mockUserRepository.findById).toBeCalledTimes(1);
           expect(mockUserRepository.findById).toBeCalledWith(users[1].id);
           expect(result).toEqual(users[1]);
        });
    });
    describe('method update', () => {
        it('should not be call when id user is not found by userRepository', async () => {
            const user = new User();
            mockUserRepository.findById.mockReturnValue(null);

            const result = await userService.update(99, user);
            expect(result).toBe(false);
            expect(mockUserRepository.update).not.toBeCalled();
        });
        it('should return true when id correspond to user id id database', async () => {
            const user = new User();
            mockUserRepository.findByIdWithLessInformations.mockReturnValue(users[0]);

            const result = await userService.update(99, user);

            expect(result).toBe(true);
        });
        it('should not update with new user when user is empty', async () => {
            const user = new User();
            mockUserRepository.findByIdWithLessInformations.mockReturnValue(users[0]);

            await userService.update(99, user);

            expect(mockUserRepository.updateUser).toBeCalledTimes(1);
            expect(mockUserRepository.updateUser).toBeCalledWith(user);
        });
        it('should not update when field firstname of request data is empty', async () => {
            const emptyFirstNames = [null, undefined, ''];

            for (let i = 0; i < emptyFirstNames.length; i++) {
                const user = new User();
                user.id = users[0].id;
                user.firstname = emptyFirstNames[i];
                mockUserRepository.findByIdWithLessInformations.mockReturnValue(users[0]);
                await userService.update(99, user);

                expect(user.firstname).not.toBe(emptyFirstNames[i]);
                expect(user.firstname).toBe(users[0].firstname);
                expect(mockUserRepository.updateUser).toBeCalledWith(user);
            }
        });

        it('should update one field of user', async () => {
            const user = new User();
            user.id = users[0].id;
            user.firstname = "updateFirstname";

            mockUserRepository.findByIdWithLessInformations.mockReturnValue(users[0]);

            await userService.update(99, user);

            expect(user.firstname).toBe("updateFirstname");
            expect(user.lastname).toBe(users[0].lastname);
            expect(user.email).toBe(users[0].email);
            expect(user.password).toBe(users[0].password);
            expect(mockUserRepository.updateUser).toBeCalledWith(user);
        });

        it('should update all field of user when all field is send',async () => {
            const user = new User();
            user.id = users[0].id;
            user.firstname = "updateFirstname";
            user.lastname = "updateLastname";
            user.email = "update@email.com";
            user.password = "updatePassword";
            mockUserRepository.findByIdWithLessInformations.mockReturnValue(users[0]);

            await userService.update(99, user);

            expect(user.firstname).toBe("updateFirstname");
            expect(user.lastname).not.toBe(users[0].lastname);
            expect(user.lastname).toBe('updateLastname');
            expect(user.email).not.toBe(users[0].email);
            expect(user.email).toBe("update@email.com");
            expect(user.password).not.toBe(users[0].password);
            expect(user.password).toBe("updatePassword");
            expect(mockUserRepository.updateUser).toBeCalledWith(user);
        });
        it('should return true after update user',async () => {
            const user = new User();
            user.id = users[0].id;
            mockUserRepository.findByIdWithLessInformations.mockReturnValue(users[0]);

            const result = await userService.update(99, user);

            expect(result).toBe(true);
        });
    });

    describe('method delete', () => {
        it('should not delete user when id not correspond to user id in database', async () => {
            mockUserRepository.findById.mockReturnValue(undefined);

            await userService.delete(77);

            expect(mockUserRepository.deleteById).not.toBeCalled();
        });
        it('should return false when id not correspond to user id in database', async () => {
            mockUserRepository.findById.mockReturnValue(undefined);

            expect(await userService.delete(77)).toBe(false);
        });
        it('should delete user when id correspond to user id in database', async () => {
            const userId = users[1].id;
            mockUserRepository.findById.mockReturnValue(users[1]);

            await userService.delete(userId);

            expect(mockUserRepository.deleteById).toBeCalledTimes(1);
            expect(mockUserRepository.deleteById).toBeCalledWith(userId);
        });
        it('should return true after delete user', async () => {
            const userId = users[1].id;
            mockUserRepository.findById.mockReturnValue(users[1]);

            expect(await userService.delete(userId)).toBe(true);

        });
    });
});
