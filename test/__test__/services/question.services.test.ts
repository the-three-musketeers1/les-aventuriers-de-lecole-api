import {QuestionService} from "../../../src/services/question.service";
import {Question} from "../../../src/entities/Question";
import {QuestionRepository} from "../../../src/repositories/QuestionRepository";

jest.mock("../../../src/repositories/QuestionRepository");

describe('Question tests', () => {
    let questionService: QuestionService;
    let questions: Question[];
    let mockQuestionRepository;

    beforeAll(() => {
        questions = [
            new Question(1, "Quel est mon nom ?", 4, 40, 20, null, null),
            new Question(2, "Qui suis je", 3, 17, 10, null, null),
        ];
        mockQuestionRepository = new QuestionRepository();
        questionService = new QuestionService(mockQuestionRepository);
    });

    beforeEach(() => {
        jest.clearAllMocks();
    });

    describe('method save', () => {
        it('should call save question repository', async() => {
            await questionService.save(questions[0]);
            expect(mockQuestionRepository.saveQuestion).toBeCalledTimes(1);
            expect(mockQuestionRepository.saveQuestion).toBeCalledWith(questions[0]);
        });
    });

    describe('method find', () => {
        it('should return Questions', async () => {
            mockQuestionRepository.findAllWithExerciseAndType.mockReturnValue(questions);

            const result = await questionService.find();
            expect(mockQuestionRepository.findAllWithExerciseAndType).toHaveBeenCalledTimes(1);
            expect(result.length).toEqual(2);
        });

        it('should call QuestionRepository', async () => {
            await questionService.find();
            expect(mockQuestionRepository.findAllWithExerciseAndType).toHaveBeenCalledTimes(1);
        });
    });

});
