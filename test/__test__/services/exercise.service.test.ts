import {ExerciseService} from "../../../src/services/exercise.service";
import {Exercise} from "../../../src/entities/Exercise";
import {Subject} from "../../../src/entities/Subject";
import {AdventureExercise} from "../../../src/entities/AdventureExercise";
import {ExerciseInformationDTO} from "../../../src/entities/dto/ExerciseInformationDTO";
import {ExerciseRepository} from "../../../src/repositories/ExerciseRepository";
import {SubjectService} from "../../../src/services/subject.service";
import {SubjectRepository} from "../../../src/repositories/SubjectRepository";
import {QuestionRepository} from "../../../src/repositories/QuestionRepository";
import {AdventureRepository} from "../../../src/repositories/AdventureRepository";

jest.mock("../../../src/repositories/ExerciseRepository");
jest.mock("../../../src/repositories/SubjectRepository");
jest.mock("../../../src/services/subject.service");
jest.mock("../../../src/repositories/QuestionRepository");
jest.mock("../../../src/repositories/AdventureRepository");


describe('Exercise tests',() => {
    let exerciseService: ExerciseService;
    let exercises: Exercise[];
    let mockExerciseRepository : ExerciseRepository;
    let mockSubjectService: SubjectService;
    let mockQuestionRepository: QuestionRepository;
    let mockAdventureRepository: AdventureRepository;

    beforeAll(() => {
        let discipline = new Subject(1,'mathematique',null,null,null,null);
        let chapter = new Subject(2,'thales',null,null, discipline,null);

        exercises = [
           new Exercise(1,"exercice 1",null,5000,null,chapter),
            new Exercise(2,"exercice 2",null,1590,null,chapter)
       ];
        mockExerciseRepository = new ExerciseRepository();
        let mockSubjectRepo = new SubjectRepository();
        mockSubjectService = new SubjectService(mockSubjectRepo);
        mockAdventureRepository = new AdventureRepository();
        exerciseService = new ExerciseService(mockExerciseRepository, mockSubjectService, mockQuestionRepository, mockAdventureRepository);
    });

    beforeEach(() => {
        jest.clearAllMocks();
    });

    describe('method transformExercisesIntoExercisesExpeditions ',() => {
        it('should transform exercise in ExerciseInformationDTO', async () => {

            const results = [
                new ExerciseInformationDTO(1,5000,"exercice 1","thales","mathematique"),
                new ExerciseInformationDTO(2,1590,"exercice 2","thales","mathematique")
            ];

            let exercisesInformations =  await  exerciseService.transformExercisesIntoExercisesExpeditions(exercises);
            expect(exercisesInformations).toEqual(results);
        }, );

        it('should return empty array if have empty array', async () => {

            let exercisesInformations =  await  exerciseService.transformExercisesIntoExercisesExpeditions([]);
            expect(exercisesInformations).toEqual([]);
        }, );

        it('should add empty string if exercise has no subject', async () => {

            const results = [
                new ExerciseInformationDTO(1,5000,"exercice 1","",""),
                new ExerciseInformationDTO(2,1590,"exercice 2","thales","mathematique")
            ];
            exercises[0].subject = null;

            let exercisesInformations =  await  exerciseService.transformExercisesIntoExercisesExpeditions(exercises);
            expect(exercisesInformations).toEqual(results);
        }, )
    })
});
