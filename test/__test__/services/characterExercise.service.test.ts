import {CharacterExerciseService} from "../../../src/services/characterExercise.service";
import {CharacterExerciseRepository} from "../../../src/repositories/CharacterExerciseRepository";
import {Subject} from "../../../src/entities/Subject";
import {Exercise} from "../../../src/entities/Exercise";
import {AdventureExercise} from "../../../src/entities/AdventureExercise";
import {CharacterExercise} from "../../../src/entities/CharacterExercise";
import {CharacterQuestion} from "../../../src/entities/CharacterQuestion";
import {Question} from "../../../src/entities/Question";
import {Answer} from "../../../src/entities/Answer";
import {StatisticsExerciseDTO} from "../../../src/entities/dto/StatisticsExerciseDTO";
import {results} from "inversify-express-utils";

jest.mock('../../../src/repositories/CharacterExerciseRepository');

describe('CharacterExerciseService',() => {
    let characterExerciseService: CharacterExerciseService;
    let mockCharacterExerciseRepository;
    let charExerciseOne: CharacterExercise;
    let charExerciseTwo: CharacterExercise;

    beforeAll(() => {
        mockCharacterExerciseRepository = new CharacterExerciseRepository();
        characterExerciseService = new CharacterExerciseService(mockCharacterExerciseRepository);
    });


    beforeEach(() => {
        jest.clearAllMocks();

        let discipline = new Subject(1,'mathematique',null,null,null,null);
        let chapter = new Subject(2,'thales',null,null, discipline,null);

        let exerciseOne =   new Exercise(1,"exercice 1",null,5000,null,chapter);
        let exerciseTwo =   new Exercise(2,"exercice 2",null,1590,null,chapter);
        let adventureExerciseOne = new AdventureExercise(1,0,1,1,null,exerciseOne,null);
        let adventureExerciseTwo = new AdventureExercise(2,0,2,1,null,exerciseTwo,null);
        let questionOne = new Question();
        questionOne.damage = 75;
        let questionTwo = new Question();
        questionTwo.damage = 145;
        let answerOne = new Answer();
        answerOne.correct = true;
        let answerTwo = new Answer();
        answerTwo.correct = false;

        let charQOne = new CharacterQuestion();
        charQOne.question = questionOne;
        charQOne.answer = answerOne;

        let charQTwo = new CharacterQuestion();
        charQTwo.question = questionTwo;
        charQTwo.answer = answerTwo;

        charExerciseOne = new CharacterExercise();
        charExerciseOne.id = 1;
        charExerciseOne.adventureExercise = adventureExerciseOne;
        charExerciseOne.characterQuestions = [charQOne,charQOne];

        charExerciseTwo = new CharacterExercise();
        charExerciseTwo.id = 2;
        charExerciseTwo.adventureExercise = adventureExerciseTwo;
        charExerciseTwo.characterQuestions = [charQTwo,charQOne];
    });

    describe('method mapToStatisticsExercise', () => {
        it('should transform CharacterExercise to StatisticsExerciseDTO', async() => {

            const result = new StatisticsExerciseDTO(1,5000,'mathematique','thales',2,0,150,undefined, 1);
            let stats = await characterExerciseService.mapToStatisticsExercise(charExerciseOne);
            expect(stats).toEqual(result);
            const result2 = new StatisticsExerciseDTO(2,1590,'mathematique','thales',1,1,75,undefined, 2);
            let stats2 = await characterExerciseService.mapToStatisticsExercise(charExerciseTwo);
            expect(stats2).toEqual(result2);
        });

        it('should throw Error when missing adventureExercise or exercise',  () => {
            const expected = Error;
            charExerciseOne.adventureExercise = null;
            let fn = () => characterExerciseService.mapToStatisticsExercise(charExerciseOne);
            expect(fn).toThrowError(expected);
        });

        it('should throw Error when get null data',  () => {
            const expected = Error;
            let fn = () => characterExerciseService.mapToStatisticsExercise(null);
            expect(fn).toThrowError(expected);
        });

        it('should throw Error when get new CharacterExercise',  () => {
            const expected = Error;
            let fn = () => characterExerciseService.mapToStatisticsExercise(new CharacterExercise());
            expect(fn).toThrowError(expected);
        });

        it('should return data with no answers',  () => {
            const result = new StatisticsExerciseDTO(1,5000,'mathematique','thales',0,0,0, undefined, 1);

            charExerciseOne.characterQuestions = [];
            let stats =  characterExerciseService.mapToStatisticsExercise(charExerciseOne);
            expect(stats).toEqual(result);
        })

    })

});
