import {AuthService} from "../../../src/services/auth.service";
import {UserRepository} from "../../../src/repositories/user.repository";
import {User} from "../../../src/entities/User";
import {SecurityUtil} from "../../../src/utils/security.util";
import {SessionRepository} from "../../../src/repositories/session.repository";
import {Session} from "../../../src/entities/Session";
import exp = require("constants");

jest.mock('../../../src/utils/security.util');
jest.mock('../../../src/repositories/user.repository');
jest.mock('../../../src/repositories/session.repository');

describe('AuthService', () => {
    let authService: AuthService;
    let mockUserRepository;
    let mockSecurityUtil;
    let mockSessionRepository;

    beforeAll(() => {
        mockUserRepository = new UserRepository();
        mockSessionRepository = new SessionRepository();
        mockSecurityUtil = new SecurityUtil();
        authService = new AuthService(mockUserRepository, mockSessionRepository, mockSecurityUtil);
    });

    beforeEach(() => {
        jest.clearAllMocks();
    });

    describe('method subscribe', () => {
        let subscriber;
        beforeEach(() => {
            subscriber = new User();
            subscriber.firstname = 'toto';
            subscriber.lastname = 'tata';
            subscriber.password = "totopassword";
            subscriber.email = "toto@tata.fr";
            subscriber.is_teacher = true;
        });

        afterEach(() => {
            subscriber = undefined;
        });

        // it('should test ci/cd', async () => {
        //    expect(true).toBe(false);
        // });

        it('should save subscriber', async () => {
            await authService.subscribe(subscriber);
            expect(mockUserRepository.save).toBeCalledTimes(1);
            expect(mockUserRepository.save).toBeCalledWith(subscriber);
        });
        it('should call user repository find to check if user is already subscribe', async () => {
            await authService.subscribe(subscriber);
            expect(mockUserRepository.findOne).toBeCalledTimes(1);
            expect(mockUserRepository.findOne).toBeCalledWith({
                where: {
                    email: subscriber.email
                }
            });
        });
        it('should return null if user is already subscribe', async () => {
            mockUserRepository.findOne.mockReturnValue(subscriber);

            const result = await authService.subscribe(subscriber);

            expect(result).toBe(null);
        });
        it('should return subscriber if user is not subscribe', async () => {
            mockUserRepository.findOne.mockReturnValue(null);
            mockUserRepository.save.mockReturnValue(subscriber);

            const result = await authService.subscribe(subscriber);
            expect(result.firstname).toEqual(subscriber.firstname);
            expect(result.lastname).toEqual(subscriber.lastname);
            expect(result.email).toEqual(subscriber.email);
        });
        it('should hash password', async () => {

            mockSecurityUtil.hashPassword.mockReturnValue('hashTheTotoPassword');
            await authService.subscribe(subscriber);

            expect(mockSecurityUtil.hashPassword).toBeCalledTimes(1);
            expect(mockSecurityUtil.hashPassword).toBeCalledWith("totopassword");
            expect(subscriber.password).toBe("hashTheTotoPassword");
        });
    });

    describe('method login', () => {
        let subscriber: User;
        let session: Session;
        beforeEach(() => {
            subscriber = new User(
                1,
                "toto@tata.fr",
                "toto",
                "tata",
                "hashTotoPassword",
            );
            subscriber.is_teacher = true;
            mockSecurityUtil.hashPassword.mockReturnValue("hashTotoPassword");

            session = new Session();
            session.id = 2;
            session.token = "6zerg65ze1r6g5e";
            mockSessionRepository.create.mockReturnValue(session);
        });
        it('should find user by email and hash password', async () => {
            await authService.login("totoPassword", "toto@tata.fr", true);
            expect(mockUserRepository.findOne).toBeCalledTimes(1);
            expect(mockUserRepository.findOne).toBeCalledWith({
                where: {
                    email: "toto@tata.fr",
                    password: "hashTotoPassword"
                }
            });
        });
        it('should return null when user is not subscribe', async () => {
            mockUserRepository.findOne.mockReturnValue(null);
            const checkSession: Session = await authService.login("totoPassword", "toto@tata.fr", true);
            expect(mockUserRepository.findOne).toBeCalledTimes(1);
            expect(checkSession).toEqual(null);
        });
        it('should return user when user is subscribe', async () => {
            mockUserRepository.findOne.mockReturnValue(subscriber);
            mockSessionRepository.create.mockReturnValue({token: "toto"});
            mockSessionRepository.save.mockReturnValue({user: subscriber});
            const checkSession: Session = await authService.login(subscriber.password, subscriber.email, true);
            expect(checkSession.user).toEqual(subscriber);
        });
        it('should generate token for session', async () => {
            mockUserRepository.findOne.mockReturnValue(subscriber);
            await authService.login(subscriber.password, subscriber.email, true);
            expect(mockSecurityUtil.randomToken).toBeCalledTimes(1);
        });
        it('should create session with JSON of token value and user id', async () => {
            mockUserRepository.findOne.mockReturnValue(subscriber);
            mockSecurityUtil.randomToken.mockReturnValue(Promise.resolve("6zerg65ze1r6g5e"));

            await authService.login(subscriber.password, subscriber.email, true);
            expect(mockSessionRepository.create).toBeCalledTimes(1);
            expect(mockSessionRepository.create).toBeCalledWith({
                token: `{"hash":"${session.token}","userId":${subscriber.id}}`
            });
        });
        it('should save session in database with id of subscriber', async () => {
            mockUserRepository.findOne.mockReturnValue(subscriber);
            mockSecurityUtil.randomToken.mockReturnValue(Promise.resolve("6zerg65ze1r6g5e"));
            mockSessionRepository.create.mockReturnValue({
                token: JSON.stringify({
                    hash: "6zerg65ze1r6g5e",
                    userId: subscriber.id
                })
            });
            mockSessionRepository.save.mockReturnValue({user: subscriber});
            session = await authService.login(subscriber.password, subscriber.email, true);
            expect(session.user).toEqual(subscriber);
            expect(mockSessionRepository.save).toBeCalledTimes(1);
            expect(mockSessionRepository.save).toBeCalledWith({
                token: JSON.stringify({
                    hash: "6zerg65ze1r6g5e",
                    userId: subscriber.id
                }),
                user: subscriber
            });
        });
    });

    describe('method getUserBySessionToken', () => {
        it('should call session repository to get user', async () => {
            await authService.getUserBySessionToken('weAreSparta!!!');
            expect(mockSessionRepository.findOne).toBeCalledTimes(1);
            expect(mockSessionRepository.findOne).toBeCalledWith({
                relations: ["user"],
                where: {
                    token: 'weAreSparta!!!'
                }
            });
        });
        it('should return ', async () => {
            mockSessionRepository.findOne.mockReturnValue(null);
            const checkUser = await authService.getUserBySessionToken('testToken');
            expect(checkUser).toBe(undefined);
        });
        it('should return user when session is found', async () => {
            const sessionOfUser = {token: 'toto', user: {name: 'loginUser'}};
            mockSessionRepository.findOne.mockReturnValue(sessionOfUser);

            const user = await authService.getUserBySessionToken('weAreSparta!!!');
            expect(user).toEqual(sessionOfUser.user);
        });
    });

    describe('method logout', () => {
        const user = {} as User;
        it('should find session by user id', async () => {
            await authService.logout(user);

            expect(mockSessionRepository.findOne).toBeCalledTimes(1);
            expect(mockSessionRepository.findOne).toBeCalledWith({
                where: {
                    user
                }
            });
        });
        it('should delete session by user id', async () => {
            const deleteSession = new Session();
            deleteSession.token = "6ez4r6g5eg";
            mockSessionRepository.findOne.mockReturnValue(deleteSession);

            await authService.logout(user);

            expect(mockSessionRepository.remove).toBeCalledTimes(1);
            expect(mockSessionRepository.remove).toBeCalledWith(deleteSession);
        });
    });
});
