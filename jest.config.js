module.exports = {
    transform: {
        '^.+\\.ts$': 'ts-jest'
    },
    moduleFileExtensions: [
        'js', 'ts'
    ],
    testEnvironment: 'node',
    testMatch: [
        '**/test/**/*.test.ts'
    ]
};
